/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import feign.Headers;

import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.ping.PingRequest;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.tokenization.TokenizeCardRequest;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.TransactionBaseRequest;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.ping.PingResponse;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.tokenization.TokenizeCardResponse;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.transaction.TransactionFinishedResponse;

/**
 * Restful client interface used to connect to the PayU API.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@FeignClient(name = "payu", url = "${payu.url}", path = "${payu.path}")
public interface PayUClient {

	/**
	 * Ping request to the PayU Api.
	 *
	 * @param body Parameters to the ping
	 * @return The response of the ping if the api is active
	 */
	@Headers("Accept: application/json")
	@PostMapping(consumes = "application/json", produces = "application/json")
	PingResponse ping(final PingRequest body);

	/**
	 * Make a transaction with credit card request to the PayU Api.
	 *
	 * @param body Parameters of the transaction
	 * @return The response of the ping if the api is active
	 */
	@Headers({"Accept: application/json", "Content-Type application/json"})
	@PostMapping(consumes = "application/json", produces = "application/json")
	TransactionFinishedResponse makeTransaction(final TransactionBaseRequest body);

	/**
	 * Make a tokenization request to the PayU Api.
	 *
	 * @param body Parameters of the transaction
	 * @return The response of the ping if the api is active
	 */
	@Headers({"Accept: application/json", "Content-Type application/json"})
	@PostMapping(consumes = "application/json", produces = "application/json")
	TokenizeCardResponse tokenizeCreditCard(final TokenizeCardRequest body);

}
