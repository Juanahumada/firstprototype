/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter.constrains;

/**
 * Interface for mapping between entities and models
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface EntityMapper<T, P> {

	/**
	 * Convert entity to model
	 *
	 * @param entity instance
	 * @return model instance
	 */
	T toModel(final P entity);

	/**
	 * Convert model to entity
	 *
	 * @param model instance
	 * @return entity instance
	 */
	P toEntity(final T model);
}
