/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.newpayment;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the Credit card information to PayU API 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreditCard {

	/**
	 * Credit card number
	 */
	@JsonProperty("number")
	private String number;
	/**
	 * Credit card security code
	 */
	@JsonProperty("securityCode")
	private String securityCode;
	/**
	 * Credit card expiration date
	 */
	@JsonProperty("expirationDate")
	private String expirationDate;
	/**
	 * Credit card name
	 */
	@JsonProperty("name")
	private String name;
}
