/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PaymentInfoNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.usecase.exception.UserNotFoundException;
import com.payulatam.prototype.application.dataprovider.adapter.converter.PaymentInfoModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.TokenizedCardModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.UserModelMapper;
import com.payulatam.prototype.application.dataprovider.database.model.PaymentInfoModel;
import com.payulatam.prototype.application.dataprovider.database.model.UserModel;
import com.payulatam.prototype.application.dataprovider.database.repository.PaymentInfoRepository;
import com.payulatam.prototype.application.dataprovider.database.repository.UserRepository;

/**
 * Adapts the UserModel object to the entity User object for the core.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class UserAdapter {

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final TokenizedCardModelMapper tokenizedCardModelMapper;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final UserModelMapper userModelMapper;

	/**
	 * User repository supplier.
	 */
	private final UserRepository userRepository;

	/**
	 * Payment info repository supplier.
	 */
	private final PaymentInfoRepository paymentInfoRepository;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final PaymentInfoModelMapper paymentInfoModelMapper;

	/**
	 * Handle the user authentication.
	 *
	 * @param username Username provided.
	 * @return True if the user is in the database, otherwise false.
	 * @throws PrototypeException When the user with the given Id does not exist
	 */
	public User getUserByEmail(final String username) throws PrototypeException {

		final Optional<UserModel> user = userRepository.findByEmail(username);
		return user.map(userModelMapper::toEntity).orElseThrow(() -> new UserNotFoundException("There are no users with the given Id."));
	}

	/**
	 * Saves the payment info and assigns the payment info to the user.
	 *
	 * @param userId         User to assign the payment info
	 * @param newPaymentInfo PaymentInfo information
	 * @param tokenizedCard  Card tokenized
	 */
	public void savePaymentInfo(final User userId, final NewPaymentInfo newPaymentInfo, final TokenizedCard tokenizedCard) {

		final UserModel userModel = userModelMapper.toModel(userId);
		final PaymentInfoModel paymentInfoModel = createPaymentInfoModel(newPaymentInfo, tokenizedCard);
		userModel.addPaymentInfo(paymentInfoModel);
		userRepository.save(userModel);

	}

	/**
	 * Method that takes the information in the user, new payment information and tokenized card. And converts it into a PaymentInfoModel to
	 * save into the database.
	 *
	 * @param newPaymentInfo Information of the payment
	 * @param tokenizedCard  Tokenized card in the payment info
	 * @return All the information transformed into a PaymentInfoModel.
	 */
	private PaymentInfoModel createPaymentInfoModel(final NewPaymentInfo newPaymentInfo,
													final TokenizedCard tokenizedCard) {

		return PaymentInfoModel.builder()
							   .tokenizedCard(tokenizedCardModelMapper.toModel(tokenizedCard))
							   .billingAddressStreet1(newPaymentInfo.getBillingAddressStreet1())
							   .billingAddressStreet2(newPaymentInfo.getBillingAddressStreet2())
							   .billingAddressState(newPaymentInfo.getBillingAddressState())
							   .billingAddressCity(newPaymentInfo.getBillingAddressCity())
							   .billingAddressCountry(newPaymentInfo.getBillingAddressCountry())
							   .billingAddressPostalCode(newPaymentInfo.getBillingAddressPostalCode())
							   .billingAddressPhone(newPaymentInfo.getBillingAddressPhone())
							   .build();
	}

	/**
	 * Gets the payment info of the given user.
	 *
	 * @param user User received.
	 * @return The list of the saved payment info for the user
	 */
	public List<SavedPaymentInfo> getPaymentInfo(final User user) {

		return user.getPaymentsInfo();
	}

	/**
	 * Gets the payment info with the given id
	 *
	 * @param paymentInfoId Payment info id
	 * @return The payment info with the given id.
	 * @throws PrototypeException When the payment information with the given id does not exists.
	 */
	public SavedPaymentInfo getPaymentInfoById(String paymentInfoId) throws PrototypeException {

		final Optional<PaymentInfoModel> optionalPaymentInfoModel = paymentInfoRepository.findAllById(paymentInfoId);
		return optionalPaymentInfoModel.map(paymentInfoModelMapper::toEntity).orElseThrow(() -> new PaymentInfoNotFoundException("Any "
																																		 + "valid payment information found with the given id."));
	}
}
