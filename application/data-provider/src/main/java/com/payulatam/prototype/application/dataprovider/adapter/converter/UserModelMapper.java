/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.dataprovider.adapter.converter.constrains.EntityMapper;
import com.payulatam.prototype.application.dataprovider.database.model.UserModel;

/**
 * Mapper that convert a {@link UserModel} to {@link User} or backwards
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "Spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface UserModelMapper extends EntityMapper<UserModel, User> {

	/**
	 * Maps a {@link UserModel} into a {@link User} Customized mapping because soft deleted model should be a null entity.
	 *
	 * @param model User model to map.
	 * @return User mapped into a entity.
	 */
	@Override
	User toEntity(UserModel model);

	/**
	 * Maps a {@link User} into a {@link UserModel}.
	 *
	 * @param entity User entity to map.
	 * @return Mapped User model.
	 */
	@Override
	@InheritInverseConfiguration
	UserModel toModel(User entity);
}
