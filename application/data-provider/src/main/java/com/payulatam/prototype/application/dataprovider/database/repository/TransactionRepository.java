/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.payulatam.prototype.application.dataprovider.database.model.TransactionModel;

/**
 * Provides CRUD functionality for the TransactionModel class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface TransactionRepository extends CrudRepository<TransactionModel, UUID> {

	/**
	 * Finds a transaction with the given id.
	 *
	 * @param id Id of the transaction
	 * @return Transaction with the given id.
	 */
	Optional<TransactionModel> findAllById(String id);
}
