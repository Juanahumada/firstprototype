/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.dataprovider.adapter.converter.constrains.EntityMapper;
import com.payulatam.prototype.application.dataprovider.database.model.OrderModel;

/**
 * Mapper that convert a {@link OrderModel} to {@link Order} or backwards
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "Spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface OrderModelMapper extends EntityMapper<OrderModel, Order> {

	/**
	 * Maps a {@link OrderModel} into a {@link Order} Customized mapping because soft deleted model should be a null entity.
	 *
	 * @param model Order model to map.
	 * @return Order mapped into a entity.
	 */
	@Override
	Order toEntity(OrderModel model);

	/**
	 * Maps a {@link Order} into a {@link OrderModel}.
	 *
	 * @param entity Product entity to map.
	 * @return Mapped product model.
	 */
	@Override
	@InheritInverseConfiguration
	OrderModel toModel(Order entity);

	/**
	 * Convert dto list to entity list
	 *
	 * @param modelList model list
	 * @return entity list
	 */
	List<Order> toEntityList(List<OrderModel> modelList);
}
