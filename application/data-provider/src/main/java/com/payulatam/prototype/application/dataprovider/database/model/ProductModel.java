/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Product model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@Entity
@Table(schema = "proto", name = "product")
@NoArgsConstructor
public class ProductModel {

	/**
	 * Product id.
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id",
			updatable = false)
	private String id;

	/**
	 * Product name
	 */
	@Column(name = "name", nullable = false)
	private String name;

	/**
	 * Product description
	 */
	@Column(name = "description", nullable = false)
	private String description;

	/**
	 * Product value
	 */
	@Column(name = "value", nullable = false)
	private BigDecimal value;

	/**
	 * Product photo
	 */
	@Column(name = "photo")
	private String photo;
	/**
	 * Product stock
	 */
	@Column(name = "stock", nullable = false)
	private Integer stock;

	/**
	 * Updates the stock of the product
	 *
	 * @param quantity Number of units sold or bought.
	 * @param increase If the stock is added or sold.
	 */
	public void updateStock(final int quantity, final boolean increase) {

		if (this.stock != null) {
			if (increase) {
				this.stock += quantity;
			} else {
				this.stock -= quantity;
			}
		}
	}
}
