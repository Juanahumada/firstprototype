/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.repository;

import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.payulatam.prototype.application.dataprovider.database.model.UserModel;

/**
 * Provides CRUD functionality for the UserModel class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface UserRepository extends CrudRepository<UserModel, UUID> {

	/**
	 * Find an user with the given email.
	 *
	 * @param email The email of the user.
	 * @return The user with the given email
	 */
	@Query("Select A from UserModel A where A.email = (:email)")
	Optional<UserModel> findByEmail(String email);
}