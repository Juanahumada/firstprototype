/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter;

import java.util.List;
import java.util.Optional;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.ProductNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.dataprovider.adapter.converter.ProductModelMapper;
import com.payulatam.prototype.application.dataprovider.database.model.ProductModel;
import com.payulatam.prototype.application.dataprovider.database.repository.ProductRepository;

/**
 * Adapts the ProductModel object to the entity Product object for the core.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ProductAdapter {

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final ProductModelMapper mapper;

	/**
	 * Product repository supplier.
	 */
	private final ProductRepository productRepository;

	/**
	 * Gets all the products.
	 *
	 * @return The profile(s) that contains all the given values.
	 */
	public List<Product> getAllProducts() {

		return mapper.toEntityList(productRepository.findAll());
	}

	/**
	 * Method that gets a product with a given id.
	 *
	 * @param productId Id of the product
	 * @return The product with the given Id.
	 * @throws PrototypeException When the product does not exists.
	 */
	public Product getProductWithId(final String productId) throws PrototypeException {

		final Optional<ProductModel> product = productRepository.findById(productId);
		return product.map(mapper::toEntity).orElseThrow(() -> new ProductNotFoundException("There are no products with the given Id."));
	}

	/**
	 * Method that saves a product.
	 *
	 * @param product Product to be saved.
	 */
	public void saveProduct(final Product product) {

		productRepository.save(mapper.toModel(product));
	}

	/**
	 * Updates the stock of a given product.
	 *
	 * @param product  The product.
	 * @param stock    The quantity to be reduced or increased.
	 * @param increase True if the stock has to be increased or false if it has to be reduced.
	 */
	public void updateStock(final Product product, final int stock, final boolean increase) {

		final ProductModel productModel = mapper.toModel(product);
		productModel.updateStock(stock, increase);
		productRepository.save(productModel);
		mapper.toEntity(productModel);
	}

	/**
	 * Method that delete a given product.
	 *
	 * @param productId Product to be deleted.
	 */
	public void deleteProduct(String productId) {

		productRepository.deleteById(productId);
	}

}
