/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * Order model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@Setter
@Entity
@Table(schema = "proto", name = "order")
@NoArgsConstructor
@AllArgsConstructor
public class OrderModel {

	/**
	 * Order id
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id",
			updatable = false, nullable = false)
	private String id;

	/**
	 * Order total value
	 */
	@Column(name = "total_value")
	private BigDecimal totalValue;

	/**
	 * Order status
	 */
	@Column(name = "status", nullable = false)
	private String status;
	/**
	 * Products of the order.
	 */
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private List<OrderProductModel> products;

	/**
	 * Method that adds a new ProductModel to an order.
	 *
	 * @param orderProductModel Product and order relation model to add to the order.
	 */
	public void addProduct(final OrderProductModel.OrderProductModelBuilder orderProductModel) {

		if (this.products == null) {
			this.products = new ArrayList<>();
			products.add(orderProductModel.build());
		} else {
			final Optional<OrderProductModel> product =
					this.products.stream().filter(p -> p.getProduct().getId().equals(orderProductModel.build().getProduct().getId()))
								 .findFirst();
			if (product.isPresent()) {
				product.get().setQuantity(product.get().getQuantity() + orderProductModel.build().getQuantity());
			} else {
				products.add(orderProductModel.build());
			}
			calculateOrderValue();
		}
	}

	/**
	 * Calculates the value of the order guided by the products quantity and value.
	 */
	private void calculateOrderValue() {

		this.totalValue = BigDecimal.ZERO;
		this.products.forEach((OrderProductModel product) -> this.totalValue = this.totalValue
				.add(product.getProduct().getValue().multiply(BigDecimal.valueOf(product.getQuantity()))));
	}

	/**
	 * Transactions of the order.
	 */
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private List<TransactionModel> transactions;

	/**
	 * Adds a transaction to the transactions list.
	 *
	 * @param transactionModel Transaction to be added
	 */
	public void addTransaction(final TransactionModel transactionModel) {

		if (this.transactions == null) {
			this.transactions = new ArrayList<>();
		}
		this.transactions.add(transactionModel);
	}
}
