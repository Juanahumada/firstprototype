/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the Payer information to PayU API 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Payer extends ContactInfo {

	/**
	 * Payer Id
	 */
	@JsonProperty("merchantPayerId")
	private String merchantPayerId;
	/**
	 * Payer shippingAddress info
	 */
	@JsonProperty("billingAddress")
	private Address billingAddress;

	/**
	 * Payer builder constructor
	 *
	 * @param merchantPayerId Payer merchant id
	 * @param fullName        Payer full name
	 * @param emailAddress    Payer email
	 * @param contactPhone    Payer phone
	 * @param dniNumber       Payer dni
	 * @param billingAddress  Payer shipping address
	 */
	@Builder
	public Payer(final String merchantPayerId, final String fullName, final String emailAddress, final String contactPhone,
				 final String dniNumber, final Address billingAddress) {

		super(fullName, emailAddress, contactPhone, dniNumber);
		this.merchantPayerId = merchantPayerId;
		this.billingAddress = billingAddress;
	}
}
