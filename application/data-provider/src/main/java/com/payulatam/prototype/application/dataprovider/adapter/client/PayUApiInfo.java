/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client;

import lombok.Getter;

/**
 * Enum with specific data of the connection to payu api.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
public enum PayUApiInfo {

	TEST_PAYU_URL("https://sandbox.api.payulatam.com/payments-api/4.0/service.cgi "),
	API_LOGIN("pRRXKOl8ikMmt9u"),
	API_KEY("4Vj8eK4rloUd272L48hsrarnUA"),
	CREATE_TOKEN_COMMAND("CREATE_TOKEN"),
	SUBMIT_TRANSACTION_COMMAND("SUBMIT_TRANSACTION"),
	DEFAULT_LANGUAGE("es"),
	NOTIFY_URL("http://www.tes.com/confirmation"),
	ACCOUNT_ID("512321"),
	DEFAULT_CURRENCY("COP"),
	MERCHANT_ID("508029"),
	PING_COMMAND("PING"),
	TOKENIZE_CARD_COMMAND("CREATE_TOKEN"),
	TRANSACTION_STATUS_APPROVED("APPROVED"),
	TRANSACTION_STATUS_PENDING("PENDING"),
	TRANSACTION_STATUS_DECLINED("DECLINED"),
	TRANSACTION_TYPE_AUTHORIZATION_CAPTURE("AUTHORIZATION_AND_CAPTURE"),
	TRANSACTION_TYPE_REFUND("REFUND"),
	MERCHANT_BUYER_ID("PROTOTYPE"),
	MERCHANT_FULL_NAME("calle 100"),
	MERCHANT_EMAIL("adminuser@test.com"),
	MERCHANT_PHONE("7563126"),
	MERCHANT_DNI("5415668464654"),
	MERCHANT_STREET1("calle 100"),
	MERCHANT_STREET2("5555487"),
	MERCHANT_CITY("Medellin"),
	MERCHANT_STATE("Antioquia"),
	MERCHANT_COUNTRY("CO"),
	MERCHANT_POSTAL_CODE("000000"),
	DEVICE_SESSION_ID("vghs6tvkcle931686k1900o6e1"),
	DEVICE_IP_ADDRESS("127.0.0.1"),
	DEVICE_COOKIE("pt1t38347bs6jc9ruv2ecpv7o2"),
	DEVICE_USER_AGENT("Mozilla/5.0 (Windows NT 5.1; rv:18.0) Gecko/20100101 Firefox/18.0"),
	CODE_SUCCESS("SUCCESS"),
	RESPONSE_CODE_INVALID_TRANSACTION("INVALID_TRANSACTION"),
	CODE_ERROR("ERROR");

	/**
	 * Api information string representation.
	 **/
	private final String detail;

	/**
	 * Constructor with description injected by dependency inversion
	 *
	 * @param detail Api information required.
	 */
	PayUApiInfo(final String detail) {

		this.detail = detail;
	}
}
