/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.refund;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Refund transaction request
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class RefundTransaction {

	/**
	 * Transaction order
	 */
	@JsonProperty("order")
	private RefundOrder order;
	/**
	 * Transaction type
	 */
	@JsonProperty("type")
	private String type;
	/**
	 * Transaction reason
	 */
	@JsonProperty("reason")
	private String reason;
	/**
	 * Transaction parent transaction id
	 */
	@JsonProperty("parentTransactionId")
	private String parentTransactionId;
}
