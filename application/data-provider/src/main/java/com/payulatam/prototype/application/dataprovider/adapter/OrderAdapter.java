/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.OrderNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.dataprovider.adapter.converter.OrderModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.ProductModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.UserModelMapper;
import com.payulatam.prototype.application.dataprovider.database.model.OrderModel;
import com.payulatam.prototype.application.dataprovider.database.model.OrderProductModel;
import com.payulatam.prototype.application.dataprovider.database.model.OrderProductModel.OrderProductModelBuilder;
import com.payulatam.prototype.application.dataprovider.database.model.OrderStatus;
import com.payulatam.prototype.application.dataprovider.database.model.UserModel;
import com.payulatam.prototype.application.dataprovider.database.repository.OrderRepository;
import com.payulatam.prototype.application.dataprovider.database.repository.UserRepository;

/**
 * Adapts the OrderModel object to the entity Order object for the core.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class OrderAdapter {

	/**
	 * Order repository supplier.
	 */
	private final OrderRepository orderRepository;

	/**
	 * User repository supplier.
	 */
	private final UserRepository userRepository;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final OrderModelMapper orderModelMapper;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final UserModelMapper userModelMapper;

	/**
	 * Product adapter to get the products.
	 */
	private final ProductModelMapper productModelMapper;

	/**
	 * Creates a new order to an user.
	 *
	 * @param user     User that is making the change.
	 * @param product  First product to add to the order.
	 * @param quantity Quantity of the product.
	 */
	public void createOrder(final User user, final Product product, final Integer quantity) {

		final OrderModel order = orderModelMapper.toModel(Order.builder()
															   .products(new ArrayList<>())
															   .status(OrderStatus.OPEN.getDetail())
															   .build());
		final UserModel userModel = userModelMapper.toModel(user);
		final OrderProductModelBuilder orderProductModel =
				OrderProductModel.builder().product(productModelMapper.toModel(product)).quantity(quantity);
		order.addProduct(orderProductModel);
		userModel.addOrder(order);
		userRepository.save(userModel);
	}

	/**
	 * Adds product to an order.
	 *
	 * @param order    Order that has to be modified.
	 * @param user     User that has the order to be modified.
	 * @param product  Product that has to been added.
	 * @param quantity Quantity of the product.
	 */
	public void addProductToOrder(final Order order, final User user, final Product product, final Integer quantity) {

		final Optional<UserModel> userModelOptional = userRepository.findByEmail(user.getEmail());
		final OrderModel orderModel = orderModelMapper.toModel(order);
		if (userModelOptional.isPresent()) {
			final UserModel userModel = userModelOptional.get();
			final OrderProductModel.OrderProductModelBuilder orderProductModel =
					OrderProductModel.builder().product(productModelMapper.toModel(product)).quantity(quantity);
			userModel.addProductToOrder(orderModel, orderProductModel);
			userRepository.save(userModel);
		}
	}

	/**
	 * Gets all the orders in the database.
	 *
	 * @return A list of the orders in the store.
	 */
	public List<Order> getAllOrders() {

		return orderModelMapper.toEntityList(orderRepository.findAll());
	}

	/**
	 * Method that search all the orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the orders in the store for the user.
	 */
	public List<Order> getAllUserOrders(final String userId) {

		final Optional<List<OrderModel>> ordersList = orderRepository.findAllByEmail(userId);
		return ordersList.map(orderModelMapper::toEntityList).orElse(null);
	}

	/**
	 * Method that search all the open orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the open orders in the store for the user.
	 */
	public List<Order> getAllOpenUserOrders(final String userId) {

		final Optional<List<OrderModel>> ordersList = orderRepository
				.findAllByEmail(userId);
		if (ordersList.isPresent()) {
			final List<OrderModel> orderModelList =
					ordersList.get().stream().filter(x -> x.getStatus().equals(OrderStatus.OPEN.getDetail())
													).collect(Collectors.toList());
			return orderModelMapper.toEntityList(orderModelList);

		}
		return ordersList.map(orderModelMapper::toEntityList).orElse(null);
	}

	/**
	 * Get an order with the given id
	 *
	 * @param id The id of the order
	 * @return The order with the given id
	 * @throws PrototypeException When the ordser does not exists.
	 */
	public Order getOrderById(final String id) throws PrototypeException {

		final Optional<OrderModel> order = orderRepository.findAllById(id);
		return order.map(orderModelMapper::toEntity).orElseThrow(() -> new OrderNotFoundException("The order with the given Id does not "
																										  + "exists."));
	}
}
