/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the order information to PayU API 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Order {

	/**
	 * Order account id
	 */
	@JsonProperty("accountId")
	private String accountId;
	/**
	 * Order reference code
	 */
	@JsonProperty("referenceCode")
	private String referenceCode;
	/**
	 * Order description
	 */
	@JsonProperty("description")
	private String description;
	/**
	 * Order language
	 */
	@JsonProperty("language")
	private String language;
	/**
	 * Order signature
	 */
	@JsonProperty("signature")
	private String signature;
	/**
	 * Order notify url
	 */
	@JsonProperty("notifyUrl")
	private String notifyUrl;
	/**
	 * Order additional values
	 */
	@JsonProperty("additionalValues")
	private AdditionalValues additionalValues;
	/**
	 * Order buyer
	 */
	@JsonProperty("buyer")
	private Buyer buyer;
	/**
	 * Order shipping address
	 */
	@JsonProperty("shippingAddress")
	private Address shippingAddress;

}
