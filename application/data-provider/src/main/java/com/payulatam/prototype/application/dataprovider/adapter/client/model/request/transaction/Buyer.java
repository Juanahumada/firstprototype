/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the Buyer information to PayU API 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Buyer extends ContactInfo {

	/**
	 * Buyer Id
	 */
	@JsonProperty("merchantBuyerId")
	private String merchantBuyerId;
	/**
	 * Buyer shippingAddress info
	 */
	@JsonProperty("shippingAddress")
	private Address shippingAddress;

	/**
	 * Buyer builder constructor
	 *
	 * @param merchantBuyerId Buyer merchant id
	 * @param fullName        Buyer full name
	 * @param emailAddress    Buyer email
	 * @param contactPhone    Buyer phone
	 * @param dniNumber       Buyer dni
	 * @param shippingAddress Buyer shipping address
	 */
	@Builder
	public Buyer(final String merchantBuyerId, final String fullName, final String emailAddress, final String contactPhone,
				 final String dniNumber, final Address shippingAddress) {

		super(fullName, emailAddress, contactPhone, dniNumber);
		this.merchantBuyerId = merchantBuyerId;
		this.shippingAddress = shippingAddress;
	}
}
