/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.payulatam.prototype.application.dataprovider.database.model.ProductModel;

/**
 * Provides CRUD functionality for the ProductModel class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface ProductRepository extends CrudRepository<ProductModel, UUID> {

	/**
	 * Find all the items
	 *
	 * @return A list of all the items
	 */
	List<ProductModel> findAll();

	/**
	 * Find the item with the given id
	 *
	 * @return The item with the given id.
	 */
	Optional<ProductModel> findById(String id);

	/**
	 * Deletes the product with the given id.
	 *
	 * @param id Id of the product
	 */
	void deleteById(String id);
}
