/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.response.transaction;

import org.codehaus.jackson.annotate.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represent the response information from PayU API when a transaction is made.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionResponse {

	/**
	 * Transaction response order id
	 */
	@JsonProperty("orderId")
	private String orderId;
	/**
	 * Transaction response transaction id
	 */
	@JsonProperty("transactionId")
	private String transactionId;
	/**
	 * Transaction response state
	 */
	@JsonProperty("state")
	private String state;
	/**
	 * Transaction response payment network responseCode
	 */
	@JsonProperty("paymentNetworkResponseCode")
	private String paymentNetworkResponseCode;
	/**
	 * Transaction response payment network response error message
	 */
	@JsonProperty("paymentNetworkResponseErrorMessage")
	private String paymentNetworkResponseErrorMessage;
	/**
	 * Transaction response traceability code
	 */
	@JsonProperty("trazabilityCode")
	private String trazabilityCode;
	/**
	 * Transaction response authorization code
	 */
	@JsonProperty("authorizationCode")
	private String authorizationCode;
	/**
	 * Transaction response response code
	 */
	@JsonProperty("responseCode")
	private String responseCode;
	/**
	 * Transaction response response message
	 */
	@JsonProperty("responseMessage")
	private String responseMessage;
	/**
	 * Transaction response operation date
	 */
	@JsonProperty("operationDate")
	private long operationDate;

}
