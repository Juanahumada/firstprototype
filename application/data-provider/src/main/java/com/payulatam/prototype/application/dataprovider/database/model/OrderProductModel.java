/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SavedPaymentInfo model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@Setter
@Entity
@Table(schema = "proto", name = "order_product")
@NoArgsConstructor
@AllArgsConstructor
public class OrderProductModel {

	/**
	 * Order id
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id",
			updatable = false, nullable = false)
	private String id;

	/**
	 * Products of the order.
	 */
	@ManyToOne
	private ProductModel product;

	/**
	 * Quantity of the product.
	 */
	@Column(name = "quantity", nullable = false)
	private Integer quantity;

}
