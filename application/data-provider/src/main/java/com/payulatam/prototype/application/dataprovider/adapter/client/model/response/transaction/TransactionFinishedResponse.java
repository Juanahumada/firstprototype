/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.response.transaction;

import org.codehaus.jackson.annotate.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represent the response from PayU API when a transaction is made.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionFinishedResponse {

	/**
	 * Transaction code
	 */
	@JsonProperty("code")
	private String code;
	/**
	 * Transaction error
	 */
	@JsonProperty("error")
	private String error;
	/**
	 * Transaction transaction response
	 */
	@JsonProperty("transactionResponse")
	private TransactionResponse transactionResponse;
}
