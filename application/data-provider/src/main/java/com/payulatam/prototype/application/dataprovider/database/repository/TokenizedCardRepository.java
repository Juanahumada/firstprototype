/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.repository;

import java.util.UUID;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.payulatam.prototype.application.dataprovider.database.model.TokenizedCardModel;

/**
 * Provides CRUD functionality for the TokenizedCardModel class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface TokenizedCardRepository extends CrudRepository<TokenizedCardModel, UUID> {

}
