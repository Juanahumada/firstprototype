/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.tokenization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.Merchant;

/**
 * Represent the Tokenize card request to PayU API.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TokenizeCardRequest {

	/**
	 * Language of the request
	 */
	@JsonProperty("language")
	private String language;
	/**
	 * Command of the request
	 */
	@JsonProperty("command")
	private String command;
	/**
	 * Merchant info of the request
	 */
	@JsonProperty("merchant")
	private Merchant merchant;
	/**
	 * Credit card info to be tokenized
	 */
	@JsonProperty("creditCardToken")
	private CreditCardToken creditCardToken;
}
