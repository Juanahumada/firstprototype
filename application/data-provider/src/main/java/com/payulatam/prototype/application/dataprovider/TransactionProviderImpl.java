/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.dataprovider;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.dataprovider.adapter.TransactionAdapter;

/**
 * Implementation of {@link ProductProvider} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class TransactionProviderImpl implements TransactionProvider {

	/**
	 * Provider for transactions.
	 */
	private final TransactionAdapter adapter;

	/**
	 * Method that makes a transaction with a new payment info.
	 *
	 * @param user           Id of the user.
	 * @param order          Id of the order.
	 * @param newPaymentInfo Information of the payment method.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	@Override public Transaction makeTransactionWithNewPaymentInfo(final User user, final Order order, final NewPaymentInfo newPaymentInfo)
			throws PrototypeException {

		return adapter.makeTransactionWithNewPaymentInfo(user, order, newPaymentInfo);
	}

	/**
	 * Method that makes a transaction with a saved payment info.
	 *
	 * @param user        Id of the user.
	 * @param order       Id of the order.
	 * @param paymentInfo Information of the payment method.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	@Override public Transaction makeTransactionWithSavedPaymentInfo(final User user, final Order order, final SavedPaymentInfo paymentInfo)
			throws PrototypeException {

		return adapter.makeTransactionWithSavedPaymentInfo(user, order, paymentInfo);
	}

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the refund.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	@Override public Transaction reverseTransaction(final Order orderId, final Transaction transactionId,
											 final String reason) throws PrototypeException {

		return adapter.reverseTransaction(orderId, transactionId, reason);
	}

	/**
	 * ∫ Method that tokenize a given credit card.
	 *
	 * @param user           Id of the user
	 * @param newPaymentInfo The information of the credit card.
	 * @return The card tokenized.
	 * @throws PrototypeException When the communication with the external api fails.
	 */
	@Override public TokenizedCard tokenizeCard(final User user, final NewPaymentInfo newPaymentInfo) throws PrototypeException {

		return adapter.tokenizeCreditCard(user, newPaymentInfo);
	}

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	@Override public String ping() {

		return adapter.ping();
	}

	/**
	 * Method that get a transaction with a given id.
	 *
	 * @param transactionId Id of the transaction
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	@Override public Transaction getTransactionWithId(final String transactionId) throws PrototypeException {

		return adapter.getTransactionWithId(transactionId);
	}
}
