/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.response.tokenization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Represent the response information from PayU API when a tokenization is made.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenizedCreditCard {

	/**
	 * Tokenized card credit card token id
	 */
	@JsonProperty("creditCardTokenId")
	private String creditCardTokenId;
	/**
	 * Tokenized card name
	 */
	@JsonProperty("name")
	private String name;
	/**
	 * Tokenized card payer id
	 */
	@JsonProperty("payerId")
	private String payerId;
	/**
	 * Tokenized card identification number
	 */
	@JsonProperty("identificationNumber")
	private String identificationNumber;
	/**
	 * Tokenized card payment method
	 */
	@JsonProperty("paymentMethod")
	private String paymentMethod;
	/**
	 * Tokenized card masked number
	 */
	@JsonProperty("maskedNumber")
	private String maskedNumber;
}
