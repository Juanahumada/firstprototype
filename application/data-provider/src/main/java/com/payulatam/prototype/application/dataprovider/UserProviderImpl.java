/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.UserProvider;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.dataprovider.adapter.UserAdapter;

/**
 * Implementation of {@link UserProvider} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class UserProviderImpl implements UserProvider {

	/**
	 * Provider for get products
	 */
	private final UserAdapter userAdapter;

	/**
	 * Authenticates a user based on the username and password.
	 *
	 * @param email Username received.
	 * @return True if the user data is correct. Otherwise false.
	 * @throws PrototypeException When the user with the given Id does not exist
	 */
	@Override public User getUserByEmail(final String email) throws PrototypeException {

		return userAdapter.getUserByEmail(email);
	}

	/**
	 * Saves the payment method in a user.
	 *
	 * @param userId         Id of the user to save the new payment info
	 * @param newPaymentInfo The new payment info that needs to be saved
	 * @param tokenizedCard  Tokenized card to be saved in the database
	 */
	@Override public void savePaymentInfo(final User userId, final NewPaymentInfo newPaymentInfo, final TokenizedCard tokenizedCard) {

		userAdapter.savePaymentInfo(userId, newPaymentInfo, tokenizedCard);
	}

	/**
	 * Gets the payment info of the given user.
	 *
	 * @param user Username received.
	 * @return The list of the payment info for the user
	 */
	@Override public List<SavedPaymentInfo> getPaymentInfo(final User user) {

		return userAdapter.getPaymentInfo(user);
	}

	/**
	 * Gets the payment info with the given id
	 *
	 * @param paymentInfoId Payment info id
	 * @return The payment info with the given id.
	 * @throws PrototypeException When the payment information with the given id does not exists.
	 */
	@Override public SavedPaymentInfo getPaymentInfoById(final String paymentInfoId) throws PrototypeException {

		return userAdapter.getPaymentInfoById(paymentInfoId);
	}

}
