/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.Merchant;

/**
 * Base request class for the transaction to PayU Api.
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TransactionBaseRequest {

	/**
	 * Language of the request
	 */
	@JsonProperty("language")
	private String language;
	/**
	 * Command of the request
	 */
	@JsonProperty("command")
	private String command;
	/**
	 * Merchant info of the request
	 */
	@JsonProperty("merchant")
	private Merchant merchant;
	/**
	 * Transaction data of the request
	 */
	@JsonProperty("transaction")
	private Object transaction;
	/**
	 * The transaction is a test or real
	 */
	@JsonProperty("test")
	private boolean test;
}
