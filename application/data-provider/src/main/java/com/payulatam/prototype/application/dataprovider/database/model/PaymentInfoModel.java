/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * SavedPaymentInfo model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Entity
@Setter
@Builder
@Table(schema = "proto", name = "saved_payment_info")
@NoArgsConstructor
@AllArgsConstructor
public class PaymentInfoModel {

	/**
	 * Payment id.
	 */
	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	@Column(name = "id",
			updatable = false, nullable = false)
	private String id;
	/**
	 * User credit card tokenized
	 */
	@OneToOne(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private TokenizedCardModel tokenizedCard;

	/**
	 * User billing street 1
	 */
	@Column(name = "billing_address_street_1", nullable = false)
	private String billingAddressStreet1;

	/**
	 * User billing street 2
	 */
	@Column(name = "billing_address_street_2", nullable = false)
	private String billingAddressStreet2;

	/**
	 * User billing state
	 */
	@Column(name = "billing_address_state", nullable = false)
	private String billingAddressState;

	/**
	 * User billing country
	 */
	@Column(name = "billing_address_country", nullable = false)
	private String billingAddressCountry;
	/**
	 * User billing city
	 */
	@Column(name = "billing_address_city", nullable = false)
	private String billingAddressCity;

	/**
	 * User billing postal code
	 */
	@Column(name = "billing_address_postal_code", nullable = false)
	private String billingAddressPostalCode;

	/**
	 * User billing phone
	 */
	@Column(name = "billing_address_phone", nullable = false)
	private String billingAddressPhone;
}
