/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the shipping address information to PayU API 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class Address {

	/**
	 * Shipping address street1
	 */
	@JsonProperty("street1")
	private String street1;
	/**
	 * Shipping address street2
	 */
	@JsonProperty("street2")
	private String street2;
	/**
	 * Shipping address city
	 */
	@JsonProperty("city")
	private String city;
	/**
	 * Shipping address state
	 */
	@JsonProperty("state")
	private String state;
	/**
	 * Shipping address country
	 */
	@JsonProperty("country")
	private String country;
	/**
	 * Shipping address postalCode
	 */
	@JsonProperty("postalCode")
	private String postalCode;
	/**
	 * Shipping address phone
	 */
	@JsonProperty("phone")
	private String phone;

}
