/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.dataprovider.adapter.converter.constrains.EntityMapper;
import com.payulatam.prototype.application.dataprovider.database.model.TransactionModel;

/**
 * Mapper that convert a {@link TransactionModel} to {@link Transaction} or backwards
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "Spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TransactionModelMapper extends EntityMapper<TransactionModel, Transaction> {

	/**
	 * Maps a {@link TransactionModel} into a {@link Transaction} Customized mapping because soft deleted model should be a null entity.
	 *
	 * @param model Transaction model to map.
	 * @return Transaction mapped into a entity.
	 */
	@Override
	Transaction toEntity(TransactionModel model);

	/**
	 * Maps a {@link Transaction} into a {@link TransactionModel}.
	 *
	 * @param entity Transaction entity to map.
	 * @return Mapped Transaction model.
	 */
	@Override
	@InheritInverseConfiguration
	TransactionModel toModel(Transaction entity);
}
