/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.repository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.payulatam.prototype.application.dataprovider.database.model.OrderModel;

/**
 * Provides CRUD functionality for the OrderModel class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Repository
public interface OrderRepository extends CrudRepository<OrderModel, UUID> {

	/**
	 * Find all the orders for an user.
	 *
	 * @param user The id of the user.
	 * @return All the orders of the user.
	 */
	@Query(value = "Select A.orders from UserModel A where A.email = (:user)")
	Optional<List<OrderModel>> findAllByEmail(String user);

	/**
	 * Find all the orders
	 *
	 * @return A list of all the orders
	 */
	List<OrderModel> findAll();

	/**
	 * Finds an order with the given id
	 *
	 * @param id Order id
	 * @return The order with the given id
	 */
	Optional<OrderModel> findAllById(String id);
}
