/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter;

import java.util.List;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.dataprovider.adapter.converter.constrains.EntityMapper;
import com.payulatam.prototype.application.dataprovider.database.model.ProductModel;

/**
 * Mapper that convert a {@link ProductModel} to {@link Product} or backwards
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "Spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface ProductModelMapper extends EntityMapper<ProductModel, Product> {

	/**
	 * Maps a {@link ProductModel} into a {@link Product} Customized mapping because soft deleted model should be a null entity.
	 *
	 * @param model Order model to map.
	 * @return Order mapped into a entity.
	 */
	@Override
	Product toEntity(ProductModel model);

	/**
	 * Maps a {@link Product} into a {@link ProductModel}.
	 *
	 * @param entity Product entity to map.
	 * @return Mapped product model.
	 */
	@Override
	@InheritInverseConfiguration
	ProductModel toModel(Product entity);

	/**
	 * Convert dto list to entity list
	 *
	 * @param modelList model list
	 * @return entity list
	 */
	List<Product> toEntityList(List<ProductModel> modelList);
}
