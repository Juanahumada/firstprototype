/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import java.util.Date;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * TokenizedCard model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
@Setter
@Entity
@Table(schema = "proto", name = "transaction")
@AllArgsConstructor
@NoArgsConstructor
public class TransactionModel {

	/**
	 * Transaction id
	 */
	@Id
	@Column(name = "id",
			updatable = false)
	private String id;
	/**
	 * Transaction order id
	 */
	@Column(name = "order_id")
	private String orderId;
	/**
	 * Transaction state
	 */
	@Column(name = "state")
	private String state;
	/**
	 * Transaction payment network response code
	 */
	@Column(name = "payment_network_response_code")
	private String paymentNetworkResponseCode;
	/**
	 * Transaction payment network response error message
	 */
	@Column(name = "payment_network_response_error_message")
	private String paymentNetworkResponseErrorMessage;
	/**
	 * Transaction traceabilityCode
	 */
	@Column(name = "traceability_code")
	private String traceabilityCode;
	/**
	 * Transaction authorization code
	 */
	@Column(name = "authorization_code")
	private String authorizationCode;
	/**
	 * Transaction response code
	 */
	@Column(name = "response_code")
	private String responseCode;
	/**
	 * Transaction response message
	 */
	@Column(name = "response_message")
	private String responseMessage;
	/**
	 * Transaction operation date
	 */
	@Column(name = "operation_date")
	private Date operationDate;
}
