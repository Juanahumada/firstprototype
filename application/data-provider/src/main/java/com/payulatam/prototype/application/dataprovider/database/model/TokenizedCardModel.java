/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * TokenizedCard model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@Entity
@Table(schema = "proto", name = "tokenized_card")
@NoArgsConstructor
public class TokenizedCardModel {

	/**
	 * Tokenized card token id
	 */
	@Id
	@Column(name = "credit_card_token_id",
			nullable = false)
	private String creditCardTokenId;
	/**
	 * Tokenized card name
	 */
	@Column(name = "name", nullable = false)
	private String name;
	/**
	 * Tokenized card payer id
	 */
	@Column(name = "payer_id", nullable = false)
	private String payerId;
	/**
	 * Tokenized card id number
	 */
	@Column(name = "identification_number", nullable = false)
	private String identificationNumber;
	/**
	 * Tokenized card payment method
	 */
	@Column(name = "payment_method", nullable = false)
	private String paymentMethod;
	/**
	 * Tokenized card masked number
	 */
	@Column(name = "maskedNumber", nullable = false)
	private String maskedNumber;
}
