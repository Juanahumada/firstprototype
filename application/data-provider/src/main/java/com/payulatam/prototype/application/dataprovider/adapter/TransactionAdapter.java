/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter;

import java.math.BigInteger;
import java.nio.charset.StandardCharsets;
import java.security.MessageDigest;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Component;
import feign.FeignException;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.ExternalTransactionApiException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.usecase.exception.TransactionNotFoundException;
import com.payulatam.prototype.application.dataprovider.adapter.client.PayUApiInfo;
import com.payulatam.prototype.application.dataprovider.adapter.client.PayUClient;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.Merchant;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.ping.PingRequest;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.tokenization.CreditCardToken;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.tokenization.TokenizeCardRequest;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.AdditionalValues;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Address;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Buyer;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Order;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Payer;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Tax;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.TransactionBaseRequest;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.newpayment.CreditCard;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.newpayment.NewCreditCardTransaction;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.refund.RefundOrder;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.refund.RefundTransaction;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.savedpayment.SavedCreditCardTransaction;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.ping.PingResponse;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.tokenization.TokenizeCardResponse;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.tokenization.TokenizedCreditCard;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.transaction.TransactionFinishedResponse;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.response.transaction.TransactionResponse;
import com.payulatam.prototype.application.dataprovider.adapter.converter.OrderModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.PaymentInfoModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.TransactionModelMapper;
import com.payulatam.prototype.application.dataprovider.adapter.converter.UserModelMapper;
import com.payulatam.prototype.application.dataprovider.database.model.OrderModel;
import com.payulatam.prototype.application.dataprovider.database.model.OrderProductModel;
import com.payulatam.prototype.application.dataprovider.database.model.OrderStatus;
import com.payulatam.prototype.application.dataprovider.database.model.PaymentInfoModel;
import com.payulatam.prototype.application.dataprovider.database.model.ProductModel;
import com.payulatam.prototype.application.dataprovider.database.model.TransactionModel;
import com.payulatam.prototype.application.dataprovider.database.model.TransactionModel.TransactionModelBuilder;
import com.payulatam.prototype.application.dataprovider.database.model.UserModel;
import com.payulatam.prototype.application.dataprovider.database.repository.OrderRepository;
import com.payulatam.prototype.application.dataprovider.database.repository.ProductRepository;
import com.payulatam.prototype.application.dataprovider.database.repository.TransactionRepository;

/**
 * Adapts the TransactionModel object to the entity Transaction object for the core.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class TransactionAdapter {

	/**
	 * Order repository supplier.
	 */
	private final OrderRepository orderRepository;
	/**
	 * Product repository supplier.
	 */
	private final ProductRepository productRepository;

	/**
	 * Transaction repository supplier.
	 */
	private final TransactionRepository transactionRepository;

	/**
	 * Feign client to get information from PayU api.
	 */
	private final PayUClient payUClient;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final UserModelMapper userModelMapper;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final OrderModelMapper orderModelMapper;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final PaymentInfoModelMapper paymentInfoModelMapper;

	/**
	 * Mapper used to map between core model and data-provider model.
	 */
	private final TransactionModelMapper transactionModelMapper;

	/**
	 * Method that tokenize a given credit card.
	 *
	 * @param user           Id of the user
	 * @param newPaymentInfo The information of the credit card.
	 * @return The card tokenized.
	 * @throws PrototypeException When the communication with the api fails.
	 */
	public TokenizedCard tokenizeCreditCard(final User user, final NewPaymentInfo newPaymentInfo) throws PrototypeException {

		try {
			final UserModel userModel = userModelMapper.toModel(user);
			final TokenizeCardRequest request = TokenizeCardRequest.builder()
																   .language(PayUApiInfo.DEFAULT_LANGUAGE.getDetail())
																   .command(PayUApiInfo.TOKENIZE_CARD_COMMAND.getDetail())
																   .merchant(createPayUMerchant())
																   .creditCardToken(createCreditCardToken(userModel,
																										  newPaymentInfo)).build();
			final TokenizeCardResponse response = payUClient.tokenizeCreditCard(request);
			return processTokenizeCardResponse(response);
		} catch (FeignException e) {
			throw new ExternalTransactionApiException("Tokenization process failed.");
		}

	}

	/**
	 * Process the response and returns a tokenized card
	 *
	 * @param response The response of the PayU Api to be processed
	 * @return The Tokenized Card.
	 * @throws PrototypeException When the api does not answer with a tokenized credit card
	 */
	private TokenizedCard processTokenizeCardResponse(final TokenizeCardResponse response) throws PrototypeException {

		if (response.getCode().equals(PayUApiInfo.CODE_SUCCESS.getDetail())) {
			TokenizedCreditCard tokenizedCreditCard = response.getCreditCardToken();
			return TokenizedCard.builder()
								.creditCardTokenId(tokenizedCreditCard.getCreditCardTokenId())
								.name(tokenizedCreditCard.getName())
								.payerId(tokenizedCreditCard.getPayerId())
								.identificationNumber(tokenizedCreditCard.getIdentificationNumber())
								.paymentMethod(tokenizedCreditCard.getPaymentMethod())
								.maskedNumber(tokenizedCreditCard.getMaskedNumber())
								.build();
		} else {
			throw new ExternalTransactionApiException("Error received from the tokenization api.");
		}
	}

	/**
	 * Creates a credit card token for the PayU api.
	 *
	 * @param user           User that has the new payment info
	 * @param newPaymentInfo Information of the new payment info
	 * @return The credit card token to be tokenized by the PayU api
	 */
	private CreditCardToken createCreditCardToken(final UserModel user, final NewPaymentInfo newPaymentInfo) {

		return CreditCardToken.builder()
							  .payerId(user.getEmail())
							  .name(newPaymentInfo.getCreditCardName())
							  .identificationNumber(newPaymentInfo.getCreditCardUserDni())
							  .paymentMethod(newPaymentInfo.getPaymentMethod())
							  .number(newPaymentInfo.getCreditCardNumber())
							  .expirationDate(newPaymentInfo.getCreditCardExpirationDate())
							  .build();
	}

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the refund.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	public Transaction reverseTransaction(final com.payulatam.prototype.application.core.usecase.entity.Order orderId,
								   final Transaction transactionId, final String reason) throws PrototypeException {

		try {
			final OrderModel orderModel = orderModelMapper.toModel(orderId);
			final TransactionModel transactionModel = transactionModelMapper.toModel(transactionId);
			final RefundOrder refundOrder = RefundOrder.builder()
													   .id(transactionModel.getOrderId())
													   .build();
			final RefundTransaction refundTransaction = RefundTransaction.builder()
																		 .order(refundOrder)
																		 .type(PayUApiInfo.TRANSACTION_TYPE_REFUND.getDetail())
																		 .reason(reason)
																		 .parentTransactionId(transactionModel.getId())
																		 .build();
			final TransactionBaseRequest transactionRequest = TransactionBaseRequest.builder()
																					.language(
																							PayUApiInfo.DEFAULT_LANGUAGE
																									.getDetail())
																					.command(
																							PayUApiInfo.SUBMIT_TRANSACTION_COMMAND
																									.getDetail())
																					.merchant(
																							createPayUMerchant())
																					.transaction(
																							refundTransaction)
																					.test(true)
																					.build();
			final TransactionFinishedResponse transactionResponse = payUClient.makeTransaction(transactionRequest);
			return transactionModelMapper.toEntity(processTransactionResult(orderModel, transactionResponse));
		} catch (FeignException e) {
			throw new ExternalTransactionApiException("Error in the API connection for reverse the transaction.");

		}
	}

	/**
	 * Method that makes a transaction with a saved payment info.
	 *
	 * @param user        Id of the user.
	 * @param order       Id of the order.
	 * @param paymentInfo Information of the payment method.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	public Transaction makeTransactionWithSavedPaymentInfo(final User user,
													final com.payulatam.prototype.application.core.usecase.entity.Order order,
													final SavedPaymentInfo paymentInfo) throws PrototypeException {

		try {
			final UserModel userModel = userModelMapper.toModel(user);
			final OrderModel orderModel = orderModelMapper.toModel(order);
			final PaymentInfoModel paymentInfoModel = paymentInfoModelMapper.toModel(paymentInfo);
			final SavedCreditCardTransaction savedCreditCardTransaction = createsAuthAndCapSavedCreditCardTransaction(
					createPayUOrder(orderModel),
					createSavedPayUPayer(userModel,
										 paymentInfoModel),
					createPayUExtraParameters(),
					paymentInfoModel);
			final TransactionBaseRequest transactionRequest = TransactionBaseRequest.builder()
																					.language(
																							PayUApiInfo.DEFAULT_LANGUAGE
																									.getDetail())
																					.command(
																							PayUApiInfo.SUBMIT_TRANSACTION_COMMAND
																									.getDetail())
																					.merchant(
																							createPayUMerchant())
																					.transaction(
																							savedCreditCardTransaction)
																					.test(true)
																					.build();
			final TransactionFinishedResponse transactionResponse = payUClient.makeTransaction(transactionRequest);
			return transactionModelMapper.toEntity(processTransactionResult(orderModel, transactionResponse));
		} catch (FeignException e) {
			throw new ExternalTransactionApiException("Error in the API connection for make a transaction with saved payment info.");
		}
	}

	/**
	 * Method that makes a transaction with a new payment info.
	 *
	 * @param user           Id of the user.
	 * @param order          Id of the order.
	 * @param newPaymentInfo Information of the payment method.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	public Transaction makeTransactionWithNewPaymentInfo(final User user,
												  final com.payulatam.prototype.application.core.usecase.entity.Order order,
												  final NewPaymentInfo newPaymentInfo) throws PrototypeException {

		try {
			final UserModel userModel = userModelMapper.toModel(user);
			final OrderModel orderModel = orderModelMapper.toModel(order);
			final NewCreditCardTransaction newCreditCardTransaction = createsAuthAndCapNewCreditCardTransaction(
					createPayUOrder(orderModel),
					createPayUPayer(userModel,
									newPaymentInfo),
					createPayUCreditCard(newPaymentInfo),
					createPayUExtraParameters(),
					newPaymentInfo);
			final TransactionBaseRequest transactionRequest = TransactionBaseRequest.builder()
																					.language(
																							PayUApiInfo.DEFAULT_LANGUAGE
																									.getDetail())
																					.command(
																							PayUApiInfo.SUBMIT_TRANSACTION_COMMAND
																									.getDetail())
																					.merchant(
																							createPayUMerchant())
																					.transaction(
																							newCreditCardTransaction)
																					.test(true)
																					.build();
			final TransactionFinishedResponse transactionResponse = payUClient.makeTransaction(transactionRequest);
			return transactionModelMapper.toEntity(processTransactionResult(orderModel, transactionResponse));
		} catch (FeignException e) {
			throw new ExternalTransactionApiException("Error in the API connection for make a transaction with new payment info.");
		}

	}

	/**
	 * Process the transaction result and saves it.
	 *
	 * @param orderModel          Order that is associated with the transaction.
	 * @param transactionResponse The transaction response obtained from the api.
	 */
	private TransactionModel processTransactionResult(final OrderModel orderModel, final TransactionFinishedResponse transactionResponse) throws PrototypeException{

		final TransactionModelBuilder transactionModel = TransactionModel.builder();
		final TransactionResponse response = transactionResponse.getTransactionResponse();
		if(response == null){
			throw new ExternalTransactionApiException(transactionResponse.getError());
		}
		processResponseCode(transactionModel, orderModel, transactionResponse);
		if (PayUApiInfo.TRANSACTION_STATUS_PENDING.getDetail().equals(response.getState())) {
			transactionModel.id(PayUApiInfo.TRANSACTION_TYPE_REFUND.getDetail() + response.getOrderId());
			orderModel.setStatus(OrderStatus.PROCESSING.getDetail());
		} else {
			orderModel.setStatus(OrderStatus.CLOSED.getDetail());
		}
		if (PayUApiInfo.TRANSACTION_STATUS_DECLINED.getDetail().equals((response.getState()))) {
			reverseOrderStock(orderModel);
		}
		orderModel.addTransaction(transactionModel.build());
		orderRepository.save(orderModel);
		return transactionModel.build();
	}

	/**
	 * Process the transaction code and assign additional values.
	 *
	 * @param transactionModel    Transaction model builder
	 * @param orderModel          Order model
	 * @param transactionResponse Transaction response
	 */
	private void processResponseCode(final TransactionModelBuilder transactionModel, final OrderModel orderModel,
									 final TransactionFinishedResponse transactionResponse) {

		final TransactionResponse response = transactionResponse.getTransactionResponse();
		if (PayUApiInfo.CODE_SUCCESS.getDetail().equals(transactionResponse.getCode())) {
			final Date operationDate = new Date(response.getOperationDate());

			if (PayUApiInfo.RESPONSE_CODE_INVALID_TRANSACTION.getDetail().equals(response.getResponseCode())) {

				final String id =
						PayUApiInfo.RESPONSE_CODE_INVALID_TRANSACTION.getDetail() + orderModel.getId() + orderModel.getTransactions()
																												   .size();
				transactionModel.id(id);
			} else {
				transactionModel.id(response.getTransactionId());
			}
			transactionModel.orderId(response.getOrderId())
							.state(response.getState())
							.paymentNetworkResponseCode(response.getPaymentNetworkResponseCode())
							.paymentNetworkResponseErrorMessage(response.getPaymentNetworkResponseErrorMessage())
							.traceabilityCode(response.getTrazabilityCode())
							.authorizationCode(response.getAuthorizationCode())
							.responseCode(response.getResponseCode())
							.responseMessage(response.getResponseMessage())
							.operationDate(operationDate);
		} else {
			transactionModel.responseCode(transactionResponse.getCode())
							.responseMessage(transactionResponse.getError())
							.id(transactionResponse.getCode() + orderModel.getId());
		}
	}

	/**
	 * Reverse the product stock discount when the order is Declined.
	 *
	 * @param orderModel Information about the order.
	 */
	private void reverseOrderStock(final OrderModel orderModel) {

		final List<OrderProductModel> products = orderModel.getProducts();
		products.forEach(product -> {
			final Optional<ProductModel> optionalProductModel = productRepository.findById(product.getProduct().getId());
			if (optionalProductModel.isPresent()) {
				final ProductModel productModel = optionalProductModel.get();
				productModel.updateStock(product.getQuantity(), true);
				productRepository.save(productModel);
			}
		});
	}

	/**
	 * Creates extra parameters for PayU Transaction Api
	 *
	 * @return The Extra parameters
	 */
	private Map<String, Object> createPayUExtraParameters() {

		Map<String, Object> map = new HashMap<>();
		map.put("INSTALLMENTS_NUMBER", 1);
		return map;
	}

	/**
	 * Creates a Credit card object for the PayU api transaction
	 *
	 * @param newPaymentInfo All the information of the credit card
	 * @return New Credit Card object with all the given info
	 */
	private CreditCard createPayUCreditCard(final NewPaymentInfo newPaymentInfo) {

		return CreditCard.builder()
						 .number(newPaymentInfo.getCreditCardNumber())
						 .securityCode(newPaymentInfo.getCreditCardSecurityCode())
						 .expirationDate(newPaymentInfo.getCreditCardExpirationDate())
						 .name(newPaymentInfo.getCreditCardName())
						 .build();
	}

	/**
	 * Creates a transaction with the given information.
	 *
	 * @param order           Order of the transaction
	 * @param payer           Payer of the transaction
	 * @param creditCard      Credit card of the transaction
	 * @param extraParameters Extra parameters of the transaction
	 * @param newPaymentInfo  Additional payment info of the transaction.
	 * @return The transaction created.
	 */
	private NewCreditCardTransaction createsAuthAndCapNewCreditCardTransaction(final Order order, final Payer payer,
																			   final CreditCard creditCard,
																			   final Map<String, Object> extraParameters,
																			   final NewPaymentInfo newPaymentInfo) {

		return NewCreditCardTransaction.builder().order(order).payer(payer).creditCard(creditCard).extraParameters(extraParameters)
									   .type(PayUApiInfo.TRANSACTION_TYPE_AUTHORIZATION_CAPTURE.getDetail())
									   .paymentMethod(newPaymentInfo.getPaymentMethod())
									   .paymentCountry(payer.getBillingAddress().getCountry())
									   .deviceSessionId(PayUApiInfo.DEVICE_SESSION_ID.getDetail())
									   .ipAddress(PayUApiInfo.DEVICE_IP_ADDRESS.getDetail())
									   .cookie(PayUApiInfo.DEVICE_COOKIE.getDetail())
									   .userAgent(PayUApiInfo.DEVICE_USER_AGENT.getDetail())
									   .build();
	}

	/**
	 * Creates a transaction with the given information.
	 *
	 * @param order            Order of the transaction
	 * @param payer            Payer of the transaction
	 * @param extraParameters  Extra parameters of the transaction
	 * @param paymentInfoModel Additional payment info of the transaction.
	 * @return The transaction created.
	 */
	private SavedCreditCardTransaction createsAuthAndCapSavedCreditCardTransaction(final Order order, final Payer payer,
																				   final Map<String, Object> extraParameters,
																				   final PaymentInfoModel paymentInfoModel) {

		return SavedCreditCardTransaction.builder()
										 .order(order)
										 .payer(payer)
										 .creditCardTokenId(paymentInfoModel.getTokenizedCard().getCreditCardTokenId())
										 .extraParameters(extraParameters)
										 .type(PayUApiInfo.TRANSACTION_TYPE_AUTHORIZATION_CAPTURE.getDetail())
										 .paymentMethod(paymentInfoModel.getTokenizedCard().getPaymentMethod())
										 .paymentCountry(payer.getBillingAddress().getCountry())
										 .deviceSessionId(PayUApiInfo.DEVICE_SESSION_ID.getDetail())
										 .ipAddress(PayUApiInfo.DEVICE_IP_ADDRESS.getDetail())
										 .cookie(PayUApiInfo.DEVICE_COOKIE.getDetail())
										 .userAgent(PayUApiInfo.DEVICE_USER_AGENT.getDetail())
										 .build();
	}

	/**
	 * Creates an order for PayU Api request
	 *
	 * @param order The order data.
	 * @return The PayU transaction order.
	 */
	private Order createPayUOrder(final OrderModel order) {

		return Order.builder()
					.accountId(PayUApiInfo.ACCOUNT_ID.getDetail())
					.referenceCode("BUY_" + order.getId())
					.description("BUY_" + order.getId() + "_DESCRIPTION")
					.language(PayUApiInfo.DEFAULT_LANGUAGE.getDetail())
					.signature(createSignature(order))
					.notifyUrl(PayUApiInfo.NOTIFY_URL.getDetail())
					.additionalValues(createOrderAdditionalValues(order))
					.buyer(createPayUBuyer())
					.shippingAddress(createBuyerShippingAddress())
					.build();
	}

	/**
	 * Creates a saved payer for the PayU api
	 *
	 * @param user             Payer information
	 * @param paymentInfoModel Payer address additional info
	 * @return The new payer created with the given data.
	 */
	private Payer createSavedPayUPayer(final UserModel user, final PaymentInfoModel paymentInfoModel) {

		return Payer.builder()
					.merchantPayerId(user.getEmail())
					.fullName(user.getName() + " " + user.getLastName())
					.emailAddress(user.getEmail())
					.contactPhone(user.getPhone())
					.dniNumber(user.getDni())
					.billingAddress(createSavedPayerBillingAddress(paymentInfoModel))
					.build();
	}

	/**
	 * Creates a payer for the PayU api
	 *
	 * @param user           Payer information
	 * @param newPaymentInfo Payer address additional info
	 * @return The new payer created with the given data.
	 */
	private Payer createPayUPayer(final UserModel user, final NewPaymentInfo newPaymentInfo) {

		return Payer.builder()
					.merchantPayerId(user.getEmail())
					.fullName(user.getName() + " " + user.getLastName())
					.emailAddress(user.getEmail())
					.contactPhone(user.getPhone())
					.dniNumber(user.getDni())
					.billingAddress(createPayerBillingAddress(newPaymentInfo))
					.build();
	}

	/**
	 * Creates a payer billing address
	 *
	 * @param newPaymentInfo Payment info that holds all the address information
	 * @return The billing address created with the given information
	 */
	private Address createPayerBillingAddress(final NewPaymentInfo newPaymentInfo) {

		return Address.builder()
					  .street1(newPaymentInfo.getBillingAddressStreet1())
					  .street2(newPaymentInfo.getBillingAddressStreet2())
					  .city(newPaymentInfo.getBillingAddressCity())
					  .state(newPaymentInfo.getBillingAddressState())
					  .country(newPaymentInfo.getBillingAddressCountry())
					  .postalCode(newPaymentInfo.getBillingAddressPostalCode())
					  .phone(newPaymentInfo.getBillingAddressPostalCode())
					  .build();
	}

	/**
	 * Creates a saves payer billing address
	 *
	 * @param paymentInfoModel Payment info that holds all the address information
	 * @return The billing address created with the given information
	 */
	private Address createSavedPayerBillingAddress(final PaymentInfoModel paymentInfoModel) {

		return Address.builder()
					  .street1(paymentInfoModel.getBillingAddressStreet1())
					  .street2(paymentInfoModel.getBillingAddressStreet2())
					  .city(paymentInfoModel.getBillingAddressCity())
					  .state(paymentInfoModel.getBillingAddressState())
					  .country(paymentInfoModel.getBillingAddressCountry())
					  .postalCode(paymentInfoModel.getBillingAddressPostalCode())
					  .phone(paymentInfoModel.getBillingAddressPostalCode())
					  .build();
	}

	/**
	 * Create buyer for PayU api
	 *
	 * @return The payu buyer
	 */
	private Buyer createPayUBuyer() {

		return Buyer.builder()
					.merchantBuyerId(PayUApiInfo.MERCHANT_BUYER_ID.getDetail())
					.fullName(PayUApiInfo.MERCHANT_FULL_NAME.getDetail())
					.emailAddress(PayUApiInfo.MERCHANT_EMAIL.getDetail())
					.contactPhone(PayUApiInfo.MERCHANT_PHONE.getDetail())
					.dniNumber(PayUApiInfo.MERCHANT_DNI.getDetail())
					.shippingAddress(createBuyerShippingAddress())
					.build();
	}

	/**
	 * Creates a buyer shipping address for PayU api
	 *
	 * @return The buyer shipping address.
	 */
	private Address createBuyerShippingAddress() {

		return Address.builder()
					  .street1(PayUApiInfo.MERCHANT_STREET1.getDetail())
					  .street2(PayUApiInfo.MERCHANT_STREET2.getDetail())
					  .city(PayUApiInfo.MERCHANT_CITY.getDetail())
					  .state(PayUApiInfo.MERCHANT_STATE.getDetail())
					  .country(PayUApiInfo.MERCHANT_COUNTRY.getDetail())
					  .postalCode(PayUApiInfo.MERCHANT_POSTAL_CODE.getDetail())
					  .phone(PayUApiInfo.MERCHANT_PHONE.getDetail())
					  .build();
	}

	/**
	 * Creates the additional values parameters for an order
	 *
	 * @param order The info of the order
	 * @return The additional values of the order
	 */
	private AdditionalValues createOrderAdditionalValues(final OrderModel order) {

		final Tax txValue = Tax.builder()
							   .value(order.getTotalValue())
							   .currency(PayUApiInfo.DEFAULT_CURRENCY.getDetail())
							   .build();
		return AdditionalValues.builder()
							   .txValue(txValue)
							   .build();
	}

	/**
	 * Creates the signature for the order applying the MD5 algorithm to the: ApiKey, MerchantId, ReferenceCode, OrderValue and Currency
	 * joined by ~
	 *
	 * @param order The order with the id and the total value.
	 * @return The string result of the MD5 algorithm.
	 */
	private String createSignature(OrderModel order) {

		final String signature = String
				.join("~", PayUApiInfo.API_KEY.getDetail(), PayUApiInfo.MERCHANT_ID.getDetail(), "BUY_" + order.getId(),
					  order.getTotalValue().toString(), PayUApiInfo.DEFAULT_CURRENCY.getDetail());
		final byte[] signatureBytes = signature.getBytes(StandardCharsets.UTF_8);
		try {
			final MessageDigest messageDigest = MessageDigest.getInstance("MD5");
			return String.format("%032X", new BigInteger(1, messageDigest.digest(signatureBytes)));
		} catch (Exception e) {
			return StringUtils.EMPTY;
		}
	}

	/**
	 * Creates a PayU merchant for the api request
	 *
	 * @return PayU merchant
	 */
	private Merchant createPayUMerchant() {

		return Merchant.builder().apiKey(PayUApiInfo.API_KEY.getDetail()).apiLogin(PayUApiInfo.API_LOGIN.getDetail()).build();
	}

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	public String ping() {

		final PingRequest pingRequest =
				PingRequest.builder().test(false).language("es").command(PayUApiInfo.PING_COMMAND.getDetail())
						   .merchant(createPayUMerchant())
						   .build();
		final PingResponse pingResponse = payUClient.ping(pingRequest);
		if (StringUtils.isEmpty(pingResponse.getError())) {
			return StringUtils.EMPTY;
		} else {
			return pingResponse.getError();
		}

	}

	/**
	 * Method that get a transaction with a given id.
	 *
	 * @param transactionId Id of the transaction
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	public Transaction getTransactionWithId(final String transactionId) throws PrototypeException {

		final Optional<TransactionModel> transaction = transactionRepository.findAllById(transactionId);
		return transaction.map(transactionModelMapper::toEntity)
						  .orElseThrow(() -> new TransactionNotFoundException("The transaction with the given Id does not exists."));
	}
}
