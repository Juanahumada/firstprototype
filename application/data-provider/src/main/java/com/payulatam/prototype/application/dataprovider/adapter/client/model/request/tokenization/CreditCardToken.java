/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.tokenization;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the Credit card information to be tokenized in the PayU API 'TokenizeCard'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CreditCardToken {

	/**
	 * Payer id
	 */
	@JsonProperty("payerId")
	private String payerId;
	/**
	 * Credit card name
	 */
	@JsonProperty("name")
	private String name;
	/**
	 * User identification number
	 */
	@JsonProperty("identificationNumber")
	private String identificationNumber;
	/**
	 * Card payment method
	 */
	@JsonProperty("paymentMethod")
	private String paymentMethod;
	/**
	 * Credit card number
	 */
	@JsonProperty("number")
	private String number;
	/**
	 * Credit card expiration date
	 */
	@JsonProperty("expirationDate")
	private String expirationDate;

}

