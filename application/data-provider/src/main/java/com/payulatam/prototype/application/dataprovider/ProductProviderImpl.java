/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.dataprovider.adapter.ProductAdapter;

/**
 * Implementation of {@link ProductProvider} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ProductProviderImpl implements ProductProvider {

	/**
	 * Provider for products.
	 */
	private final ProductAdapter adapter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Product> getAllProducts() {

		return adapter.getAllProducts();
	}

	/**
	 * Method that gets a product with a given id.
	 *
	 * @param productId Id of the product
	 * @return The product with the given Id.
	 * @throws PrototypeException When the product does not exists.
	 */
	@Override public Product getProductWithId(final String productId) throws PrototypeException {

		return adapter.getProductWithId(productId);
	}

	/**
	 * Method that saves a product.
	 *
	 * @param product Product to be saved.
	 */
	@Override public void saveProduct(final Product product) {

		adapter.saveProduct(product);
	}

	/**
	 * Updates the stock of a given product.
	 *
	 * @param product  The product.
	 * @param stock    The quantity to be reduced or increased.
	 * @param increase True if the stock has to be increased or false if it has to be reduced.
	 */
	@Override public void updateStock(final Product product, final int stock, final boolean increase) {

		adapter.updateStock(product, stock, increase);
	}

	/**
	 * Method that delete a given product.
	 *
	 * @param productId Product to be deleted.
	 */
	@Override public void deleteProduct(final String productId) {

		adapter.deleteProduct(productId);
	}
}
