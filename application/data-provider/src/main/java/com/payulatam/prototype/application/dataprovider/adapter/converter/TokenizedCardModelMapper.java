/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.dataprovider.adapter.converter.constrains.EntityMapper;
import com.payulatam.prototype.application.dataprovider.database.model.TokenizedCardModel;

/**
 * Mapper that convert a {@link TokenizedCardModel} to {@link TokenizedCard} or backwards
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "Spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface TokenizedCardModelMapper extends EntityMapper<TokenizedCardModel, TokenizedCard> {

	/**
	 * Maps a {@link TokenizedCardModel} into a {@link TokenizedCard} Customized mapping because soft deleted model should be a null
	 * entity.
	 *
	 * @param model Tokenized card model to map.
	 * @return TokenizedCard mapped into a entity.
	 */
	@Override
	TokenizedCard toEntity(TokenizedCardModel model);

	/**
	 * Maps a {@link TokenizedCard} into a {@link TokenizedCardModel}.
	 *
	 * @param entity TokenizedCard entity to map.
	 * @return Mapped TokenizedCard model.
	 */
	@Override
	@InheritInverseConfiguration
	TokenizedCardModel toModel(TokenizedCard entity);
}
