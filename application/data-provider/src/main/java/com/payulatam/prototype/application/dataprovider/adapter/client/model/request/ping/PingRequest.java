/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.ping;

import org.codehaus.jackson.annotate.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.Merchant;

/**
 * Represent the request to PayU API 'ping'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class PingRequest {

	/**
	 * If it is test or not.
	 */
	@JsonProperty("test")
	private boolean test;
	/**
	 * Language used in the request
	 */
	@JsonProperty("language")
	private String language;
	/**
	 * Command of the request
	 */
	@JsonProperty("command")
	private String command;
	/**
	 * Merchant additional data
	 */
	@JsonProperty("merchant")
	private Merchant merchant;
}

