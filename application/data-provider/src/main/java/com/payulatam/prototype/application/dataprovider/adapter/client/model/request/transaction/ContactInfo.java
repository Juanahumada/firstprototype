/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represent the Contact information to PayU API 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class ContactInfo {

	/**
	 * Contact info full name
	 */
	@JsonProperty("fullName")
	private String fullName;
	/**
	 * Contact info email address
	 */
	@JsonProperty("emailAddress")
	private String emailAddress;
	/**
	 * Contact info contact phone
	 */
	@JsonProperty("contactPhone")
	private String contactPhone;
	/**
	 * Contact info dni number
	 */
	@JsonProperty("dniNumber")
	private String dniNumber;
}
