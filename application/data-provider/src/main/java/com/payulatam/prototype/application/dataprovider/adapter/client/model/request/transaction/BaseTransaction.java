/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

/**
 * Represents the Transaction info to PayU API request 'TransactionWithNewPaymentInfo'.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BaseTransaction {

	/**
	 * Transaction prder data
	 */
	@JsonProperty("order")
	private Order order;
	/**
	 * Transaction payer data
	 */
	@JsonProperty("payer")
	private Payer payer;
	/**
	 * Additional parameters of the transaction
	 */
	@JsonProperty("extraParameters")
	private Map<String, Object> extraParameters;
	/**
	 * Type of the transaction
	 */
	@JsonProperty("type")
	private String type;
	/**
	 * Payment method of the transaction
	 */
	@JsonProperty("paymentMethod")
	private String paymentMethod;
	/**
	 * Payment country of the transaction
	 */
	@JsonProperty("paymentCountry")
	private String paymentCountry;
	/**
	 * Session id of the device where the transaction is requested
	 */
	@JsonProperty("deviceSessionId")
	private String deviceSessionId;
	/**
	 * Ip address of the device where the transaction is requested
	 */
	@JsonProperty("ipAddress")
	private String ipAddress;
	/**
	 * Cookie saved on the device where the transaction is requested
	 */
	@JsonProperty("cookie")
	private String cookie;
	/**
	 * User agent of the browser where the transaction is requested
	 */
	@JsonProperty("userAgent")
	private String userAgent;

}
