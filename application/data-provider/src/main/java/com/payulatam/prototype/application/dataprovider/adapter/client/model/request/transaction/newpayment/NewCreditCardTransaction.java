/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.newpayment;

import java.util.Map;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.ToString;

import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.BaseTransaction;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Order;
import com.payulatam.prototype.application.dataprovider.adapter.client.model.request.transaction.Payer;

/**
 * Transaction credit card request
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class NewCreditCardTransaction extends BaseTransaction {

	/**
	 * Transaction credit card info
	 */
	@JsonProperty("creditCard")
	private CreditCard creditCard;

	@Builder
	public NewCreditCardTransaction(final Order order,
									final Payer payer,
									final Map<String, Object> extraParameters, final String type, final String paymentMethod,
									final String paymentCountry, final String deviceSessionId, final String ipAddress, final String cookie,
									final String userAgent,
									final CreditCard creditCard) {

		super(order, payer, extraParameters, type, paymentMethod, paymentCountry, deviceSessionId, ipAddress, cookie, userAgent);
		this.creditCard = creditCard;
	}
}
