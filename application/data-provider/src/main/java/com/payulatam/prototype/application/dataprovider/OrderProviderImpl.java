/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.dataprovider;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.OrderProvider;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.dataprovider.adapter.OrderAdapter;

/**
 * Implementation of {@link OrderProvider} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class OrderProviderImpl implements OrderProvider {

	/**
	 * Provider for order products
	 */
	private final OrderAdapter adapter;

	/**
	 * Creates a new order to an user.
	 *
	 * @param user     User that is making the change.
	 * @param product  First product to add to the order.
	 * @param quantity Quantity of the product.
	 */
	@Override public void createOrder(final User user, final Product product, final Integer quantity) {

		adapter.createOrder(user, product, quantity);
	}

	/**
	 * Adds product to an order.
	 *
	 * @param order    Order that has to be modified.
	 * @param user     User that has the order to be modified.
	 * @param product  Product that has to been added.
	 * @param quantity Quantity of the product.
	 */
	@Override public void addProductToOrder(final Order order, final User user, final Product product, final Integer quantity) {

		adapter.addProductToOrder(order, user, product, quantity);
	}

	/**
	 * Gets all the orders in the database.
	 *
	 * @return A list of the orders in the store.
	 */
	@Override public List<Order> getAllOrders() {

		return adapter.getAllOrders();
	}

	/**
	 * Method that search all the orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the orders in the store for the user.
	 */
	@Override public List<Order> getAllUserOrders(final String userId) {

		return adapter.getAllUserOrders(userId);
	}

	/**
	 * Method that search all the open orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the open orders in the store for the user.
	 */
	@Override public List<Order> getAllOpenUserOrders(final String userId) {

		return adapter.getAllOpenUserOrders(userId);
	}

	/**
	 * Get an order with the given id
	 *
	 * @param id The id of the order
	 * @return The order with the given id
	 * @throws PrototypeException When the order does not exists.
	 */
	@Override
	public Order getOrderById(final String id) throws PrototypeException {

		return adapter.getOrderById(id);
	}
}
