/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.database.model;

import java.util.ArrayList;
import java.util.List;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * User model definition
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Setter
@Entity
@Table(schema = "proto", name = "user")
@NoArgsConstructor
public class UserModel {

	/**
	 * User id.
	 */
	@Id
	@Column(name = "id",
			updatable = false)
	private String email;

	/**
	 * User dni.
	 */
	@Column(name = "dni", nullable = false)
	private String dni;

	/**
	 * User name.
	 */
	@Column(name = "name", nullable = false)
	private String name;

	/**
	 * User last name.
	 */
	@Column(name = "last_name", nullable = false)
	private String lastName;

	/**
	 * User phone
	 */
	@Column(name = "phone", nullable = false)
	private String phone;

	/**
	 * User password
	 */
	@Column(name = "password", nullable = false)
	private String password;

	/**
	 * User status
	 */
	@Column(name = "enabled", nullable = false)
	private Boolean enabled;

	/**
	 * User roles
	 */
	@ManyToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private List<RoleModel> roles;

	/**
	 * List of the payment information that the owner has.
	 */
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private List<PaymentInfoModel> paymentsInfo;

	/**
	 * Method that adds a new paymentInfoModel to an user.
	 *
	 * @param paymentInfoModel Information of the new payment info model
	 */
	public void addPaymentInfo(final PaymentInfoModel paymentInfoModel) {

		if (this.paymentsInfo == null) {
			final List<PaymentInfoModel> paymentInfoModelList = new ArrayList<>();
			paymentInfoModelList.add(paymentInfoModel);
			this.paymentsInfo = paymentInfoModelList;
		} else {
			this.paymentsInfo.add(paymentInfoModel);
		}
	}

	/**
	 * List of the orders of the user.
	 */
	@OneToMany(fetch = FetchType.LAZY,
			cascade = CascadeType.ALL)
	private List<OrderModel> orders;

	/**
	 * Method that adds a new order to an user.
	 *
	 * @param orderModel Information of the new order
	 */
	public void addOrder(final OrderModel orderModel) {

		if (this.orders == null) {
			final List<OrderModel> orderModelList = new ArrayList<>();
			orderModelList.add(orderModel);
			this.orders = orderModelList;
		} else {
			this.orders.add(orderModel);
		}
	}

	/**
	 * Method that adds a new order to an user.
	 *
	 * @param orderModel Information of the new order
	 */
	public void addProductToOrder(final OrderModel orderModel, final OrderProductModel.OrderProductModelBuilder orderProductModelBuilder) {

		if (this.orders != null) {
			var a =
					this.orders.stream().filter(p -> p.getId().equals(orderModel.getId()))
							   .findFirst();
			a.ifPresent(model -> model.addProduct(orderProductModelBuilder));
		}
	}
}
