/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.dataprovider.adapter.converter;

import org.mapstruct.InheritInverseConfiguration;
import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.NullValuePropertyMappingStrategy;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.dataprovider.adapter.converter.constrains.EntityMapper;
import com.payulatam.prototype.application.dataprovider.database.model.PaymentInfoModel;

/**
 * Mapper that convert a {@link PaymentInfoModel} to {@link SavedPaymentInfo} or backwards
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "Spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR,
		nullValuePropertyMappingStrategy = NullValuePropertyMappingStrategy.IGNORE)
public interface PaymentInfoModelMapper extends EntityMapper<PaymentInfoModel, SavedPaymentInfo> {

	/**
	 * Maps a {@link PaymentInfoModel} into a {@link SavedPaymentInfo} Customized mapping because soft deleted model should be a null
	 * entity.
	 *
	 * @param model Order model to map.
	 * @return Order mapped into a entity.
	 */
	@Override
	SavedPaymentInfo toEntity(PaymentInfoModel model);

	/**
	 * Maps a {@link SavedPaymentInfo} into a {@link PaymentInfoModel}.
	 *
	 * @param entity Product entity to map.
	 * @return Mapped product model.
	 */
	@Override
	@InheritInverseConfiguration
	PaymentInfoModel toModel(SavedPaymentInfo entity);

}
