/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.MakeTransactionNewPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.OrderValidator;
import com.payulatam.prototype.application.core.validator.TransactionValidator;

/**
 * Implementation of {@link MakeTransactionNewPaymentInfoUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class MakeTransactionNewPaymentInfoUseCaseImpl implements MakeTransactionNewPaymentInfoUseCase {

	/**
	 * Provider for make transaction operation.
	 */
	private final TransactionProvider transactionProvider;

	/**
	 * Read user use case
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Read order use case
	 */
	private final ReadOrderUseCase readOrderUseCase;

	/**
	 * Makes a transaction with a new payment info.
	 *
	 * @param user           Id of the user that is creating the order.
	 * @param order          Id of the order.
	 * @param newPaymentInfo Information of the new payment.
	 * @throws PrototypeException When the user or the order do not exists.
	 */
	@Override public Transaction makeTransactionWithNewPaymentInfo(final String user, final String order, final NewPaymentInfo newPaymentInfo)
			throws
			PrototypeException {

		final User us = readUserUseCase.getUserByEmail(user);
		final Order or = readOrderUseCase.getOrderById(order);
		OrderValidator.verifyOpenOrder(or);
		TransactionValidator.verifyTransactionNewPaymentMethod(or, us);
		return transactionProvider.makeTransactionWithNewPaymentInfo(us, or, newPaymentInfo);
	}
}
