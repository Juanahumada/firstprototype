/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import java.math.BigDecimal;
import java.util.List;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the order object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class Order {

	/**
	 * Order id
	 */
	private final String id;

	/**
	 * Order total value
	 */
	private final BigDecimal totalValue;

	/**
	 * Products of the order
	 */
	private final List<OrderProduct> products;

	/**
	 * Transactions associated with the order
	 */
	private final List<Transaction> transactions;

	/**
	 * Order status
	 */
	private final String status;
}
