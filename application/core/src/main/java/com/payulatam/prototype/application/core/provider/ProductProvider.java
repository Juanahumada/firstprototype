/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.provider;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Interface that exposes products operations for the use cases: ReadProduct
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ProductProvider {

	/**
	 * Method that search all the products.
	 *
	 * @return All the products in the store.
	 */
	List<Product> getAllProducts();

	/**
	 * Method that gets a product with a given id.
	 *
	 * @param productId Id of the product
	 * @return The product with the given Id.
	 * @throws PrototypeException When the product does not exists.
	 */
	Product getProductWithId(String productId) throws PrototypeException;

	/**
	 * Method that saves a product.
	 *
	 * @param product Product to be saved.
	 */
	void saveProduct(Product product);

	/**
	 * Updates the stock of a given product.
	 *
	 * @param product  The product.
	 * @param stock    The quantity to be reduced or increased.
	 * @param increase True if the stock has to be increased or false if it has to be reduced.
	 */
	void updateStock(Product product, int stock, boolean increase);

	/**
	 * Method that delete a given product.
	 *
	 * @param productId Product to be deleted.
	 */
	void deleteProduct(String productId);
}
