/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.provider;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Interface that exposes user operations for the use cases: AuthenticateUser
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface UserProvider {

	/**
	 * Authenticates a user based on the username and password.
	 *
	 * @param username Username received.
	 * @return True if the user data is correct. Otherwise false.
	 * @throws PrototypeException When the user with the given Id does not exist
	 */
	User getUserByEmail(String username) throws PrototypeException;

	/**
	 * Saves the payment method in a user.
	 *
	 * @param user           Id of the user to save the new payment info
	 * @param newPaymentInfo The new payment info that needs to be saved
	 */
	void savePaymentInfo(User user, NewPaymentInfo newPaymentInfo, TokenizedCard tokenizedCard);

	/**
	 * Gets the payment info of the given user.
	 *
	 * @param user User received.
	 * @return The list of the saved payment info for the user
	 * @throws PrototypeException When the user does not exists.
	 */
	List<SavedPaymentInfo> getPaymentInfo(User user) throws PrototypeException;

	/**
	 * Gets the payment info with the given id
	 *
	 * @param paymentInfoId Payment info id
	 * @return The payment info with the given id.
	 * @throws PrototypeException When the payment information with the given id does not exists.
	 */
	SavedPaymentInfo getPaymentInfoById(String paymentInfoId) throws PrototypeException;
}
