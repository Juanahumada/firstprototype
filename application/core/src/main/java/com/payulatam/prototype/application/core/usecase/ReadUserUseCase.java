/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to authenticate user.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ReadUserUseCase {

	/**
	 * Find the user with the given username
	 *
	 * @param username Username provided.
	 * @return True if the authentication is valid, otherwise false.
	 * @throws PrototypeException When the user with the given Id does not exist
	 */
	User getUserByEmail(String username) throws PrototypeException;
}
