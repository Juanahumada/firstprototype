/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.ProductAlreadyExistsException;
import com.payulatam.prototype.application.core.usecase.exception.ProductNotStockException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Make the validations for the product.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class ProductValidator {

	/**
	 * Class Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(ProductValidator.class);

	/**
	 * Private constructor in order to hide implicit public one.
	 */
	private ProductValidator() {
		//Do nothing
	}

	/**
	 * Validate if a product already exists.
	 *
	 * @param product            Product to create.
	 * @param readProductUseCase Read product use case
	 */
	public static void verifyDuplicateProductCreation(final Product product,
													  final ReadProductUseCase readProductUseCase)
			throws PrototypeException {

		final List<Product> productList = readProductUseCase.getAllProducts();
		Optional<Product> optionalProduct =
				productList.stream()
						   .filter(p -> p.getId().equals(product.getId()) || p.getName().equals(product.getName()))
						   .findFirst();
		if (optionalProduct.isPresent()) {
			LOGGER.info("The product to be added to the store must be different of the other products.");
			throw new ProductAlreadyExistsException("The product to be added to the store must be different of the other products.");
		}
		LOGGER.debug("No duplicate product found, starting product creation.");
	}

	/**
	 * Verifies if the product has enough stock
	 *
	 * @param product  Product information
	 * @param required Stock required
	 * @throws PrototypeException If the product does not have enough stock
	 */
	public static void verifyProductStock(final Product product, final Integer required)
			throws PrototypeException {

		if (product.getStock() < required) {
			LOGGER.info("The product does not have enough stock as the required: Required: {}, Stock: {}", required,
						product.getStock());
			throw new ProductNotStockException("The product does not have enough stock as the required: Required: " + required.toString() +
													   ", Stock: " + product.getStock());
		}
		LOGGER.debug("The product has enough stock. Continue with the adding to the order process.");
	}

	/**
	 * Verifies if a product exists.
	 *
	 * @param productId          Product id
	 * @param readProductUseCase Read product use case
	 * @throws PrototypeException When the product does not exists.
	 */
	public static void verifyProductExistence(final String productId, final ReadProductUseCase readProductUseCase)
			throws PrototypeException {

		readProductUseCase.getProductWithId(productId);
	}

}
