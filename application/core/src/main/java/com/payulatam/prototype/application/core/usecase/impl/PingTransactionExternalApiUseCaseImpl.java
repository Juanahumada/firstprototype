/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.PingTransactionExternalApiUseCase;

/**
 * Implementation of {@link PingTransactionExternalApiUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class PingTransactionExternalApiUseCaseImpl implements PingTransactionExternalApiUseCase {

	/**
	 * Provider for make transaction operation.
	 */
	private final TransactionProvider transactionProvider;

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	public String ping() {

		return transactionProvider.ping();
	}
}
