/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.exception;

/**
 * Definition of product already exists exception.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class ProductAlreadyExistsException extends PrototypeException {

	/**
	 * Constructs a new exception with the specified detail message.  The cause is not initialized, and may subsequently be initialized by a
	 * call to {@link #initCause}.
	 *
	 * @param message the detail message. The detail message is saved for later retrieval by the {@link #getMessage()} method.
	 */
	public ProductAlreadyExistsException(String message) {

		super(message);
	}

	/**
	 * Constructs a new exception with the specified detail message and cause.  <p>Note that the detail message associated with {@code
	 * cause} is <i>not</i> automatically incorporated in this exception's detail message.
	 *
	 * @param message the detail message (which is saved for later retrieval by the {@link #getMessage()} method).
	 * @param cause   the cause (which is saved for later retrieval by the {@link #getCause()} method).  (A <tt>null</tt> value is
	 *                permitted, and indicates that the cause is nonexistent or unknown.)
	 * @since 1.4
	 */
	public ProductAlreadyExistsException(String message, Throwable cause) {

		super(message, cause);
	}
}
