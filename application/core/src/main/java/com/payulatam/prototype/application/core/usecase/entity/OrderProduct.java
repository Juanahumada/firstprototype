/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the products of the order object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class OrderProduct {

	/**
	 * Order id
	 */
	private final String id;

	/**
	 * Product reference
	 */
	private final Product product;
	/**
	 * Quantity of the product
	 */
	private final Integer quantity;
}
