/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to make a transaction with new payment info.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface MakeTransactionNewPaymentInfoUseCase {

	/**
	 * Makes a transaction with a new payment info.
	 *
	 * @param order          Id of the order.
	 * @param user           Id of the user that is creating the order.
	 * @param newPaymentInfo Information of the new payment.
	 * @throws PrototypeException When the user or the order do not exists.
	 */
	Transaction makeTransactionWithNewPaymentInfo(String user, String order, NewPaymentInfo newPaymentInfo) throws PrototypeException;
}
