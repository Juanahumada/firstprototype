/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Implementation of {@link ReadProductUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ReadProductUseCaseImpl implements ReadProductUseCase {

	/**
	 * Provider for get products.
	 */
	private final ProductProvider provider;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Product> getAllProducts() {

		return provider.getAllProducts();
	}

	/**
	 * Method that get a product with a given id.
	 *
	 * @param productId Id of the product
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	@Override public Product getProductWithId(final String productId) throws PrototypeException {

		return provider.getProductWithId(productId);
	}
}
