/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to tokenize a card.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface TokenizeCardUseCase {

	/**
	 * Method that sends a credit card and returns a tokenized card.
	 *
	 * @param newPaymentInfo All the information of the credit card.
	 * @param userId         Id of the user.
	 * @return The new card tokenized.
	 * @throws PrototypeException When the user does not exists.
	 */
	TokenizedCard tokenizeCard(String userId, NewPaymentInfo newPaymentInfo) throws PrototypeException;
}
