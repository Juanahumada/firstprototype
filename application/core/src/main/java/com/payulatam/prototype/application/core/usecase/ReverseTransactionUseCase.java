/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to reverse transaction.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ReverseTransactionUseCase {

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param userId        The id of the user that has the order.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the reund.
	 * @throws PrototypeException When the user, order or transaction does not exists.
	 */
	Transaction reverseTransaction(String orderId, String userId, String transactionId, String reason) throws PrototypeException;
}
