/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import java.math.BigDecimal;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the product object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class Product {

	/**
	 * Product id.
	 */
	private final String id;

	/**
	 * Product name.
	 */
	private final String name;

	/**
	 * Product description.
	 */
	private final String description;

	/**
	 * Product value.
	 */
	private final BigDecimal value;

	/**
	 * Product stock.
	 */
	private final Integer stock;

	/**
	 * Product photo
	 */
	private final String photo;
}
