/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the tokenized card object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class TokenizedCard {

	/**
	 * Tokenized card token id
	 */
	private final String creditCardTokenId;
	/**
	 * Tokenized card name
	 */
	private final String name;
	/**
	 * Tokenized card payer id
	 */
	private final String payerId;
	/**
	 * Tokenized card id number
	 */
	private final String identificationNumber;
	/**
	 * Tokenized card payment method
	 */
	private final String paymentMethod;
	/**
	 * Tokenized card masked number
	 */
	private final String maskedNumber;
}
