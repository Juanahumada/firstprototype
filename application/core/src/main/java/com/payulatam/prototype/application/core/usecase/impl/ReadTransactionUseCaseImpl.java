/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.ReadTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Implementation of {@link ReadTransactionUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ReadTransactionUseCaseImpl implements ReadTransactionUseCase {

	/**
	 * Provider for get transactions.
	 */
	private final TransactionProvider provider;

	/**
	 * Method that get a transaction with a given id.
	 *
	 * @param transactionId Id of the transaction
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	@Override public Transaction getTransactionWithId(final String transactionId) throws PrototypeException {

		return provider.getTransactionWithId(transactionId);
	}
}
