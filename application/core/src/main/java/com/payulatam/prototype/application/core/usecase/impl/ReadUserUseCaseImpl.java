/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.UserProvider;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Implementation of {@link ReadUserUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ReadUserUseCaseImpl implements ReadUserUseCase {

	/**
	 * Provider for authenticate user operation.
	 */
	private final UserProvider userProvider;

	/**
	 * {@inheritDoc}
	 */
	public User getUserByEmail(final String email) throws PrototypeException {

		return userProvider.getUserByEmail(email);
	}
}
