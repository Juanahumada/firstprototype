/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to save the payment info.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface SavePaymentInfoUseCase {

	/**
	 * Method that saves a new payment info.
	 *
	 * @param user           Id of the user.
	 * @param newPaymentInfo Information of the new payment.
	 * @throws PrototypeException When the user does not exists.
	 */
	void savePaymentInfo(NewPaymentInfo newPaymentInfo, String user) throws PrototypeException;
}
