/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import java.math.BigDecimal;
import java.util.Date;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the transaction object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class Transaction {

	/**
	 * Transaction id
	 */
	private final String id;
	/**
	 * Transaction order id
	 */
	private final String orderId;
	/**
	 * Transaction state
	 */
	private final String state;
	/**
	 * Transaction payment network response code
	 */
	private final String paymentNetworkResponseCode;
	/**
	 * Transaction payment network response error message
	 */
	private final String paymentNetworkResponseErrorMessage;
	/**
	 * Transaction traceability code.
	 */
	private final String traceabilityCode;
	/**
	 * Transaction authorization code
	 */
	private final String authorizationCode;
	/**
	 * Transaction pending reason
	 */
	private final String pendingReason;
	/**
	 * Transaction response code
	 */
	private final String responseCode;
	/**
	 * Transaction error code
	 */
	private final String errorCode;
	/**
	 * Transaction response message
	 */
	private final String responseMessage;
	/**
	 * Transaction date
	 */
	private final Date transactionDate;
	/**
	 * Transaction time
	 */
	private final BigDecimal transactionTime;
	/**
	 * Transaction operation date
	 */
	private final Date operationDate;

}
