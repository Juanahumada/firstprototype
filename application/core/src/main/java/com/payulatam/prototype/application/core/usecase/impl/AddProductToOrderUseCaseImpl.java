/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.OrderProvider;
import com.payulatam.prototype.application.core.usecase.AddProductToOrderUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.ProductValidator;

/**
 * Implementation of {@link AddProductToOrderUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class AddProductToOrderUseCaseImpl implements AddProductToOrderUseCase {

	/**
	 * Provider for create order operation.
	 */
	private final OrderProvider addProductToOrderProvider;

	/**
	 * {@inheritDoc}
	 */
	public void addProductToOrder(final Order order, final User user, final Product product, final Integer quantity) throws
																													 PrototypeException {

		ProductValidator.verifyProductStock(product, quantity);
		addProductToOrderProvider.addProductToOrder(order, user, product, quantity);
	}
}
