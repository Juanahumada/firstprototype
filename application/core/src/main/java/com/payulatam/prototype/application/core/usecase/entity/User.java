/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import java.util.List;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the user object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class User {

	/**
	 * User dni.
	 */
	private final String dni;

	/**
	 * User name.
	 */
	private final String name;

	/**
	 * User last name.
	 */
	private final String lastName;

	/**
	 * User email
	 */
	private final String email;

	/**
	 * User phone
	 */
	private final String phone;

	/**
	 * User password
	 */
	private final String password;

	/**
	 * User status
	 */
	private final Boolean enabled;

	/**
	 * User roles
	 */
	private final List<Role> roles;

	/**
	 * User orders
	 */
	private final List<Order> orders;

	/**
	 * Different payment options registered by the user.
	 */
	private final List<SavedPaymentInfo> paymentsInfo;
}
