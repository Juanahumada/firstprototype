/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.usecase.DeleteProductUseCase;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.ProductValidator;

/**
 * Implementation of {@link DeleteProductUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class DeleteProductUseCaseImpl implements DeleteProductUseCase {

	/**
	 * Provider for delete product operation.
	 */
	private final ProductProvider productProvider;

	/**
	 * Read product use case
	 */
	private final ReadProductUseCase readProductUseCase;

	/**
	 * Method that delete a given product.
	 *
	 * @param productId Product to be deleted.
	 * @throws PrototypeException When the product does not exists.
	 */
	@Override public void deleteProduct(final String productId) throws PrototypeException {

		ProductValidator.verifyProductExistence(productId, readProductUseCase);
		productProvider.deleteProduct(productId);
	}
}
