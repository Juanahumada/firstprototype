/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.TokenizeCardUseCase;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Implementation of {@link TokenizeCardUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class TokenizeCardUseCaseImpl implements TokenizeCardUseCase {

	/**
	 * Provider for transaction.
	 */
	private final TransactionProvider transactionProvider;

	/**
	 * Read user use case.
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public TokenizedCard tokenizeCard(final String user, final NewPaymentInfo newPaymentInfo) throws PrototypeException {

		final User us = readUserUseCase.getUserByEmail(user);
		return transactionProvider.tokenizeCard(us, newPaymentInfo);
	}
}
