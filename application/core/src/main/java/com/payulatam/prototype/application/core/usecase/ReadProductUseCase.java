/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to get products.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ReadProductUseCase {

	/**
	 * Method that search all the products.
	 *
	 * @return All the products in the store.
	 */
	List<Product> getAllProducts();

	/**
	 * Method that get a product with a given id.
	 *
	 * @param productId Id of the product
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	Product getProductWithId(String productId) throws PrototypeException;
}
