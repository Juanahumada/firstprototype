/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.UserProvider;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.SavePaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.TokenizeCardUseCase;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.UserValidator;

/**
 * Implementation of {@link SavePaymentInfoUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class SavePaymentInfoUseCaseImpl implements SavePaymentInfoUseCase {

	/**
	 * Provider for save new payment info operation.
	 */
	private final UserProvider provider;

	/**
	 * Use case for tokenize a card.
	 */
	private final TokenizeCardUseCase tokenizeCardUseCase;

	/**
	 * Use case for read an user.
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Save a payment info to a user
	 *
	 * @param user           User to assign the new payment info
	 * @param newPaymentInfo Data of the new payment
	 * @throws PrototypeException When the user does not exists.
	 */
	public void savePaymentInfo(final NewPaymentInfo newPaymentInfo, final String user) throws PrototypeException {

		final TokenizedCard tokenizedCard = tokenizeCardUseCase.tokenizeCard(user, newPaymentInfo);
		final User us = readUserUseCase.getUserByEmail(user);
		UserValidator.verifyPaymentMethod(us, tokenizedCard);
		provider.savePaymentInfo(us, newPaymentInfo, tokenizedCard);
	}
}
