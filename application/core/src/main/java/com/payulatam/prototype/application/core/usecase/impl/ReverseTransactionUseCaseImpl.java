/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.ReverseTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.OrderValidator;
import com.payulatam.prototype.application.core.validator.TransactionValidator;

/**
 * Implementation of {@link ReverseTransactionUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ReverseTransactionUseCaseImpl implements ReverseTransactionUseCase {

	/**
	 * Provider for transaction.
	 */
	private final TransactionProvider transactionProvider;

	/**
	 * Read Order use case
	 */
	private final ReadOrderUseCase readOrderUseCase;

	/**
	 * Read user use case
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Read Transaction use case
	 */
	private final ReadTransactionUseCase readTransactionUseCase;

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param userId        The id of the user that has the order.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the reund.
	 * @throws PrototypeException When the user, order or transaction does not exists.
	 */
	@Override public Transaction reverseTransaction(final String orderId, final String userId, final String transactionId,
											 final String reason) throws PrototypeException {

		final Order order = readOrderUseCase.getOrderById(orderId);
		OrderValidator.verifyClosedOrder(order);
		final User user = readUserUseCase.getUserByEmail(userId);
		final Transaction transaction = readTransactionUseCase.getTransactionWithId(transactionId);
		TransactionValidator.verifyApprovedTransaction(transaction);
		TransactionValidator.verifyReverseConsistence(order, user, transaction);
		return transactionProvider.reverseTransaction(order, transaction, reason);
	}
}
