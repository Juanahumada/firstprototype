/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to create orders.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface CreateOrderUseCase {

	/**
	 * Creates a given order.
	 *
	 * @param product  First product to add to the order.
	 * @param user     User that is creating the order.
	 * @param quantity Quantity of the product.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	void createOrder(User user, Product product, Integer quantity) throws PrototypeException;
}
