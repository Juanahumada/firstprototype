/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.UserProvider;
import com.payulatam.prototype.application.core.usecase.ReadUserSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Implementation of {@link ReadUserSavedPaymentInfoUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ReadUserSavedPaymentInfoUseCaseImpl implements ReadUserSavedPaymentInfoUseCase {

	/**
	 * Provider for get payment info.
	 */
	private final UserProvider provider;
	/**
	 * Read user use case
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Gets the payment info for the given user
	 *
	 * @param username Username provided.
	 * @return True if the authentication is valid, otherwise false.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public List<SavedPaymentInfo> getPaymentInfo(final String username) throws PrototypeException {

		final User user = readUserUseCase.getUserByEmail(username);
		return provider.getPaymentInfo(user);
	}

	/**
	 * Gets the payment info with the given id
	 *
	 * @param paymentInfoId Payment info id
	 * @return The payment info with the given id.
	 * @throws PrototypeException When the payment information with the given id does not exists.
	 */
	@Override public SavedPaymentInfo getPaymentInfoById(final String paymentInfoId) throws PrototypeException {

		return provider.getPaymentInfoById(paymentInfoId);
	}
}
