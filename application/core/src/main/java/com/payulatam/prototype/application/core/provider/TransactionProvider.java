/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.provider;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Interface that exposes transaction operations for the use cases: MakeTransactionWithNewPaymentInfo
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface TransactionProvider {

	/**
	 * Method that makes a transaction with a new payment info.
	 *
	 * @param user           User.
	 * @param order          Id of the order.
	 * @param newPaymentInfo Information of the payment method.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	Transaction makeTransactionWithNewPaymentInfo(User user, Order order, NewPaymentInfo newPaymentInfo) throws PrototypeException;

	/**
	 * Method that makes a transaction with a saved payment info.
	 *
	 * @param user        Id of the user.
	 * @param order       Id of the order.
	 * @param paymentInfo Information of the payment method.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	Transaction makeTransactionWithSavedPaymentInfo(User user, Order order, SavedPaymentInfo paymentInfo) throws PrototypeException;

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the refund.
	 * @throws PrototypeException When the connection with the api fails.
	 */
	Transaction reverseTransaction(Order orderId, Transaction transactionId, String reason) throws PrototypeException;

	/**
	 * Method that tokenize a given credit card.
	 *
	 * @param user           Id of the user
	 * @param newPaymentInfo The information of the credit card.
	 * @return The card tokenized.
	 * @throws PrototypeException When the communication with the external api fails.
	 */
	TokenizedCard tokenizeCard(User user, NewPaymentInfo newPaymentInfo) throws PrototypeException;

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	String ping();

	/**
	 * Method that get a transaction with a given id.
	 *
	 * @param transactionId Id of the transaction
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	Transaction getTransactionWithId(final String transactionId) throws PrototypeException;

}
