/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.provider;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Interface that exposes order operations for the use cases: CreateOrder
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface OrderProvider {

	/**
	 * Creates a new order to an user.
	 *
	 * @param product  First product to add to the order.
	 * @param user     User that is making the change.
	 * @param quantity Quantity of the product.
	 */
	void createOrder(User user, Product product, Integer quantity);

	/**
	 * Adds product to an order.
	 *
	 * @param order    Order that has to be modified.
	 * @param user     User that has the order to be modified.
	 * @param product  Product that has to been added.
	 * @param quantity Quantity of the product.
	 */
	void addProductToOrder(Order order, User user, Product product, Integer quantity);

	/**
	 * Gets all the orders in the database.
	 *
	 * @return A list of the orders in the store.
	 */
	List<Order> getAllOrders();

	/**
	 * Method that search all the orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the orders in the store for the user.
	 */
	List<Order> getAllUserOrders(String userId);

	/**
	 * Method that search all the open orders of a given user.
	 *
	 * @param user Id of the user.
	 * @return All the orders in the store for the user.
	 */
	List<Order> getAllOpenUserOrders(String user);

	/**
	 * Get an order with the given id
	 *
	 * @param id The id of the order
	 * @return The order with the given id
	 * @throws PrototypeException When the order does not exists.
	 */
	Order getOrderById(final String id) throws PrototypeException;
}
