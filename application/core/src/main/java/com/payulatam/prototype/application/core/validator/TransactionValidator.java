/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.DataConsistenceException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.usecase.exception.TransactionInvalidStateException;

/**
 * Make the validations for the transaction.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class TransactionValidator {

	/**
	 * Class Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionValidator.class);

	/**
	 * Private constructor in order to hide implicit public one.
	 */
	private TransactionValidator() {
		//Do nothing
	}

	/**
	 * Verifies if the transaction is in an approved state
	 *
	 * @param transaction Transaction to check
	 * @throws PrototypeException When the transaction is not in an approved state.
	 */
	public static void verifyApprovedTransaction(final Transaction transaction)
			throws PrototypeException {

		if (!"APPROVED".equals(transaction.getState())) {
			throw new TransactionInvalidStateException("The transaction must be in an approved state.");
		}
		LOGGER.debug("The transaction state is correct.");
	}

	/**
	 * Verifies if the reverse transaction is consistent
	 *
	 * @param order       Order
	 * @param user        User
	 * @param transaction Transaction
	 * @throws PrototypeException When the information has inconsistencies.
	 */
	public static void verifyReverseConsistence(final Order order, final User user, final Transaction transaction)
			throws PrototypeException {

		final Optional<Transaction> transactionOptional =
				order.getTransactions().stream().filter(t -> t.getId().equals(transaction.getId())).findFirst();
		final Optional<Order> orderOptional =
				user.getOrders().stream().filter(o -> o.getId().equals(order.getId())).findFirst();
		if (orderOptional.isEmpty() || transactionOptional.isEmpty()) {
			LOGGER.info("The information given is inconsistent for the reverse transaction.");
			throw new DataConsistenceException("The information given is inconsistent for the reverse transaction.");
		}
		LOGGER.debug("The information given is correct for the reverse transaction.");
	}

	/**
	 * Verifies if the transaction with new payment method is consistent
	 *
	 * @param order Order
	 * @param user  User
	 * @throws PrototypeException When the information has inconsistencies.
	 */
	public static void verifyTransactionSavedPaymentMethod(final Order order, final User user, final SavedPaymentInfo savedPaymentInfo)
			throws PrototypeException {

		final Optional<SavedPaymentInfo> savedPaymentInfoOptional =
				user.getPaymentsInfo().stream().filter(p -> p.getId().equals(savedPaymentInfo.getId())).findFirst();
		final Optional<Order> orderOptional =
				user.getOrders().stream().filter(o -> o.getId().equals(order.getId())).findFirst();
		if (orderOptional.isEmpty() || savedPaymentInfoOptional.isEmpty()) {
			LOGGER.info("The information given is inconsistent for the payment transaction.");
			throw new DataConsistenceException("The information given is inconsistent for the payment transaction.");
		}
		LOGGER.debug("The information given is correct for the saved payment transaction.");
	}

	/**
	 * Verifies if the transaction with new payment method is consistent
	 *
	 * @param order Order
	 * @param user  User
	 * @throws PrototypeException When the information has inconsistencies.
	 */
	public static void verifyTransactionNewPaymentMethod(final Order order, final User user)
			throws PrototypeException {

		final Optional<Order> orderOptional =
				user.getOrders().stream().filter(o -> o.getId().equals(order.getId())).findFirst();
		if (orderOptional.isEmpty()) {
			LOGGER.info("The information given is inconsistent for the new payment transaction.");
			throw new DataConsistenceException("The information given is inconsistent for the new payment transaction.");
		}
		LOGGER.debug("The information given is correct for the new payment transaction.");
	}
}
