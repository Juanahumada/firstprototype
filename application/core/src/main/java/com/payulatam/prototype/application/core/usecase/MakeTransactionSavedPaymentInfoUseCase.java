/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to make a transaction with a saved payment info.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface MakeTransactionSavedPaymentInfoUseCase {

	/**
	 * Makes a transaction with a saved payment info.
	 *
	 * @param order       Id of the order.
	 * @param user        Id of the user that is creating the order.
	 * @param paymentInfo Id of the saved payment info.
	 * @throws PrototypeException When whe user, order or payment info does not exists.
	 */
	Transaction makeTransactionWithSavedPaymentInfo(String user, String order, String paymentInfo) throws PrototypeException;
}
