/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import lombok.Getter;

/**
 * Order status.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
public enum OrderStatus {
	/**
	 * Status when the order is open.
	 */
	OPEN("OPEN"),

	/**
	 * Status when the order is closed.
	 */
	CLOSED("CLOSED"),

	/**
	 * Status when the order is in progress of processing.
	 */
	PROCESSING("PROCESSING"),

	/**
	 * Status when the order is reversed.
	 */
	REVERSED("REVERSED");

	/**
	 * Order string representation.
	 **/
	private final String detail;

	/**
	 * Constructor with description injected by dependency inversion
	 *
	 * @param detail exception string representation
	 */
	OrderStatus(final String detail) {

		this.detail = detail;
	}
}
