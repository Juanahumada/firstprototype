/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.MakeTransactionSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.OrderValidator;
import com.payulatam.prototype.application.core.validator.TransactionValidator;

/**
 * Implementation of {@link MakeTransactionSavedPaymentInfoUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class MakeTransactionSavedPaymentInfoUseCaseImpl implements MakeTransactionSavedPaymentInfoUseCase {

	/**
	 * Provider for make transaction operation.
	 */
	private final TransactionProvider transactionProvider;

	/**
	 * Read user user case
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Read order user case
	 */
	private final ReadOrderUseCase readOrderUseCase;

	/**
	 * Read payment info user case
	 */
	private final ReadUserSavedPaymentInfoUseCase readUserSavedPaymentInfoUseCase;

	/**
	 * Makes a transaction with a saved payment info.
	 *
	 * @param user        Id of the user that is creating the order.
	 * @param order       Id of the order.
	 * @param paymentInfo Id of the saved payment info.
	 * @throws PrototypeException When whe user, order or payment info does not exists.
	 */
	@Override public Transaction makeTransactionWithSavedPaymentInfo(final String user, final String order, final String paymentInfo)
			throws PrototypeException {

		final User u = readUserUseCase.getUserByEmail(user);
		final Order o = readOrderUseCase.getOrderById(order);
		OrderValidator.verifyOpenOrder(o);
		final SavedPaymentInfo savedPaymentInfo = readUserSavedPaymentInfoUseCase.getPaymentInfoById(paymentInfo);
		TransactionValidator.verifyTransactionSavedPaymentMethod(o, u, savedPaymentInfo);
		return transactionProvider.makeTransactionWithSavedPaymentInfo(u, o, savedPaymentInfo);
	}
}
