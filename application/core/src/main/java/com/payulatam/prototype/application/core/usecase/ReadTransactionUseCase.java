/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes transaction operations to get transactions.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ReadTransactionUseCase {

	/**
	 * Method that get a transaction with a given id.
	 *
	 * @param transactionId Id of the transaction
	 * @return The product with the given id.
	 * @throws PrototypeException When the product does not exists.
	 */
	Transaction getTransactionWithId(String transactionId) throws PrototypeException;
}
