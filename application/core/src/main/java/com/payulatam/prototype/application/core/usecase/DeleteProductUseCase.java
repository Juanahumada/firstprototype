/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to delete products.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface DeleteProductUseCase {

	/**
	 * Method that delete a given product.
	 *
	 * @param productId Product to be deleted.
	 * @throws PrototypeException When the product does not exists.
	 */
	void deleteProduct(String productId) throws PrototypeException;
}
