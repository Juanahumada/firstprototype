/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the NewPaymentInfo object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class NewPaymentInfo {

	/**
	 * Payment info credit card number
	 */
	private final String creditCardNumber;
	/**
	 * Payment info credit card security code
	 */
	private final String creditCardSecurityCode;
	/**
	 * Payment info credit card expiration date
	 */
	private final String creditCardExpirationDate;
	/**
	 * Payment info card name
	 */
	private final String creditCardName;
	/**
	 * Payment info owner dni
	 */
	private final String creditCardUserDni;
	/**
	 * Payment info billing address street 1
	 */
	private final String billingAddressStreet1;
	/**
	 * Payment info billing address street 2
	 */
	private final String billingAddressStreet2;
	/**
	 * Payment info billing address city
	 */
	private final String billingAddressCity;
	/**
	 * Payment info billing address state
	 */
	private final String billingAddressState;
	/**
	 * Payment info billing address country
	 */
	private final String billingAddressCountry;
	/**
	 * Payment info billing address postal code
	 */
	private final String billingAddressPostalCode;
	/**
	 * Payment info billing address phone
	 */
	private final String billingAddressPhone;
	/**
	 * Payment info payment method
	 */
	private final String paymentMethod;
}
