/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.SaveProductUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.ProductValidator;

/**
 * Implementation of {@link SaveProductUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class SaveProductUseCaseImpl implements SaveProductUseCase {

	/**
	 * Provider for get products.
	 */
	private final ProductProvider provider;

	/**
	 * Read product use case
	 */
	private final ReadProductUseCase readProductUseCase;

	/**
	 * Method that saves a product.
	 *
	 * @param product Product to be saved.
	 * @throws PrototypeException when the product alerady exists
	 */
	@Override public void saveProduct(final Product product) throws PrototypeException {

		ProductValidator.verifyDuplicateProductCreation(product, readProductUseCase);
		provider.saveProduct(product);
	}

	/**
	 * Updates the stock of a given product.
	 *
	 * @param product  The product.
	 * @param stock    The quantity to be reduced or increased.
	 * @param increase True if the stock has to be increased or false if it has to be reduced.
	 */
	@Override public void updateStock(final Product product, final int stock, final boolean increase) {

		provider.updateStock(product, stock, increase);
	}
}
