/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

public interface ReadUserSavedPaymentInfoUseCase {

	/**
	 * Gets the payment info for the given user
	 *
	 * @param username Username provided.
	 * @return A list with all the payments info
	 * @throws PrototypeException When the user does not exists.
	 */
	List<SavedPaymentInfo> getPaymentInfo(String username) throws PrototypeException;

	/**
	 * Gets the payment info with the given id
	 *
	 * @param paymentInfoId Payment info id
	 * @return The payment info with the given id.
	 * @throws PrototypeException When the payment information with the given id does not exists.
	 */
	SavedPaymentInfo getPaymentInfoById(String paymentInfoId) throws PrototypeException;
}
