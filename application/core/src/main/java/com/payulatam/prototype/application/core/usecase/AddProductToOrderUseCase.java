/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to add products to an order.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface AddProductToOrderUseCase {

	/**
	 * Adds a product to a given order.
	 *
	 * @param order    Order that has to be modified.
	 * @param user     User that is adding the product.
	 * @param product  Product that have to been added to the order.
	 * @param quantity Quantity of the product.
	 * @throws PrototypeException When the product does not have enough stock.
	 */
	void addProductToOrder(Order order, User user, Product product, Integer quantity) throws PrototypeException;
}
