/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.AllArgsConstructor;

import com.payulatam.prototype.application.core.provider.OrderProvider;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.validator.OrderValidator;
import com.payulatam.prototype.application.core.validator.UserValidator;

/**
 * Implementation of {@link ReadOrderUseCase} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@AllArgsConstructor
public class ReadOrderUseCaseImpl implements ReadOrderUseCase {

	/**
	 * Provider for get products.
	 */
	private final OrderProvider provider;

	/**
	 * Read user use case
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<Order> getAllOrders() {

		return provider.getAllOrders();
	}

	/**
	 * Method that search all the orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the orders in the store for the user.
	 * @throws PrototypeException If the user does not exists.
	 */
	@Override public List<Order> getAllUserOrders(final String userId) throws PrototypeException {

		UserValidator.verifyUser(userId, readUserUseCase);
		return provider.getAllUserOrders(userId);
	}

	/**
	 * Method that search all the open orders of a given user.
	 *
	 * @param user Id of the user.
	 * @return All the orders in the store for the user.
	 * @throws PrototypeException When the user has more than 1 order in an open state.
	 */
	@Override public List<Order> getAllOpenUserOrders(final String user) throws PrototypeException {

		final List<Order> orderList = provider.getAllOpenUserOrders(user);
		OrderValidator.verifyUserOpenOrders(orderList);
		return orderList;
	}

	/**
	 * Get an order with the given id
	 *
	 * @param id The id of the order
	 * @return The order with the given id
	 * @throws PrototypeException When the order does not exists.
	 */
	@Override public Order getOrderById(final String id) throws PrototypeException {

		return provider.getOrderById(id);
	}
}
