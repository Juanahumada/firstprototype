/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.OrderStatus;
import com.payulatam.prototype.application.core.usecase.exception.OpenOrdersQuantityException;
import com.payulatam.prototype.application.core.usecase.exception.OrderInvalidStateException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Make the validations for the product.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class OrderValidator {

	/**
	 * Class Logger.
	 */
	private static final Logger LOGGER = LoggerFactory.getLogger(OrderValidator.class);

	/**
	 * Private constructor in order to hide implicit public one.
	 */
	private OrderValidator() {
		//Do nothing
	}

	/**
	 * Verify if the user has more than 1 open order
	 *
	 * @param orderList User order list
	 * @throws PrototypeException When the user has more than 1 order in Open state
	 */
	public static void verifyUserOpenOrders(final List<Order> orderList)
			throws PrototypeException {

		if (orderList.size() > 1) {
			int openCounter = 0;
			for (Order order : orderList) {
				if (OrderStatus.OPEN.getDetail().equals(order.getStatus())) {
					openCounter += 1;
				}
				if (openCounter > 1) {
					LOGGER.info("The user has more than 1 order in an open state.");
					throw new OpenOrdersQuantityException("The user has more than 1 order in an open state.");
				}
			}
		}
		LOGGER.debug("Less than two orders in open state found.");
	}

	/**
	 * Verifies if the order is in an open state
	 *
	 * @param order Order to check
	 * @throws PrototypeException When the order is not in an open state.
	 */
	public static void verifyOpenOrder(final Order order)
			throws PrototypeException {

		if (!OrderStatus.OPEN.getDetail().equals(order.getStatus())) {
			LOGGER.info("The order with the id: {} must be in open state", order.getId());
			throw new OrderInvalidStateException("The order " + order.getId() + " must be in open state.");
		}
		LOGGER.debug("The order state is correct.");
	}

	/**
	 * Verifies if the order is in a closed state
	 *
	 * @param order Order to check
	 * @throws PrototypeException When the order is not in an closed state.
	 */
	public static void verifyClosedOrder(final Order order)
			throws PrototypeException {

		if (!OrderStatus.CLOSED.getDetail().equals(order.getStatus())) {
			LOGGER.info("The order with the id: {} must be in close state", order.getId());
			throw new OrderInvalidStateException("The order " + order.getId() + " must be in closed state.");
		}
		LOGGER.debug("The order {} state is correct.", order.getId());
	}
}
