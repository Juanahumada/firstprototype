/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

/**
 * Entity that represents the SavedPaymentInfo object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
@Setter
public class SavedPaymentInfo {

	/**
	 * Order id
	 */
	private final String id;
	/**
	 * User credit card tokenized
	 */
	private final TokenizedCard tokenizedCard;

	/**
	 * User billing street 1
	 */
	private final String billingAddressStreet1;

	/**
	 * User billing street 2
	 */
	private final String billingAddressStreet2;

	/**
	 * User billing state
	 */
	private final String billingAddressState;

	/**
	 * User billing country
	 */
	private final String billingAddressCountry;

	/**
	 * User billing country
	 */
	private final String billingAddressCity;

	/**
	 * User billing postal code
	 */
	private final String billingAddressPostalCode;

	/**
	 * User billing phone
	 */
	private final String billingAddressPhone;
}
