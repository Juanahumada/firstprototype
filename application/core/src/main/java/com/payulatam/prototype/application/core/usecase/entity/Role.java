/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.core.usecase.entity;

import lombok.Builder;
import lombok.Getter;

/**
 * Entity that represents the role object of the business.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Builder
@Getter
public class Role {

	/**
	 * Role id
	 */
	private final String id;
	/**
	 * Role name
	 */
	private final String name;
}
