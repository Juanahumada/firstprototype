/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to get orders.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ReadOrderUseCase {

	/**
	 * Method that search all the orders.
	 *
	 * @return All the orders in the store.
	 */
	List<Order> getAllOrders();

	/**
	 * Method that search all the orders of a given user.
	 *
	 * @param userId Id of the user.
	 * @return All the orders in the store for the user.
	 * @throws PrototypeException When the user does not exists.
	 */
	List<Order> getAllUserOrders(String userId) throws PrototypeException;

	/**
	 * Method that search all the open orders of a given user.
	 *
	 * @param user Id of the user.
	 * @return All the open orders in the store for the user.
	 * @throws PrototypeException When the user has more than 1 order in an open state.
	 */
	List<Order> getAllOpenUserOrders(String user) throws PrototypeException;

	/**
	 * Get an order with the given id
	 *
	 * @param id The id of the order
	 * @return The order with the given id
	 * @throws PrototypeException When the order does not exists.
	 */
	Order getOrderById(String id) throws PrototypeException;
}
