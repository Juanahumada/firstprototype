/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import java.util.Optional;

import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PaymentMethodAlreadyExistsException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Make the validations for the user.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class UserValidator {

	/**
	 * Private constructor in order to hide implicit public one.
	 */
	private UserValidator() {
		//Do nothing
	}

	/**
	 * Verifies if the user exists.
	 *
	 * @param userId          Id of the user
	 * @param readUserUseCase Read user use case
	 * @throws PrototypeException If the user does not exists.
	 */
	public static void verifyUser(final String userId, final ReadUserUseCase readUserUseCase)
			throws PrototypeException {

		readUserUseCase.getUserByEmail(userId);
	}

	/**
	 * Verifies if the payment method exists for the user.
	 *
	 * @param user          The user
	 * @param tokenizedCard The tokenized card
	 * @throws PrototypeException When the payment method already exists.
	 */
	public static void verifyPaymentMethod(final User user, final TokenizedCard tokenizedCard)
			throws PrototypeException {

		final Optional<SavedPaymentInfo> savedPaymentInfoOptional =
				user.getPaymentsInfo().stream().filter(p -> p.getTokenizedCard().getCreditCardTokenId().equals(tokenizedCard.getCreditCardTokenId())).findFirst();
		if (savedPaymentInfoOptional.isPresent()) {
			throw new PaymentMethodAlreadyExistsException("The payment method is already associated with the user.");
		}
	}
}
