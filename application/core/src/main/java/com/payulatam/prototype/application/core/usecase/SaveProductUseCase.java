/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.core.usecase;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;

/**
 * Exposes operation to update a product.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface SaveProductUseCase {

	/**
	 * Method that saves a product.
	 *
	 * @param product Product to be saved.
	 * @throws PrototypeException when the product alerady exists
	 */
	void saveProduct(Product product) throws PrototypeException;

	/**
	 * Updates the stock of a given product.
	 *
	 * @param product  The product.
	 * @param stock    The quantity to be reduced or increased.
	 * @param increase True if the stock has to be increased or false if it has to be reduced.
	 */
	void updateStock(Product product, int stock, boolean increase);
}
