/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.exception.OpenOrdersQuantityException;
import com.payulatam.prototype.application.core.usecase.exception.OrderInvalidStateException;
import com.payulatam.prototype.application.core.util.SupplierOrderObjectFaker;

/**
 * Test cases for the {@link OrderValidator} class.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class OrderValidatorTest {

	/**
	 * Test that the method {@link OrderValidator#verifyUserOpenOrders(List)} Check successfully when the user has more than 1 open order.
	 */
	@Test
	@SneakyThrows
	void verifyUserOpenOrdersVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> OrderValidator.verifyUserOpenOrders(SupplierOrderObjectFaker.getBasicOrderListWithoutErrors())
									 );

	}

	/**
	 * Test that the method {@link OrderValidator#verifyUserOpenOrders(List)} Check with error when the user has more than 1 open order.
	 */
	@Test
	@SneakyThrows
	void verifyUserOpenOrdersVerifyError() {

		final Exception exception = assertThrows(OpenOrdersQuantityException.class, () -> OrderValidator
				.verifyUserOpenOrders(SupplierOrderObjectFaker.getBasicOrderListWithErrors()));
		final String expectedMessage = "The user has more than 1 order in an open state.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	/**
	 * Test that the method {@link OrderValidator#verifyOpenOrder(Order)} Check successfully when the user has more than 1 open order.
	 */
	@Test
	@SneakyThrows
	void verifyOpenOrderVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> OrderValidator.verifyOpenOrder(SupplierOrderObjectFaker.getBasicOpenOrder("O1"))
									 );

	}

	/**
	 * Test that the method {@link OrderValidator#verifyOpenOrder(Order)} Check with error when the user has more than 1 open order.
	 */
	@Test
	@SneakyThrows
	void verifyOpenOrderVerifyError() {

		final Exception exception = assertThrows(OrderInvalidStateException.class,
												 () -> OrderValidator.verifyOpenOrder(SupplierOrderObjectFaker.getBasicClosedOrder("O1")));
		final String expectedMessage = "The order O1 must be in open state.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	/**
	 * Test that the method {@link OrderValidator#verifyClosedOrder(Order)} Check successfully that the order must be in a closed state.
	 */
	@Test
	@SneakyThrows
	void verifyClosedOrderVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> OrderValidator.verifyClosedOrder(SupplierOrderObjectFaker.getBasicClosedOrder("O1"))
									 );

	}

	/**
	 * Test that the method {@link OrderValidator#verifyClosedOrder(Order)} Check with error that the order must be in a closed state.
	 */
	@Test
	@SneakyThrows
	void verifyClosedOrderVerifyError() {

		final Exception exception = assertThrows(OrderInvalidStateException.class,
												 () -> OrderValidator.verifyClosedOrder(SupplierOrderObjectFaker.getBasicOpenOrder("O1")));
		final String expectedMessage = "The order O1 must be in closed state.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
}