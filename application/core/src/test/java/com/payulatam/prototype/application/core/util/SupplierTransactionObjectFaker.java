/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.util;

import java.util.Date;

import com.payulatam.prototype.application.core.usecase.entity.Transaction;

/**
 * Class that supplied fake transaction for use in tests.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class SupplierTransactionObjectFaker {

	/**
	 * Provide a fake {@link Transaction}.
	 *
	 * @return Fake transaction entity
	 */
	public static Transaction getBasicApprovedTransaction(final String id) {

		return Transaction.builder()
						  .id(id)
						  .authorizationCode("AUT123")
						  .state("APPROVED")
						  .transactionDate(new Date())
						  .orderId("O1")
						  .build();
	}

	/**
	 * Provide a fake {@link Transaction}.
	 *
	 * @return Fake transaction entity
	 */
	public static Transaction getBasicDeclinedTransaction(final String id) {

		return Transaction.builder()
						  .id(id)
						  .authorizationCode("AUT123")
						  .state("DECLINED")
						  .transactionDate(new Date())
						  .orderId("O1")
						  .build();
	}
}
