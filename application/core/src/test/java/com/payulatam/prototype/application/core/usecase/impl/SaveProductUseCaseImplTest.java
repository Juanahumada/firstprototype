/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyBoolean;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.util.SupplierProductObjectFaker;

/**
 * Test cases for the {@link SaveProductUseCaseImpl} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class SaveProductUseCaseImplTest {

	/**
	 * Mocked product provider used in the use case.
	 */
	@Mock
	private ProductProvider productProvider;

	/**
	 * Instance of the tested class.
	 */
	@InjectMocks
	private SaveProductUseCaseImpl saveProductUseCase;

	/**
	 * Mocked read product use case used in the test.
	 */
	@Mock
	private ReadProductUseCase readProductUseCase;

	/**
	 * Tests that {@link SaveProductUseCaseImpl#saveProduct} method successfully saves the product.
	 */
	@Test
	@SneakyThrows
	void decreaseStockShouldUpdateSuccessfully() {

		final Product product = Product.builder()
									   .name("P1")
									   .value(BigDecimal.valueOf(10000))
									   .description("D1")
									   .build();
		saveProductUseCase.updateStock(product, 2, false);
		verify(productProvider).updateStock(any(Product.class), anyInt(), anyBoolean());
	}

	/**
	 * Tests that {@link SaveProductUseCaseImpl#saveProduct} method successfully saves the product.
	 */
	@Test
	@SneakyThrows
	void saveProductSuccessfully() {

		when(readProductUseCase.getAllProducts()).thenReturn(SupplierProductObjectFaker.getBasicProductList());
		final Product product = SupplierProductObjectFaker.getBasicProductWithDescription("O1","Other Shoe");
		Assertions.assertDoesNotThrow(() -> saveProductUseCase.saveProduct(product));
		verify(productProvider).saveProduct(any(Product.class));
	}

}
