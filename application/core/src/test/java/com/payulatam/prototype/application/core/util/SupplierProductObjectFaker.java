/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.Product;

/**
 * Class that supplied fake product for use in tests.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class SupplierProductObjectFaker {

	/**
	 * Provide a fake {@link Product}.
	 *
	 * @return Fake product entity
	 */
	public static Product getBasicProduct(final String id) {

		return Product.builder()
					  .id(id)
					  .name("Product1")
					  .value(BigDecimal.valueOf(10000))
					  .description("Description1")
					  .photo("photo.png")
					  .stock(5)
					  .build();
	}

	/**
	 * Provide a fake {@link Product}.
	 *
	 * @return Fake product entity
	 */
	public static Product getBasicProductWithDescription(final String id, final String description) {

		return Product.builder()
					  .id(id)
					  .name(description)
					  .value(BigDecimal.valueOf(10000))
					  .description("Description1")
					  .photo("photo.png")
					  .stock(5)
					  .build();
	}

	/**
	 * Provide a fake {@link Product} list.
	 *
	 * @return Fake product list entity
	 */
	public static List<Product> getBasicProductList() {

		final List<Product> productList = new ArrayList<>();
		productList.add(getBasicProduct("P1"));
		productList.add(getBasicProduct("P2"));
		productList.add(getBasicProduct("P3"));
		productList.add(getBasicProduct("P4"));
		productList.add(getBasicProduct("P5"));
		return productList;
	}
}
