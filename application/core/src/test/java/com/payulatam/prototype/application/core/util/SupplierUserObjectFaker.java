/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.util;

import java.util.ArrayList;
import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.User;

/**
 * Class that supplied fake user for use in tests.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class SupplierUserObjectFaker {

	/**
	 * Provide a fake {@link User}.
	 *
	 * @return Fake user entity
	 */
	public static User getBasicUser(final String userId) {

		return User.builder()
				   .email(userId)
				   .paymentsInfo(new ArrayList<>())
				   .build();
	}

	/**
	 * Provide a fake {@link User}.
	 *
	 * @return Fake user entity
	 */
	public static User getBasicUserWithOrders(final String userId) {

		return User.builder()
				   .email(userId)
				   .orders(SupplierOrderObjectFaker.getBasicOrderListWithoutErrors())
				   .paymentsInfo(new ArrayList<>())
				   .build();
	}

	/**
	 * Provide a fake {@link User}.
	 *
	 * @return Fake user entity
	 */
	public static User getBasicUserWithOrdersAndSavedPaymentInfo(final String userId) {

		final List<SavedPaymentInfo> paymentInfoList = new ArrayList<>();
		paymentInfoList.add(SupplierSavedPaymentInfoObjectFaker.getBasicSavedPaymentInfo("P1"));
		return User.builder()
				   .email(userId)
				   .orders(SupplierOrderObjectFaker.getBasicOrderListWithoutErrors())
				   .paymentsInfo(paymentInfoList)
				   .build();
	}
}
