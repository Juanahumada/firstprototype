/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.DataConsistenceException;
import com.payulatam.prototype.application.core.usecase.exception.TransactionInvalidStateException;
import com.payulatam.prototype.application.core.util.SupplierOrderObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierSavedPaymentInfoObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierTransactionObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierUserObjectFaker;

/**
 * Test cases for the {@link TransactionValidator} class.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class TransactionValidatorTest {

	/**
	 * Test that the method {@link TransactionValidator#verifyApprovedTransaction(Transaction)}  Check successfully if the transaction is in
	 * an approved state.
	 */
	@Test
	@SneakyThrows
	void verifyApprovedTransactionVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> TransactionValidator.verifyApprovedTransaction(SupplierTransactionObjectFaker.getBasicApprovedTransaction("T1")));

	}

	/**
	 * Test that the method {@link TransactionValidator#verifyApprovedTransaction(Transaction)} Check with error if the transaction is in an
	 * approved state.
	 */
	@Test
	@SneakyThrows
	void verifyApprovedTransactionVerifyError() {

		final Exception exception = assertThrows(TransactionInvalidStateException.class,
												 () -> TransactionValidator.verifyApprovedTransaction(
														 SupplierTransactionObjectFaker.getBasicDeclinedTransaction("T1")));
		final String expectedMessage = "The transaction must be in an approved state.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	/**
	 * Test that the method {@link TransactionValidator#verifyTransactionNewPaymentMethod(Order, User)} checks successfully if the
	 * information has inconsistencies.
	 */
	@Test
	@SneakyThrows
	void verifyTransactionNewPaymentMethodVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> TransactionValidator.verifyTransactionNewPaymentMethod(SupplierOrderObjectFaker.getBasicOpenOrder("O1"),
																			 SupplierUserObjectFaker
																					 .getBasicUserWithOrders("test@test.com")));

	}

	/**
	 * Test that the method {@link TransactionValidator#verifyTransactionNewPaymentMethod(Order, User)} checks with error if the information
	 * has inconsistencies.
	 */
	@Test
	@SneakyThrows
	void verifyTransactionNewPaymentMethodVerifyError() {

		final Exception exception = assertThrows(DataConsistenceException.class,
												 () -> TransactionValidator
														 .verifyTransactionNewPaymentMethod(
																 SupplierOrderObjectFaker.getBasicOpenOrder("O5"),
																 SupplierUserObjectFaker.getBasicUserWithOrders("test@test.com")));
		final String expectedMessage = "The information given is inconsistent for the new payment transaction.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	/**
	 * Test that the method {@link TransactionValidator#verifyTransactionSavedPaymentMethod(Order, User, SavedPaymentInfo)} checks
	 * successfully if the information has inconsistencies.
	 */
	@Test
	@SneakyThrows
	void verifyTransactionSavedPaymentMethodVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> TransactionValidator.verifyTransactionSavedPaymentMethod(SupplierOrderObjectFaker.getBasicOpenOrder("O1"),
																			   SupplierUserObjectFaker
																					   .getBasicUserWithOrdersAndSavedPaymentInfo(
																							   "test@test.com"),
																			   SupplierSavedPaymentInfoObjectFaker
																					   .getBasicSavedPaymentInfo("P1")));

	}

	/**
	 * Test that the method {@link TransactionValidator#verifyTransactionSavedPaymentMethod(Order, User, SavedPaymentInfo)} checks
	 * successfully if the information has inconsistencies.
	 */
	@Test
	@SneakyThrows
	void verifyTransactionSavedPaymentMethodVerifyError() {

		final Exception exception = assertThrows(DataConsistenceException.class,
												 () -> TransactionValidator
														 .verifyTransactionSavedPaymentMethod(
																 SupplierOrderObjectFaker.getBasicOpenOrder("O1"),
																 SupplierUserObjectFaker
																		 .getBasicUserWithOrdersAndSavedPaymentInfo("test@test.com"),
																 SupplierSavedPaymentInfoObjectFaker
																		 .getBasicSavedPaymentInfo("P20")));
		final String expectedMessage = "The information given is inconsistent for the payment transaction.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	/**
	 * Test that the method {@link TransactionValidator#verifyReverseConsistence(Order, User, Transaction)} checks successfully if the
	 * information has inconsistencies.
	 */
	@Test
	@SneakyThrows
	void verifyReverseConsistenceVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> TransactionValidator.verifyReverseConsistence(SupplierOrderObjectFaker.getBasicClosedOrder("O1"),
																	SupplierUserObjectFaker
																			.getBasicUserWithOrdersAndSavedPaymentInfo("test@test.com"),
																	SupplierTransactionObjectFaker.getBasicApprovedTransaction("T1")));

	}

	/**
	 * Test that the method {@link TransactionValidator#verifyReverseConsistence(Order, User, Transaction)} checks successfully if the *
	 * information has inconsistencies.
	 */
	@Test
	@SneakyThrows
	void verifyReverseConsistenceVerifyError() {

		final Exception exception = assertThrows(DataConsistenceException.class,
												 () -> TransactionValidator
														 .verifyReverseConsistence(SupplierOrderObjectFaker.getBasicOpenOrder(
																 "O20"),
																				   SupplierUserObjectFaker
																						   .getBasicUserWithOrdersAndSavedPaymentInfo(
																								   "test@test.com"),
																				   SupplierTransactionObjectFaker
																						   .getBasicApprovedTransaction("T1")));
		final String expectedMessage = "The information given is inconsistent for the reverse transaction.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
}
