/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.exception.ProductAlreadyExistsException;
import com.payulatam.prototype.application.core.usecase.exception.ProductNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.ProductNotStockException;
import com.payulatam.prototype.application.core.util.SupplierProductObjectFaker;

/**
 * Test cases for the {@link ProductValidator} class.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class ProductValidatorTest {

	/**
	 * Mocked class of the {@link ReadProductUseCase}
	 */
	@Mock
	private ReadProductUseCase readProductUseCase;

	/**
	 * Test that the method {@link ProductValidator#verifyDuplicateProductCreation(Product, ReadProductUseCase)} check successfully if a
	 * product with the same data is already in the database.
	 */
	@Test
	@SneakyThrows
	void verifyDuplicateProductCreationVerifySuccessfully() {

		when(readProductUseCase.getAllProducts()).thenReturn(SupplierProductObjectFaker.getBasicProductList());
		Assertions.assertDoesNotThrow(
				() -> ProductValidator
						.verifyDuplicateProductCreation(SupplierProductObjectFaker.getBasicProductWithDescription("P6","Other shoe."),
														readProductUseCase));
		verify(readProductUseCase).getAllProducts();

	}

	/**
	 * Test that the method {@link ProductValidator#verifyDuplicateProductCreation(Product, ReadProductUseCase)} checks with error if a
	 * product with the same data is already in the database.
	 */
	@Test
	@SneakyThrows
	void verifyDuplicateProductCreationVerifyError() {

		when(readProductUseCase.getAllProducts()).thenReturn(SupplierProductObjectFaker.getBasicProductList());
		final Exception exception = assertThrows(ProductAlreadyExistsException.class, () -> ProductValidator
				.verifyDuplicateProductCreation(SupplierProductObjectFaker.getBasicProduct("P1"), readProductUseCase));
		final String expectedMessage = "The product to be added to the store must be different of the other products.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
		verify(readProductUseCase).getAllProducts();
	}

	/**
	 * Test that the method {@link ProductValidator#verifyProductStock(Product, Integer)} check successfully if the product have enough
	 * stock.
	 */
	@Test
	@SneakyThrows
	void verifyProductStockVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> ProductValidator
						.verifyProductStock(SupplierProductObjectFaker.getBasicProduct("P1"), 3));
	}

	/**
	 * Test that the method {@link ProductValidator#verifyProductStock(Product, Integer)}  checks with error if the product have enough
	 * stock.
	 */
	@Test
	@SneakyThrows
	void verifyProductStockVerifyError() {

		final Exception exception = assertThrows(ProductNotStockException.class, () -> ProductValidator
				.verifyProductStock(SupplierProductObjectFaker.getBasicProduct("P1"), 10));
		final String expectedMessage = "The product does not have enough stock as the required: Required: 10, Stock: 5";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

	/**
	 * Test that the method {@link ProductValidator#verifyProductExistence(String, ReadProductUseCase)} check successfully if the product
	 * exists.
	 */
	@Test
	@SneakyThrows
	void verifyProductExistenceVerifySuccessfully() {

		when(readProductUseCase.getProductWithId(anyString())).thenReturn(SupplierProductObjectFaker.getBasicProduct("P10"));
		Assertions.assertDoesNotThrow(
				() -> ProductValidator
						.verifyProductExistence("P1", readProductUseCase));
	}

	/**
	 * Test that the method {@link ProductValidator#verifyProductExistence(String, ReadProductUseCase)}   checks with error if the product
	 * exists.
	 */
	@Test
	@SneakyThrows
	void verifyProductExistenceVerifyError() {

		doThrow(new ProductNotFoundException("There are no products with the given Id.")).when(readProductUseCase)
																						 .getProductWithId(anyString());
		final Exception exception = assertThrows(ProductNotFoundException.class, () -> ProductValidator
				.verifyProductExistence("P1", readProductUseCase));
		final String expectedMessage = "There are no products with the given Id.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}

}
