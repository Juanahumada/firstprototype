/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.provider.UserProvider;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.TokenizeCardUseCase;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.util.SupplierTokenizedCardObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierUserObjectFaker;

/**
 * Test cases for the {@link SavePaymentInfoUseCaseImpl} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class SavePaymentInfoUseCaseImplTest {

	/**
	 * Mocked user provider used in the use case.
	 */
	@Mock
	private UserProvider userProvider;

	/**
	 * Instance of the tested class.
	 */
	@InjectMocks
	private SavePaymentInfoUseCaseImpl savePaymentInfoUseCase;

	/**
	 * Mocked tokenize card use case used in the use case.
	 */
	@Mock
	private TokenizeCardUseCase tokenizeCardUseCase;

	/**
	 * Mocked read user use case used in the use case.
	 */
	@Mock
	private ReadUserUseCase readUserUseCase;

	/**
	 * Tests that {@link SavePaymentInfoUseCaseImpl#savePaymentInfo} method successfully saves the payment info.
	 */
	@Test
	@SneakyThrows
	void savePaymentInfoSuccessfully() {

		when(tokenizeCardUseCase.tokenizeCard(anyString(), any(NewPaymentInfo.class))).thenReturn(SupplierTokenizedCardObjectFaker.getSuccessfulTokenizedCard1());
		when(readUserUseCase.getUserByEmail(anyString())).thenReturn(SupplierUserObjectFaker.getBasicUser("test@test.com"));
		final NewPaymentInfo newPaymentInfo = NewPaymentInfo.builder()
															.build();
		savePaymentInfoUseCase.savePaymentInfo(newPaymentInfo, "test@test.com");
		verify(userProvider).savePaymentInfo(any(User.class),any(NewPaymentInfo.class),any(TokenizedCard.class));
		Assertions.assertDoesNotThrow(() -> savePaymentInfoUseCase.savePaymentInfo(newPaymentInfo, "test@test.com"));
	}

}
