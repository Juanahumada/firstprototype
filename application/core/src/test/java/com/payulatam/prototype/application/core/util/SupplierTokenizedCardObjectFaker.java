/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.util;

import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;

/**
 * Class that supplied fake tokenized card for use in tests.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class SupplierTokenizedCardObjectFaker {

	/**
	 * Fake credit card token id for test cases.
	 */
	private static final String creditCardTokenId1 = "53af9c3a-c815-4f7a-8f13-518b12a93bf4";

	/**
	 * Fake credit card token id for test cases.
	 */
	private static final String creditCardTokenId2 = "892fee4d-5cae-4054-9e87-038ad2aa1d26";

	/**
	 * Provide a fake {@link TokenizedCard}.
	 *
	 * @return Fake tokenized card entity
	 */
	public static TokenizedCard getSuccessfulTokenizedCard1() {

		return TokenizedCard.builder()
							.creditCardTokenId(creditCardTokenId1)
							.name("full name")
							.payerId("10")
							.identificationNumber("32144457")
							.paymentMethod("VISA")
							.maskedNumber("411111******1111")
							.build();
	}

	/**
	 * Provide a fake {@link TokenizedCard}.
	 *
	 * @return Fake tokenized card entity
	 */
	public static TokenizedCard getSuccessfulTokenizedCard2() {

		return TokenizedCard.builder()
							.creditCardTokenId(creditCardTokenId2)
							.name("full name")
							.payerId("10")
							.identificationNumber("32144457")
							.paymentMethod("VISA")
							.maskedNumber("411111******1111")
							.build();
	}
}
