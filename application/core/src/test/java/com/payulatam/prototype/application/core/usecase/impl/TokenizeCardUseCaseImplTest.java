/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.util.SupplierTokenizedCardObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierUserObjectFaker;

/**
 * Test cases for the {@link TokenizeCardUseCaseImpl} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class TokenizeCardUseCaseImplTest {

	/**
	 * Mocked transaction provider used in the use case.
	 */
	@Mock
	private TransactionProvider transactionProvider;

	/**
	 * Instance of the tested class.
	 */
	@InjectMocks
	private TokenizeCardUseCaseImpl tokenizeCardUseCase;

	/**
	 * Mocked read user use case used in the use case.
	 */
	@Mock
	private ReadUserUseCase readUserUseCase;

	/**
	 * Tests that {@link TokenizeCardUseCaseImpl#tokenizeCard} method successfully tokenize the card.
	 */
	@Test
	@SneakyThrows
	void tokenizeCardShouldTokenizeCardSuccessfully() {

		final TokenizedCard tokenizedCard = SupplierTokenizedCardObjectFaker.getSuccessfulTokenizedCard1();
		final User user = SupplierUserObjectFaker.getBasicUser("test@test.com");
		when(readUserUseCase.getUserByEmail(anyString())).thenReturn(user);
		when(transactionProvider.tokenizeCard(any(User.class),any(NewPaymentInfo.class))).thenReturn(tokenizedCard);
		final NewPaymentInfo paymentInfo = NewPaymentInfo.builder()
														 .creditCardName("full name")
														 .creditCardUserDni("32144457")
														 .paymentMethod("VISA")
														 .creditCardNumber("4111111111111111")
														 .creditCardExpirationDate("2024/01")
														 .build();
		final TokenizedCard response = tokenizeCardUseCase.tokenizeCard("",paymentInfo);
		Assertions.assertEquals(response,tokenizedCard);
		verify(transactionProvider).tokenizeCard(any(User.class),any(NewPaymentInfo.class));
	}

}
