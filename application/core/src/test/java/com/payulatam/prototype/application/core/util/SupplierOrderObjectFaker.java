/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.util;

import java.util.ArrayList;
import java.util.List;

import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.OrderProduct;
import com.payulatam.prototype.application.core.usecase.entity.OrderStatus;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;

/**
 * Class that supplied fake order for use in tests.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class SupplierOrderObjectFaker {

	/**
	 * Provide a fake {@link Order} with an open state.
	 *
	 * @return Fake order entity
	 */
	public static Order getBasicOpenOrder(final String id) {

		final Product product = SupplierProductObjectFaker.getBasicProduct("P1");
		final List<OrderProduct> orderProductList = new ArrayList<>();
		final OrderProduct orderProduct = OrderProduct.builder()
													  .id("OP1")
													  .product(product)
													  .quantity(3)
													  .build();
		orderProductList.add(orderProduct);
		return Order.builder()
					.id(id)
					.status(OrderStatus.OPEN.getDetail())
					.products(orderProductList)
					.transactions(new ArrayList<>())
					.build();
	}

	/**
	 * Provide a fake {@link Order} with an open state.
	 *
	 * @return Fake order entity
	 */
	public static Order getBasicClosedOrder(final String id) {

		final List<OrderProduct> orderProductList = new ArrayList<>();
		final OrderProduct orderProduct = OrderProduct.builder()
													  .id("OP1")
													  .product(SupplierProductObjectFaker.getBasicProduct("O1"))
													  .quantity(3)
													  .build();
		orderProductList.add(orderProduct);
		final List<Transaction> transactionList = new ArrayList<>();
		transactionList.add(SupplierTransactionObjectFaker.getBasicApprovedTransaction("T1"));
		return Order.builder()
					.id(id)
					.status(OrderStatus.CLOSED.getDetail())
					.products(orderProductList)
					.transactions(transactionList)
					.build();
	}

	/**
	 * Provide a fake {@link Order} list without errors.
	 *
	 * @return Fake order list without errors.
	 */
	public static List<Order> getBasicOrderListWithoutErrors() {

		final List<Order> orderList = new ArrayList<>();
		orderList.add(getBasicClosedOrder("O1"));
		orderList.add(getBasicClosedOrder("O2"));
		orderList.add(getBasicOpenOrder("O3"));
		return orderList;
	}

	/**
	 * Provide a fake {@link Order} list with errors.
	 *
	 * @return Fake order list with errors.
	 */
	public static List<Order> getBasicOrderListWithErrors() {

		final List<Order> orderList = new ArrayList<>();
		orderList.add(getBasicClosedOrder("O1"));
		orderList.add(getBasicOpenOrder("O2"));
		orderList.add(getBasicOpenOrder("O3"));
		return orderList;
	}
}
