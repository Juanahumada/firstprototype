/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.validator;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.TokenizedCard;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PaymentMethodAlreadyExistsException;
import com.payulatam.prototype.application.core.usecase.exception.UserNotFoundException;
import com.payulatam.prototype.application.core.util.SupplierTokenizedCardObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierUserObjectFaker;

/**
 * Test cases for the {@link UserValidator} class.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class UserValidatorTest {

	/**
	 * Mocked class of the {@link ReadUserUseCase}
	 */
	@Mock
	private ReadUserUseCase readUserUseCase;

	/**
	 * Test that the method {@link UserValidator#verifyUser(String, ReadUserUseCase)}  Check successfully if the user exists
	 */
	@Test
	@SneakyThrows
	void verifyUserVerifySuccessfully() {

		when(readUserUseCase.getUserByEmail(anyString())).thenReturn(SupplierUserObjectFaker.getBasicUser("test@test.com"));
		Assertions.assertDoesNotThrow(
				() -> UserValidator.verifyUser("test@test.com", readUserUseCase));
		verify(readUserUseCase).getUserByEmail(anyString());
	}

	/**
	 * Test that the method {@link UserValidator#verifyUser(String, ReadUserUseCase)} Check with error if the user exists
	 */
	@Test
	@SneakyThrows
	void verifyUserVerifyError() {

		doThrow(new UserNotFoundException("There are no users with the given Id.")).when(readUserUseCase)
																				   .getUserByEmail(anyString());
		final Exception exception = assertThrows(UserNotFoundException.class,
												 () -> UserValidator.verifyUser("test@test.com", readUserUseCase));
		final String expectedMessage = "There are no users with the given Id.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
		verify(readUserUseCase).getUserByEmail(anyString());
	}

	/**
	 * Test that the method {@link UserValidator#verifyPaymentMethod(User, TokenizedCard)} Check successfully if the user already has the
	 * payment method
	 */
	@Test
	@SneakyThrows
	void verifyPaymentMethodVerifySuccessfully() {

		Assertions.assertDoesNotThrow(
				() -> UserValidator.verifyPaymentMethod(SupplierUserObjectFaker.getBasicUserWithOrdersAndSavedPaymentInfo("user@user.com"),
														SupplierTokenizedCardObjectFaker.getSuccessfulTokenizedCard2()));
	}

	/**
	 * Test that the method {@link UserValidator#verifyPaymentMethod(User, TokenizedCard)} Check with error if the user has the payment
	 * method
	 */
	@Test
	@SneakyThrows
	void verifyPaymentMethodVerifyError() {

		final Exception exception = assertThrows(PaymentMethodAlreadyExistsException.class,
												 () -> UserValidator.verifyPaymentMethod(
														 SupplierUserObjectFaker.getBasicUserWithOrdersAndSavedPaymentInfo("user@user.com"),
														 SupplierTokenizedCardObjectFaker.getSuccessfulTokenizedCard1()));
		final String expectedMessage = "The payment method is already associated with the user.";
		final String actualMessage = exception.getMessage();
		assertTrue(actualMessage.contains(expectedMessage));
	}
}
