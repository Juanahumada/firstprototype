/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.util;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;

/**
 * Class that supplied fake saved payment info for use in tests.
 *
 * @author <a href='juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public class SupplierSavedPaymentInfoObjectFaker {

	/**
	 * Provide a fake {@link Product}.
	 *
	 * @return Fake product entity
	 */
	public static SavedPaymentInfo getBasicSavedPaymentInfo(final String id) {

		return SavedPaymentInfo.builder()
							   .id(id)
							   .billingAddressCity("Bogotá")
							   .billingAddressCountry("CO")
							   .billingAddressPhone("3162227585")
							   .billingAddressPostalCode("1234")
							   .billingAddressState("CO")
							   .billingAddressStreet1("Calle 90")
							   .billingAddressStreet2("85")
							   .tokenizedCard(SupplierTokenizedCardObjectFaker.getSuccessfulTokenizedCard1())
							   .build();
	}
}
