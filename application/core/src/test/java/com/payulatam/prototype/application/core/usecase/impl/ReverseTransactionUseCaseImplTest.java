/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.core.usecase.impl;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.apache.commons.lang3.StringUtils;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import lombok.SneakyThrows;

import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.core.util.SupplierOrderObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierTransactionObjectFaker;
import com.payulatam.prototype.application.core.util.SupplierUserObjectFaker;

/**
 * Test cases for the {@link ReverseTransactionUseCaseImpl} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@ExtendWith(MockitoExtension.class)
class ReverseTransactionUseCaseImplTest {

	/**
	 * Mocked transaction provider used in the use case.
	 */
	@Mock
	private TransactionProvider transactionProvider;

	/**
	 * Instance of the tested class.
	 */
	@InjectMocks
	private ReverseTransactionUseCaseImpl reverseTransactionUseCase;

	/**
	 * Mocked read order use case used in the use case.
	 */
	@Mock
	private ReadOrderUseCase readOrderUseCase;

	/**
	 * Mocked read transaction use case used in the use case.
	 */
	@Mock
	private ReadTransactionUseCase readTransactionUseCase;

	/**
	 * Mocked read user use case used in the use case.
	 */
	@Mock
	private ReadUserUseCase readUserUseCase;

	/**
	 * Tests that {@link ReverseTransactionUseCaseImpl#reverseTransaction} method successfully reverse the transaction.
	 */
	@Test
	@SneakyThrows
	void reverseTransactionSuccessfully() {

		when(readOrderUseCase.getOrderById(anyString())).thenReturn(SupplierOrderObjectFaker.getBasicClosedOrder("O1"));
		when(readUserUseCase.getUserByEmail(anyString())).thenReturn(SupplierUserObjectFaker.getBasicUserWithOrdersAndSavedPaymentInfo("test@test.com"));
		when(readTransactionUseCase.getTransactionWithId(anyString())).thenReturn(SupplierTransactionObjectFaker.getBasicApprovedTransaction("T1"));
		reverseTransactionUseCase.reverseTransaction(StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY,StringUtils.EMPTY);
		verify(transactionProvider).reverseTransaction(any(Order.class),any(Transaction.class),anyString());
		Assertions.assertDoesNotThrow(() -> reverseTransactionUseCase.reverseTransaction(StringUtils.EMPTY,StringUtils.EMPTY,
																						 StringUtils.EMPTY,StringUtils.EMPTY));
	}
}
