/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.service.impl;

import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.TransactionDto;
import com.payulatam.prototype.application.entrypoint.presenter.TransactionPresenter;
import com.payulatam.prototype.application.entrypoint.service.TransactionService;

/**
 * Implementation of {@link TransactionService} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
@RequiredArgsConstructor
public class TransactionServiceImpl implements TransactionService {

	/**
	 * Presenter of the product uses cases.
	 */
	private final TransactionPresenter presenter;

	/**
	 * Checkout the open order for the given user with the given Payment information.
	 *
	 * @param newPaymentInfoDto The payment information.
	 * @param userId            The id of the user that has the order.
	 * @param orderId           The id of the order to make a checkout.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	@Override public TransactionDto checkoutOrderWithNewPaymentMethod(final NewPaymentInfoDto newPaymentInfoDto,
																	  final String orderId, final String userId) throws PrototypeException {

		return presenter.checkoutOrderWithNewPaymentMethod(newPaymentInfoDto, orderId, userId);
	}

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param paymentId The payment id.
	 * @param orderId   The id of the order to make a checkout.
	 * @param userId    The id of the user that has the order.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	@Override public TransactionDto checkoutOrderWithSavedPaymentMethod(final String paymentId, final String orderId, final String userId)
			throws PrototypeException {

		return presenter.checkoutOrderWithSavedPaymentMethod(paymentId, orderId, userId);
	}

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param userId        The id of the user that has the order.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the refund.
	 * @throws PrototypeException When the order, user or transaction does not exists.
	 */
	@Override public void refundOrder(final String orderId, final String userId, final String transactionId, final String reason)
			throws PrototypeException {

		presenter.refundOrder(orderId, userId, transactionId, reason);
	}

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	@Override public String ping() {

		return presenter.ping();
	}
}
