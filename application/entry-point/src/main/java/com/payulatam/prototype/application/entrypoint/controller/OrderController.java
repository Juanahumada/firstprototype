/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller;

import java.time.ZonedDateTime;
import java.util.List;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.OrderDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.ResponseDto;
import com.payulatam.prototype.application.entrypoint.controller.exception.Responses;
import com.payulatam.prototype.application.entrypoint.service.OrderService;

/**
 * Order controller.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/v1.0/order")
@RequiredArgsConstructor
public class OrderController {

	/**
	 * Instance of the service that provides information to the controller about the products.
	 */
	private final OrderService service;

	/**
	 * Adds a product to an order. If the order does not exists, creates a new one.
	 *
	 * @return {@link ResponseDto} With the result message of the operation.
	 * @throws PrototypeException When the product does not exists.
	 */
	@PostMapping
	@Transactional
	public ResponseEntity<ResponseDto> addProductToAnOrder(final String productId, final Integer quantity,
														   final String userId) throws PrototypeException {

		service.addProductToOrder(productId, quantity, userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.UPDATED.getDetail())
											  .build());
	}

	/**
	 * Get a list of all orders in the store.
	 *
	 * @return {@link ResponseDto} With a list of orders.
	 */
	@GetMapping("/all")
	public ResponseEntity<ResponseDto> getAllOrders() {

		final List<OrderDto> orders = service.getAllOrders();
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(orders)
											  .build());

	}

	/**
	 * Get a list of all orders that the given user has.
	 *
	 * @param userId Id of the user.
	 * @return {@link ResponseDto} With a list of orders.
	 * @throws PrototypeException When the user does not exists.
	 */
	@GetMapping()
	public ResponseEntity<ResponseDto> getUserOrders(final String userId) throws PrototypeException {

		final List<OrderDto> orders = service.getUserOrders(userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(orders)
											  .build());

	}

	/**
	 * Get a list of the open order that the given user has.
	 *
	 * @param userId Id of the user.
	 * @return {@link ResponseDto} With a list of orders.
	 * @throws PrototypeException When the user does not exists.
	 */
	@GetMapping(("/open"))
	public ResponseEntity<ResponseDto> getOpenOrder(final String userId) throws PrototypeException {

		final OrderDto order = service.getOpenOrder(userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(order)
											  .build());

	}

	/**
	 * Get an order with the given id.
	 *
	 * @param orderId Id of the order.
	 * @return {@link ResponseDto} With the order.
	 * @throws PrototypeException When the order does not exists.
	 */
	@GetMapping("/single")
	public ResponseEntity<ResponseDto> getOrder(final String orderId) throws PrototypeException {

		final OrderDto order = service.getOrder(orderId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(order)
											  .build());

	}

}
