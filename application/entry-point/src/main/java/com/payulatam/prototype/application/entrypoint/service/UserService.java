/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.service;

import java.util.List;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.PaymentInfoDto;

/**
 * Handles all the logic needed for the Users in the entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface UserService extends UserDetailsService {

	/**
	 * Adds a new payment info to the given user.
	 *
	 * @param newPaymentInfo New payment info to add to the user.
	 * @param userId         User id.
	 * @throws PrototypeException When the user does not exists.
	 */
	void saveNewPaymentInfo(NewPaymentInfoDto newPaymentInfo, String userId) throws PrototypeException;

	/**
	 * Gets the saved payment methods for an user.
	 *
	 * @param userId Id of the user
	 * @return List of the user's payment info
	 * @throws PrototypeException When the user does not exists.
	 */
	List<PaymentInfoDto> getPaymentMethods(final String userId) throws PrototypeException;
}
