/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import java.math.BigDecimal;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Product Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class OrderDto {

	/**
	 * Order id
	 */
	private String id;

	/**
	 * Order total value
	 */
	private BigDecimal totalValue;
	/**
	 * Order status
	 */
	private String status;
	/**
	 * Products of the order.
	 */
	private List<OrderProductDto> products;
	/**
	 * Transactions of the order.
	 */
	private List<TransactionDto> transactions;
}
