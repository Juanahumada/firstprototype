/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * NewPaymentInfo Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class NewPaymentInfoDto {

	/**
	 * Payment info credit card number
	 */
	private String creditCardNumber;
	/**
	 * Payment info credit card security code
	 */
	private String creditCardSecurityCode;
	/**
	 * Payment info credit card expiration date
	 */
	private String creditCardExpirationDate;
	/**
	 * Payment info owner dni
	 */
	private String creditCardUserDni;
	/**
	 * Payment info card name
	 */
	private String creditCardName;
	/**
	 * Payment info billing address street 1
	 */
	private String billingAddressStreet1;
	/**
	 * Payment info billing address street 2
	 */
	private String billingAddressStreet2;
	/**
	 * Payment info billing address city
	 */
	private String billingAddressCity;
	/**
	 * Payment info billing address state
	 */
	private String billingAddressState;
	/**
	 * Payment info billing address country
	 */
	private String billingAddressCountry;
	/**
	 * Payment info billing address postal code
	 */
	private String billingAddressPostalCode;
	/**
	 * Payment info billing address phone
	 */
	private String billingAddressPhone;
	/**
	 * Payment info payment method
	 */
	private String paymentMethod;
}
