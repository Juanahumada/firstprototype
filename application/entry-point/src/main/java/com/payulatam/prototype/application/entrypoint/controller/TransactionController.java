/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller;

import java.time.ZonedDateTime;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.ResponseDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.TransactionDto;
import com.payulatam.prototype.application.entrypoint.controller.exception.Responses;
import com.payulatam.prototype.application.entrypoint.service.TransactionService;

/**
 * Transaction controller.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/v1.0/transaction")
@RequiredArgsConstructor
public class TransactionController {

	/**
	 * Instance of the service that provides information to the controller about the transaction.
	 */
	private final TransactionService service;

	@GetMapping("/ping")
	public ResponseEntity<ResponseDto> ping() {

		final String response = service.ping();
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(response)
											  .build());
	}

	/**
	 * Checkout the open order for the given user with the given Payment information.
	 *
	 * @param newPaymentInfoDto The payment information.
	 * @param userId            The id of the user that has the order.
	 * @param orderId           The id of the order to make a checkout.
	 * @return {@link ResponseDto} With the result message of the operation.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	@PostMapping("/checkoutOrderNewPaymentInfo")
	@Transactional
	public ResponseEntity<ResponseDto> checkoutOrderWithNewPaymentMethod(@Valid @RequestBody final NewPaymentInfoDto newPaymentInfoDto,
																		 final String orderId,
																		 final String userId) throws PrototypeException {

		final TransactionDto transactionDto = service.checkoutOrderWithNewPaymentMethod(newPaymentInfoDto, orderId, userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(transactionDto)
											  .build());
	}

	/**
	 * Checkout the open order for the given user with saved Payment information.
	 *
	 * @param paymentId The payment id
	 * @param userId    The id of the user that has the order.
	 * @param orderId   The id of the order to make a checkout.
	 * @return {@link ResponseDto} With the result message of the operation.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	@PostMapping("/checkoutOrderSavedPaymentInfo")
	@Transactional
	public ResponseEntity<ResponseDto> checkoutOrderWithSavedPaymentMethod(final String paymentId,
																		   final String orderId,
																		   final String userId) throws PrototypeException {

		final TransactionDto transactionDto = service.checkoutOrderWithSavedPaymentMethod(paymentId, orderId, userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(transactionDto)
											  .build());
	}

	/**
	 * Checkout the open order for the given user with saved Payment information.
	 *
	 * @param userId        The id of the user that has the order.
	 * @param orderId       The id of the order to make a checkout.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the refund.
	 * @return {@link ResponseDto} With the result message of the operation.
	 * @throws PrototypeException When the order, user or transaction does not exists.
	 */
	@PostMapping("/refund")
	@Transactional
	public ResponseEntity<ResponseDto> refundOrder(@RequestParam final String orderId,
												   @RequestParam final String userId, @RequestParam final String transactionId,
												   @RequestBody final String reason)
			throws PrototypeException {

		service.refundOrder(orderId, userId, transactionId, reason);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .build());
	}

}
