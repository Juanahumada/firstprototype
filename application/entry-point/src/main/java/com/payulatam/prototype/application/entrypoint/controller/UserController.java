/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller;

import java.time.ZonedDateTime;
import java.util.List;
import javax.validation.Valid;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.PaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.ResponseDto;
import com.payulatam.prototype.application.entrypoint.controller.exception.Responses;
import com.payulatam.prototype.application.entrypoint.service.UserService;

/**
 * User controller.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/v1.0/user")
@RequiredArgsConstructor
public class UserController {

	/**
	 * Instance of the service that provides information to the controller about the users.
	 */
	private final UserService service;

	/**
	 * Adds a payment method to the user.
	 *
	 * @return {@link ResponseDto} With the result message of the operation.
	 * @throws PrototypeException When the user does not exists.
	 */
	@PostMapping("/savePaymentMethod")
	public ResponseEntity<ResponseDto> addPaymentMethod(@Valid @RequestBody final NewPaymentInfoDto newPaymentInfoDto,
														@RequestParam final String userId)
			throws
			PrototypeException {

		service.saveNewPaymentInfo(newPaymentInfoDto, userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.SAVED.getDetail())
											  .build());
	}

	/**
	 * Gets the saved payment methods for an user.
	 *
	 * @param userId Id of the user
	 * @return {@link ResponseDto} With the result message of the operation.
	 * @throws PrototypeException When the user does not exists.
	 */
	@GetMapping("/paymentMethods")
	public ResponseEntity<ResponseDto> getPaymentMethods(final String userId) throws PrototypeException {

		final List<PaymentInfoDto> paymentMethods = service.getPaymentMethods(userId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(paymentMethods)
											  .build());
	}
}
