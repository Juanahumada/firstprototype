/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.exception.handler;

import java.time.ZonedDateTime;

import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.dao.DataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.lang.NonNull;
import org.springframework.validation.BindException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingRequestHeaderException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import lombok.extern.slf4j.Slf4j;

import com.payulatam.prototype.application.core.usecase.exception.DataConsistenceException;
import com.payulatam.prototype.application.core.usecase.exception.ExternalTransactionApiException;
import com.payulatam.prototype.application.core.usecase.exception.FileException;
import com.payulatam.prototype.application.core.usecase.exception.InvalidFileException;
import com.payulatam.prototype.application.core.usecase.exception.OpenOrdersQuantityException;
import com.payulatam.prototype.application.core.usecase.exception.OrderInvalidStateException;
import com.payulatam.prototype.application.core.usecase.exception.OrderNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.PaymentInfoNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.PaymentMethodAlreadyExistsException;
import com.payulatam.prototype.application.core.usecase.exception.ProductAlreadyExistsException;
import com.payulatam.prototype.application.core.usecase.exception.ProductNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.ProductNotStockException;
import com.payulatam.prototype.application.core.usecase.exception.ProductReceivedExcepion;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.core.usecase.exception.TransactionInvalidStateException;
import com.payulatam.prototype.application.core.usecase.exception.TransactionNotFoundException;
import com.payulatam.prototype.application.core.usecase.exception.UserNotFoundException;
import com.payulatam.prototype.application.entrypoint.controller.dto.ResponseDto;

/**
 * Exception handler
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@RestControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
@Slf4j
public class RestErrorHandler extends ResponseEntityExceptionHandler {

	/**
	 * Handle a {@link DataConsistenceException} on the entry point layer
	 *
	 * @return a responseEntity that shows a conflict error
	 */
	@ExceptionHandler(DataConsistenceException.class)
	public ResponseEntity<ResponseDto> dataInconsistenciesFound(final DataConsistenceException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.CONFLICT.value())
											  .status(HttpStatus.CONFLICT.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link ExternalTransactionApiException} on the entry point layer
	 *
	 * @return a responseEntity that shows a failed dependency error
	 */
	@ExceptionHandler(ExternalTransactionApiException.class)
	public ResponseEntity<ResponseDto> externalApiException(final ExternalTransactionApiException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.FAILED_DEPENDENCY.value())
											  .status(HttpStatus.FAILED_DEPENDENCY.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link OpenOrdersQuantityException} on the entry point layer
	 *
	 * @return a responseEntity that shows a conflict error
	 */
	@ExceptionHandler(OpenOrdersQuantityException.class)
	public ResponseEntity<ResponseDto> openOrdersQuantityException(final OpenOrdersQuantityException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.CONFLICT.value())
											  .status(HttpStatus.CONFLICT.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link OrderInvalidStateException} on the entry point layer
	 *
	 * @return a responseEntity that shows a conflict error
	 */
	@ExceptionHandler(OrderInvalidStateException.class)
	public ResponseEntity<ResponseDto> orderInvalidStateException(final OrderInvalidStateException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link OrderNotFoundException} on the entry point layer
	 *
	 * @return a responseEntity that shows a not found error
	 */
	@ExceptionHandler(OrderNotFoundException.class)
	public ResponseEntity<ResponseDto> orderNotFoundException(final OrderNotFoundException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.NOT_FOUND.value())
											  .status(HttpStatus.NOT_FOUND.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link PaymentInfoNotFoundException} on the entry point layer
	 *
	 * @return a responseEntity that shows a not found error
	 */
	@ExceptionHandler(PaymentInfoNotFoundException.class)
	public ResponseEntity<ResponseDto> paymentInfoNotFoundException(final PaymentInfoNotFoundException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.NOT_FOUND.value())
											  .status(HttpStatus.NOT_FOUND.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link PaymentMethodAlreadyExistsException} on the entry point layer
	 *
	 * @return a responseEntity that shows a conflict error
	 */
	@ExceptionHandler(PaymentMethodAlreadyExistsException.class)
	public ResponseEntity<ResponseDto> paymentMethodAlreadyExistsException(final PaymentMethodAlreadyExistsException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.CONFLICT.value())
											  .status(HttpStatus.CONFLICT.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link ProductAlreadyExistsException} on the entry point layer
	 *
	 * @return a responseEntity that shows a conflict error
	 */
	@ExceptionHandler(ProductAlreadyExistsException.class)
	public ResponseEntity<ResponseDto> productAlreadyExistsException(final ProductAlreadyExistsException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.CONFLICT)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.CONFLICT.value())
											  .status(HttpStatus.CONFLICT.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link ProductNotFoundException} on the entry point layer
	 *
	 * @return a responseEntity that shows a not found error
	 */
	@ExceptionHandler(ProductNotFoundException.class)
	public ResponseEntity<ResponseDto> productNotFoundException(final ProductNotFoundException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.NOT_FOUND.value())
											  .status(HttpStatus.NOT_FOUND.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link FileException} on the entry point layer
	 *
	 * @return a responseEntity that shows an internal server error
	 */
	@ExceptionHandler(FileException.class)
	public ResponseEntity<ResponseDto> fileException(final FileException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
											  .status(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link ProductReceivedExcepion} on the entry point layer
	 *
	 * @return a responseEntity that shows a bad request error
	 */
	@ExceptionHandler(ProductReceivedExcepion.class)
	public ResponseEntity<ResponseDto> productReceivedException(final ProductReceivedExcepion e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link InvalidFileException} on the entry point layer
	 *
	 * @return a responseEntity that shows a bad request error
	 */
	@ExceptionHandler(InvalidFileException.class)
	public ResponseEntity<ResponseDto> invalidFileException(final InvalidFileException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link ProductNotStockException} on the entry point layer
	 *
	 * @return a responseEntity that shows a bad request error
	 */
	@ExceptionHandler(ProductNotStockException.class)
	public ResponseEntity<ResponseDto> productNotStockException(final ProductNotStockException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link TransactionInvalidStateException} on the entry point layer
	 *
	 * @return a responseEntity that shows a bad request error
	 */
	@ExceptionHandler(TransactionInvalidStateException.class)
	public ResponseEntity<ResponseDto> transactionInvalidStateException(final TransactionInvalidStateException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link TransactionNotFoundException} on the entry point layer
	 *
	 * @return a responseEntity that shows a not found error
	 */
	@ExceptionHandler(TransactionNotFoundException.class)
	public ResponseEntity<ResponseDto> transactionNotFoundException(final TransactionNotFoundException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.NOT_FOUND.value())
											  .status(HttpStatus.NOT_FOUND.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link DataAccessException} on the entry point layer
	 *
	 * @return a responseEntity that shows a not found error
	 */
	@ExceptionHandler(DataAccessException.class)
	public ResponseEntity<ResponseDto> dataAccessException(final DataAccessException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.FAILED_DEPENDENCY)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.FAILED_DEPENDENCY.value())
											  .status(HttpStatus.FAILED_DEPENDENCY.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link UserNotFoundException} on the entry point layer
	 *
	 * @return a responseEntity that shows a not found error
	 */
	@ExceptionHandler(UserNotFoundException.class)
	public ResponseEntity<ResponseDto> userNotFoundException(final UserNotFoundException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.NOT_FOUND)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.NOT_FOUND.value())
											  .status(HttpStatus.NOT_FOUND.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle a {@link PrototypeException} on the entry point layer
	 *
	 * @return a responseEntity that shows an internal server error error
	 */
	@ExceptionHandler(PrototypeException.class)
	public ResponseEntity<ResponseDto> prototypeException(final PrototypeException e) {

		log.info(e.getMessage());
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
											  .status(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
											  .responseMessage(e.getMessage())
											  .build());
	}

	/**
	 * Handle an {@link MissingRequestHeaderException}.
	 *
	 * @param ex MissingRequestHeaderException.
	 */
	@ResponseBody
	@ExceptionHandler(MissingRequestHeaderException.class)
	public ResponseEntity<ResponseDto> missingRequestHeaderExceptionHandler(final MissingRequestHeaderException ex) {

		log.info(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(ex.getMessage())
											  .build());
	}

	/**
	 * Customize the response for MethodArgumentNotValidException.
	 * <p>This method delegates to {@link #handleExceptionInternal}.
	 *
	 * @param ex      the exception
	 * @param headers the headers to be written to the response
	 * @param status  the selected response status
	 * @param request the current request
	 * @return a {@code ResponseEntity} instance
	 */
	@Override
	@NonNull
	protected ResponseEntity<Object> handleMethodArgumentNotValid(final MethodArgumentNotValidException ex,
																  final @NonNull HttpHeaders headers,
																  final @NonNull HttpStatus status,
																  final @NonNull WebRequest request) {

		log.info(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(ex.getMessage())
											  .build());
	}

	/**
	 * Customize the response for HttpMessageNotReadableException.
	 * <p>This method delegates to {@link #handleExceptionInternal}.
	 *
	 * @param ex      the exception
	 * @param headers the headers to be written to the response
	 * @param status  the selected response status
	 * @param request the current request
	 * @return a {@code ResponseEntity} instance
	 */
	@Override
	@NonNull
	protected ResponseEntity<Object> handleHttpMessageNotReadable(final @NonNull HttpMessageNotReadableException ex,
																  final @NonNull HttpHeaders headers,
																  final @NonNull HttpStatus status,
																  final @NonNull WebRequest request) {

		log.info(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(ex.getMessage())
											  .build());
	}

	/**
	 * Customize the response for handleMissingServletRequestParameter.
	 *
	 * @param ex      the exception
	 * @param headers the headers to be written to the response
	 * @param status  the selected response status
	 * @param request the current request
	 * @return a {@code ResponseEntity} instance
	 */
	@Override
	@NonNull
	protected ResponseEntity<Object> handleMissingServletRequestParameter(
			final @NonNull MissingServletRequestParameterException ex,
			final @NonNull HttpHeaders headers,
			final @NonNull HttpStatus status,
			final @NonNull WebRequest request) {

		log.info(ex.getMessage());
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(ex.getMessage())
											  .build());

	}

	/**
	 * Customize the response for BindException.
	 * <p>This method delegates to {@link #handleExceptionInternal}.
	 *
	 * @param ex      the exception
	 * @param headers the headers to be written to the response
	 * @param status  the selected response status
	 * @param request the current request
	 * @return a {@code ResponseEntity} instance
	 */
	@Override
	@NonNull
	protected ResponseEntity<Object> handleBindException(final @NonNull BindException ex,
														 final @NonNull HttpHeaders headers,
														 final @NonNull HttpStatus status,
														 final @NonNull WebRequest request) {

		log.info(ex.getMessage());
		final StringBuilder messageBuilder = new StringBuilder("Check Request: ");
		ex.getAllErrors().forEach(objectError -> messageBuilder.append("[")
															   .append(objectError.getDefaultMessage())
															   .append("]"));
		return ResponseEntity.status(HttpStatus.BAD_REQUEST)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.BAD_REQUEST.value())
											  .status(HttpStatus.BAD_REQUEST.getReasonPhrase())
											  .responseMessage(messageBuilder.toString())
											  .build());
	}

	/**
	 * Handle an {@link RuntimeException}.
	 *
	 * @param ex Exception.
	 */
	@ResponseBody
	@ExceptionHandler(RuntimeException.class)
	public ResponseEntity<ResponseDto> internalErrorHandler(final RuntimeException ex) {

		log.warn("An unexpected error occurred while processing the request:[{}]", ex.getMessage(), ex);
		return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.INTERNAL_SERVER_ERROR.value())
											  .status(HttpStatus.INTERNAL_SERVER_ERROR.getReasonPhrase())
											  .responseMessage(ex.getMessage())
											  .build());
	}
}
