/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.PaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.UserDto;

/**
 * Class that adapts the product message from core layer to entry point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface UserPresenter {

	/**
	 * Method that brings a user with the given email from the core layer to the entry point layer.
	 *
	 * @param email Email of the user.
	 * @return User dto with the given email.
	 * @throws PrototypeException When the user with the given Id does not exist
	 */
	UserDto getUserByEmail(String email) throws PrototypeException;

	/**
	 * Adds a new payment info to the given user.
	 *
	 * @param newPaymentInfo New payment info to add to the user.
	 * @param userId         User id.
	 * @throws PrototypeException When the user does not exists.
	 */
	void saveNewPaymentInfo(NewPaymentInfoDto newPaymentInfo, String userId) throws PrototypeException;

	/**
	 * Gets the saved payment methods for an user.
	 *
	 * @param userId If of the user
	 * @return List of the user payment info
	 * @throws PrototypeException When the user does not exists.
	 */
	List<PaymentInfoDto> getPaymentMethods(final String userId) throws PrototypeException;
}
