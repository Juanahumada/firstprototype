/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * User Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class UserDto {

	/**
	 * User id.
	 */
	private String id;

	/**
	 * User dni.
	 */
	private String dni;

	/**
	 * User name.
	 */
	private String name;

	/**
	 * User last name.
	 */
	private String lastName;

	/**
	 * User email
	 */
	private String email;

	/**
	 * User phone
	 */
	private String phone;

	/**
	 * User password
	 */
	private String password;

	/**
	 * User status
	 */
	private Boolean enabled;

	/**
	 * User roles
	 */
	private List<RoleDto> roles;

	/**
	 * List of the payment information that the owner has.
	 */
	private List<PaymentInfoDto> paymentsInfo;

	/**
	 * List of the orders of the user.
	 */
	private List<OrderDto> orders;
}
