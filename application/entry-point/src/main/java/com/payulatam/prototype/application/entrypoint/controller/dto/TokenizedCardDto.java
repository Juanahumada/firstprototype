/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Tokenized card Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TokenizedCardDto {

	/**
	 * Tokenized card token id
	 */
	private String creditCardTokenId;
	/**
	 * Tokenized card payment info id.
	 */
	private PaymentInfoDto paymentInfoId;
	/**
	 * Tokenized card name
	 */
	private String name;
	/**
	 * Tokenized card payer id
	 */
	private String payerId;
	/**
	 * Tokenized card id number
	 */
	private String identificationNumber;
	/**
	 * Tokenized card payment method
	 */
	private String paymentMethod;
	/**
	 * Tokenized card masked number
	 */
	private String maskedNumber;
}
