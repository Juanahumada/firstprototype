/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.service.impl;

import java.util.List;

import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.OrderDto;
import com.payulatam.prototype.application.entrypoint.presenter.OrderPresenter;
import com.payulatam.prototype.application.entrypoint.service.OrderService;

/**
 * Implementation of {@link OrderService} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
@RequiredArgsConstructor
public class OrderServiceImpl implements OrderService {

	/**
	 * Presenter of the product uses cases.
	 */
	private final OrderPresenter presenter;

	/**
	 * Adds product to an order
	 *
	 * @param productId Product id.
	 * @param quantity  Quantity of the product to add to the order.
	 * @param userId    User id.
	 * @throws PrototypeException When the product does not exists.
	 */
	@Override
	public void addProductToOrder(final String productId, final Integer quantity, final String userId) throws
																									   PrototypeException {

		presenter.addProductToOrder(productId, quantity, userId);
	}

	/**
	 * Get all the orders.
	 *
	 * @return All the orders in the store.
	 */
	@Override public List<OrderDto> getAllOrders() {

		return presenter.getAllOrders();
	}

	/**
	 * Get all the orders that a user has.
	 *
	 * @param userId The id of the user.
	 * @return All the orders in the store.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public List<OrderDto> getUserOrders(final String userId) throws PrototypeException {

		return presenter.getUserOrders(userId);
	}

	/**
	 * Get the open order that a user has.
	 *
	 * @param userId The id of the user.
	 * @return Open orders for the user.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public OrderDto getOpenOrder(final String userId) throws PrototypeException {

		return presenter.getOpenOrder(userId);
	}

	/**
	 * Get the order with the given id.
	 *
	 * @param orderId The id of the order.
	 * @return The order with the given id.
	 * @throws PrototypeException When the order does not exists.
	 */
	@Override public OrderDto getOrder(final String orderId) throws PrototypeException {

		return presenter.getOrder(orderId);
	}
}
