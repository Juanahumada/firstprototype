/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.exception;

import lombok.Getter;

/**
 * Responses for the api.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
public enum Responses {
	/**
	 * Response type when the object was created.
	 */
	CREATED("CREATED"),

	/**
	 * Response type when the object was updated.
	 */
	UPDATED("UPDATED"),

	/**
	 * Response type when the object was saved
	 */
	SAVED("SAVED"),

	/**
	 * Response type when the object was deleted.
	 */
	DELETED("DELETED"),

	/**
	 * Response type when a bad request error occurs.
	 */
	BAD_REQUEST("Check the request"),
	/**
	 * Response type when everything is ok.
	 */
	OK("OK"),

	/**
	 * Response type when when a internal error occurs.
	 */
	INTERNAL_ERROR("An error has occurred while processing the request");

	/**
	 * Response string representation.
	 **/
	private final String detail;

	/**
	 * Constructor with description injected by dependency inversion
	 *
	 * @param detail exception string representation
	 */
	Responses(final String detail) {

		this.detail = detail;
	}
}
