/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.impl;

import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.MakeTransactionNewPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.MakeTransactionSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.PingTransactionExternalApiUseCase;
import com.payulatam.prototype.application.core.usecase.ReverseTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.TransactionDto;
import com.payulatam.prototype.application.entrypoint.presenter.TransactionPresenter;
import com.payulatam.prototype.application.entrypoint.presenter.converter.NewPaymentInfoDtoMapper;
import com.payulatam.prototype.application.entrypoint.presenter.converter.TransactionDtoMapper;

/**
 * Implementation of the {@link TransactionPresenter} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@RequiredArgsConstructor
public class TransactionPresenterImpl implements TransactionPresenter {

	/**
	 * Make transaction with new payment info use case.
	 */
	private final MakeTransactionNewPaymentInfoUseCase makeTransactionNewPaymentInfoUseCase;

	/**
	 * Make transaction with saved payment info use case.
	 */
	private final MakeTransactionSavedPaymentInfoUseCase makeTransactionSavedPaymentInfoUseCase;

	/**
	 * Ping transaction external api use case.
	 */
	private final PingTransactionExternalApiUseCase pingTransactionExternalApiUseCase;

	/**
	 * Reverse transaction api use case.
	 */
	private final ReverseTransactionUseCase reverseTransactionUseCase;

	/**
	 * New payment info mapper for entry point model and core model.
	 */
	private final NewPaymentInfoDtoMapper newPaymentInfoDtoMapper;

	/**
	 * Transaction dto mapper for entry point model and core model.
	 */
	private final TransactionDtoMapper transactionDtoMapper;

	/**
	 * Checkout the open order for the given user with the given Payment information.
	 *
	 * @param newPaymentInfoDto The payment information.
	 * @param userId            The id of the user that has the order.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	@Override public TransactionDto checkoutOrderWithNewPaymentMethod(final NewPaymentInfoDto newPaymentInfoDto,
																	  final String orderId, final String userId) throws PrototypeException {

		return transactionDtoMapper.toDto(makeTransactionNewPaymentInfoUseCase.makeTransactionWithNewPaymentInfo(userId, orderId,
																												 newPaymentInfoDtoMapper
																														 .toEntity(
																																 newPaymentInfoDto)));
	}

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param paymentId The payment id.
	 * @param orderId   The id of the order to make a checkout.
	 * @param userId    The id of the user that has the order.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public TransactionDto checkoutOrderWithSavedPaymentMethod(final String paymentId, final String orderId, final String userId)
			throws PrototypeException {

		return transactionDtoMapper.toDto(makeTransactionSavedPaymentInfoUseCase.makeTransactionWithSavedPaymentInfo(userId
				, orderId, paymentId));

	}

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	@Override public String ping() {

		return pingTransactionExternalApiUseCase.ping();
	}

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param orderId       The id of the order to make a checkout.
	 * @param userId        The id of the user that has the order.
	 * @param transactionId The id of the transaction.
	 * @throws PrototypeException When the user, order or transaction does not exists.
	 */
	@Override public TransactionDto refundOrder(final String orderId, final String userId, final String transactionId, final String reason)
			throws PrototypeException {

		return transactionDtoMapper.toDto(reverseTransactionUseCase.reverseTransaction(orderId, userId, transactionId, reason));
	}

}
