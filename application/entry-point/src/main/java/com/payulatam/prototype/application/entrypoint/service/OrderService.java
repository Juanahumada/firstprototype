/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.service;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.OrderDto;

/**
 * Handles all the logic needed for the Orders in the entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface OrderService {

	/**
	 * Adds product to an order
	 *
	 * @param productId Product id.
	 * @param quantity  Quantity of the product to add to the order.
	 * @param userId    User id.
	 * @throws PrototypeException When the product does not exists.
	 */
	void addProductToOrder(String productId, Integer quantity, String userId) throws PrototypeException;

	/**
	 * Get all the orders.
	 *
	 * @return All the orders in the store.
	 */
	List<OrderDto> getAllOrders();

	/**
	 * Get all the orders that a user has.
	 *
	 * @param userId The id of the user.
	 * @return All the orders in the store.
	 * @throws PrototypeException When the user does not exists.
	 */
	List<OrderDto> getUserOrders(String userId) throws PrototypeException;

	/**
	 * Get the open order that a user has.
	 *
	 * @param userId The id of the user.
	 * @return All the orders in the store.
	 * @throws PrototypeException When the user does not exists.
	 */
	OrderDto getOpenOrder(String userId) throws PrototypeException;

	/**
	 * Get the order with the given id.
	 *
	 * @param orderId The id of the order.
	 * @return The order with the given id.
	 * @throws PrototypeException When the order does not exists.
	 */
	OrderDto getOrder(String orderId) throws PrototypeException;
}
