/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import java.time.ZonedDateTime;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Builder;
import lombok.Getter;

/**
 * Api response DTO
 *
 * @author <a href="mailto:juan.ahumada@payu.com">Juan Daniel Ahumada</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ResponseDto {

	/**
	 * Time when the request was processed
	 */
	private final ZonedDateTime time;

	/**
	 * Request response code
	 */
	private final Integer responseCode;

	/**
	 * Request HTTP status
	 */
	private final String status;

	/**
	 * Response message
	 */
	private final String responseMessage;

	/**
	 * Object if it is needed
	 */
	private final Object object;
}
