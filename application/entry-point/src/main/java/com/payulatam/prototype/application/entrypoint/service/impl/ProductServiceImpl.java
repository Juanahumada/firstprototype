/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.service.impl;

import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.ProductDto;
import com.payulatam.prototype.application.entrypoint.presenter.ProductPresenter;
import com.payulatam.prototype.application.entrypoint.service.ProductService;

/**
 * Implementation of {@link ProductService} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
@RequiredArgsConstructor
public class ProductServiceImpl implements ProductService {

	/**
	 * Presenter of the product uses cases.
	 */
	private final ProductPresenter presenter;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductDto> getAllProducts() {

		return presenter.getAllProducts();
	}

	/**
	 * Saves a new product
	 *
	 * @param productDto New product to be saved
	 * @throws PrototypeException when the product already exists
	 */
	@Override public void saveNewProduct(final ProductDto productDto) throws PrototypeException {

		presenter.saveNewProduct(productDto);
	}

	/**
	 * Deletes a product with the given id
	 *
	 * @param productId Id of the product
	 * @throws PrototypeException If the product does not exists.
	 */
	@Override public void deleteProduct(final String productId) throws PrototypeException {

		presenter.deleteProduct(productId);
	}

	/**
	 * Gets the photo of a given product
	 *
	 * @param productId The id of the product
	 * @return The photo of the product with the given id
	 * @throws PrototypeException If the product does not exists.
	 */
	@Override public Resource getProductPhoto(final String productId) throws PrototypeException {

		return presenter.getProductPhoto(productId);
	}
}
