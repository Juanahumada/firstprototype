/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.FileException;
import com.payulatam.prototype.application.core.usecase.exception.InvalidFileException;
import com.payulatam.prototype.application.core.usecase.exception.ProductReceivedExcepion;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.ProductDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.ResponseDto;
import com.payulatam.prototype.application.entrypoint.controller.exception.Responses;
import com.payulatam.prototype.application.entrypoint.service.ProductService;

/**
 * Product controller.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@CrossOrigin(origins = {"http://localhost:4200"})
@RestController
@RequestMapping("/v1.0/product")
@RequiredArgsConstructor
public class ProductController {

	/**
	 * Instance of the service that provides information to the controller about the products.
	 */
	private final ProductService service;

	/**
	 * Get a list of all products in the store.
	 *
	 * @return {@link ProductDto} List of products.
	 */
	@GetMapping
	public ResponseEntity<ResponseDto> getAllProducts() {

		final List<ProductDto> products = service.getAllProducts();
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.OK.getDetail())
											  .object(products)
											  .build());
	}

	/**
	 * Saves a new product
	 *
	 * @param productInfo Product to be saved
	 * @return {@link ResponseDto} With the message of the operation.
	 * @throws PrototypeException when the product already exists
	 */
	@PostMapping
	public ResponseEntity<ResponseDto> saveNewProduct(@RequestParam("file") final MultipartFile file,
													  @RequestParam final String productInfo) throws PrototypeException {

		final ProductDto productDto = processProductInfo(productInfo);
		productDto.setPhoto(processMultipartFile(file));
		service.saveNewProduct(productDto);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.SAVED.getDetail())
											  .build());
	}

	/**
	 * Gets the photo of a given product
	 *
	 * @param productId The id of the product
	 * @return The photo of the product with the given id
	 * @throws PrototypeException If the product does not exists.
	 */
	@GetMapping("/photo")
	public ResponseEntity<Resource> getProductPhoto(final String productId) throws PrototypeException {

		final Resource resource = service.getProductPhoto(productId);
		final HttpHeaders headers = new HttpHeaders();
		headers.add(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + resource.getFilename() + "\"");
		headers.add(HttpHeaders.CONTENT_TYPE, "image/*");
		return ResponseEntity.status(HttpStatus.OK)
							 .headers(headers)
							 .body(resource);
	}

	/**
	 * Maps a json product into a Dto Product
	 *
	 * @param productInfo The json product
	 * @return Json product mapped to Dto product
	 * @throws PrototypeException If occurs an error during the mapping.
	 */
	private ProductDto processProductInfo(final String productInfo) throws PrototypeException {

		try {
			final ObjectMapper mapper = new ObjectMapper();
			return mapper.readValue(productInfo, ProductDto.class);
		} catch (JsonProcessingException e) {
			throw new ProductReceivedExcepion("The product received can not be mapped into an internal product.");
		}
	}

	/**
	 * Process the multipart file and saves it into the server
	 *
	 * @param file The multipart file
	 * @return The name of the file
	 * @throws PrototypeException When an error occurs during the file copying or if the file is empty.
	 */
	private String processMultipartFile(final MultipartFile file) throws PrototypeException {

		if (!file.isEmpty()) {
			final String fileName = UUID.randomUUID().toString() + "_" + file.getOriginalFilename();
			if (!StringUtils.isEmpty(fileName)) {
				final Path fileRoute = Paths.get("uploads").resolve(fileName).toAbsolutePath();
				try {
					Files.copy(file.getInputStream(), fileRoute);
					return fileName;
				} catch (IOException e) {
					throw new FileException("Error copying the file in the server.", e);
				}
			} else {
				throw new InvalidFileException("The name of the file provided is empty");
			}
		} else {
			throw new InvalidFileException("The file provided is empty.");
		}
	}

	/**
	 * Deletes a given product
	 *
	 * @param productId Id of the product to be deleted
	 * @return {@link ResponseDto} With the message of the operation.
	 * @throws PrototypeException If the product does not exists.
	 */
	@DeleteMapping
	@Transactional
	public ResponseEntity<ResponseDto> deleteProduct(@RequestParam final String productId) throws PrototypeException {

		service.deleteProduct(productId);
		return ResponseEntity.status(HttpStatus.OK)
							 .body(ResponseDto.builder()
											  .time(ZonedDateTime.now())
											  .responseCode(HttpStatus.OK.value())
											  .status(HttpStatus.OK.getReasonPhrase())
											  .responseMessage(Responses.DELETED.getDetail())
											  .build());
	}
}
