/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.AddProductToOrderUseCase;
import com.payulatam.prototype.application.core.usecase.CreateOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.SaveProductUseCase;
import com.payulatam.prototype.application.core.usecase.entity.Order;
import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.core.usecase.entity.User;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.OrderDto;
import com.payulatam.prototype.application.entrypoint.presenter.OrderPresenter;
import com.payulatam.prototype.application.entrypoint.presenter.converter.OrderDtoMapper;

/**
 * Implementation of the {@link OrderPresenter} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@RequiredArgsConstructor
public class OrderPresenterImpl implements OrderPresenter {

	/**
	 * Read order use case.
	 */
	private final ReadOrderUseCase readOrderUseCase;

	/**
	 * Create order use case.
	 */
	private final CreateOrderUseCase createOrderUseCase;

	/**
	 * Read product use case.
	 */
	private final ReadProductUseCase readProductUseCase;

	/**
	 * Read user use case.
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Read user use case.
	 */
	private final AddProductToOrderUseCase addProductToOrderUseCase;

	/**
	 * Save product use case.
	 */
	private final SaveProductUseCase saveProductUseCase;

	/**
	 * Product mapper for entry point model and core model.
	 */
	private final OrderDtoMapper orderDtoMapper;

	/**
	 * Adds product to an order
	 *
	 * @param productId Product id.
	 * @param quantity  Quantity of the product to add to the order.
	 * @param userId    User id.
	 * @throws PrototypeException When the product does not exists or does not have enough stock.
	 */
	@Override
	public void addProductToOrder(final String productId, final Integer quantity, final String userId)
			throws PrototypeException {

		final Product product = readProductUseCase.getProductWithId(productId);
		final User user = readUserUseCase.getUserByEmail(userId);
		final List<Order> orders = readOrderUseCase.getAllOpenUserOrders(userId);
		if (CollectionUtils.isEmpty(orders)) {
			createOrderUseCase.createOrder(user, product, quantity);
		} else {
			final Order order = orders.get(0);
			addProductToOrderUseCase.addProductToOrder(order, user, product, quantity);
		}
		saveProductUseCase.updateStock(product, quantity, false);
	}

	/**
	 * Get all the Orders.
	 *
	 * @return All the orders in the store.
	 */
	@Override public List<OrderDto> getAllOrders() {

		return orderDtoMapper.toDtoList(readOrderUseCase.getAllOrders());
	}

	/**
	 * Get all the orders that a user has.
	 *
	 * @param userId The id of the user.
	 * @return All the orders in the store.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public List<OrderDto> getUserOrders(final String userId) throws PrototypeException {

		return orderDtoMapper.toDtoList(readOrderUseCase.getAllUserOrders(userId));
	}

	/**
	 * Get the open order that a user has.
	 *
	 * @param userId The id of the user.
	 * @return Open orders for the user.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public OrderDto getOpenOrder(final String userId) throws PrototypeException {

		final List<Order> orders = readOrderUseCase.getAllOpenUserOrders(userId);
		if (CollectionUtils.isEmpty(orders)) {
			return null;
		} else {
			return orderDtoMapper.toDto(orders.get(0));
		}
	}

	/**
	 * Get the order with the given id.
	 *
	 * @param orderId The id of the order.
	 * @return The order with the given id.
	 * @throws PrototypeException When the order does not exists.
	 */
	@Override public OrderDto getOrder(final String orderId) throws PrototypeException {

		return orderDtoMapper.toDto(readOrderUseCase.getOrderById(orderId));
	}
}
