/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter;

import java.util.List;

import org.springframework.core.io.Resource;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.ProductDto;

/**
 * Class that adapts the product message from core layer to entry point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface ProductPresenter {

	/**
	 * Method that brings the products from the core layer to the entry point layer.
	 *
	 * @return All the products.
	 */
	List<ProductDto> getAllProducts();

	/**
	 * Saves a new product
	 *
	 * @param productDto New product to be saved
	 * @throws PrototypeException when the product already exists
	 */
	void saveNewProduct(ProductDto productDto) throws PrototypeException;

	/**
	 * Deletes a product with the given id
	 *
	 * @param productId Id of the product
	 * @throws PrototypeException If the product does not exists.
	 */
	void deleteProduct(String productId) throws PrototypeException;

	/**
	 * Gets the photo of a given product
	 *
	 * @param productId The id of the product
	 * @return The photo of the product with the given id
	 * @throws PrototypeException If the product does not exists.
	 */
	Resource getProductPhoto(String productId) throws PrototypeException;
}
