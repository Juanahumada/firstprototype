/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.converter;

import java.util.List;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.Transaction;
import com.payulatam.prototype.application.entrypoint.controller.dto.TransactionDto;

/**
 * Transaction mapper class from core layer to entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface TransactionDtoMapper {

	/**
	 * Method which translate a transaction from core entity to entry point dto.
	 *
	 * @param transaction from core.
	 * @return {@link TransactionDto}
	 */
	TransactionDto toDto(final Transaction transaction);

	/**
	 * Method which translate transaction from core entity list to entry point dto list.
	 *
	 * @param transactionList from core.
	 * @return {@link TransactionDto} list.
	 */
	List<TransactionDto> toDtoList(final List<Transaction> transactionList);

	/**
	 * Method which translate a new transaction dto to core entity object.
	 *
	 * @param transactionDto Object which will be translate to core entity.
	 * @return {@link Transaction}
	 */
	Transaction toEntity(TransactionDto transactionDto);
}
