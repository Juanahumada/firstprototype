/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Product Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class ProductDto {

	/**
	 * Product id.
	 */
	private String id;

	/**
	 * Product name.
	 */
	private String name;

	/**
	 * Product description.
	 */
	private String description;

	/**
	 * Product value.
	 */
	private BigDecimal value;

	/**
	 * Product stock.
	 */
	private int stock;

	/**
	 * Product photo
	 */
	private String photo;

}
