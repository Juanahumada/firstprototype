/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   04/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.converter;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.NewPaymentInfo;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;

/**
 * New payment info mapper class from core layer to entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface NewPaymentInfoDtoMapper {

	/**
	 * Method which translate a new payment info dto to core entity object.
	 *
	 * @param newPaymentInfoDto Object which will be translate to core entity.
	 * @return {@link NewPaymentInfo}
	 */
	NewPaymentInfo toEntity(NewPaymentInfoDto newPaymentInfoDto);

}
