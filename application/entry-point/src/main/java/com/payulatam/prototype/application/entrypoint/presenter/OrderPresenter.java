/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter;

import java.util.List;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.OrderDto;

/**
 * Class that adapts the order message from core layer to entry point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface OrderPresenter {

	/**
	 * Adds product to an order
	 *
	 * @param productId Product id.
	 * @param quantity  Quantity of the product to add to the order.
	 * @param userId    User id.
	 * @throws PrototypeException When the product does not exists.
	 */
	void addProductToOrder(String productId, Integer quantity, String userId) throws PrototypeException;

	/**
	 * Get all the Orders.
	 *
	 * @return All the orders in the store.
	 */
	List<OrderDto> getAllOrders();

	/**
	 * Get all the orders that a user has.
	 *
	 * @param userId The id of the user.
	 * @return All the orders in the store.
	 * @throws PrototypeException When the user does not exists.
	 */
	List<OrderDto> getUserOrders(String userId) throws PrototypeException;

	/**
	 * Get the open order that a user has.
	 *
	 * @param userId The id of the user.
	 * @return Open orders for the user.
	 * @throws PrototypeException When the user does not exists.
	 */
	OrderDto getOpenOrder(final String userId) throws PrototypeException;

	/**
	 * Get the order with the given id.
	 *
	 * @param orderId The id of the order.
	 * @return The order with the given id.
	 * @throws PrototypeException When the order does not exists.
	 */
	OrderDto getOrder(String orderId) throws PrototypeException;
}
