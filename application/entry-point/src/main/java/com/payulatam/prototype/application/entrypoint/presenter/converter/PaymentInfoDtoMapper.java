/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.converter;

import java.util.List;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.SavedPaymentInfo;
import com.payulatam.prototype.application.entrypoint.controller.dto.PaymentInfoDto;

/**
 * Payment info mapper class from core layer to entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface PaymentInfoDtoMapper {

	/**
	 * Method which translate a product from core entity to entry point dto.
	 *
	 * @param paymentInfo from core.
	 * @return {@link PaymentInfoDto}
	 */
	PaymentInfoDto toDto(final SavedPaymentInfo paymentInfo);

	/**
	 * Method which translate products from core entity list to entry point dto list.
	 *
	 * @param paymentInfoList from core.
	 * @return {@link PaymentInfoDto} list.
	 */
	List<PaymentInfoDto> toDtoList(final List<SavedPaymentInfo> paymentInfoList);

}
