/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.impl;

import java.util.List;

import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.ReadUserSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.SavePaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.PaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.UserDto;
import com.payulatam.prototype.application.entrypoint.presenter.UserPresenter;
import com.payulatam.prototype.application.entrypoint.presenter.converter.NewPaymentInfoDtoMapper;
import com.payulatam.prototype.application.entrypoint.presenter.converter.PaymentInfoDtoMapper;
import com.payulatam.prototype.application.entrypoint.presenter.converter.UserDtoMapper;

/**
 * Implementation of the {@link UserPresenter} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@RequiredArgsConstructor
public class UserPresenterImpl implements UserPresenter {

	/**
	 * Product mapper for entry point model and core model.
	 */
	private final UserDtoMapper mapper;

	/**
	 * Read products use case for product operations.
	 */
	private final ReadUserUseCase readUserUseCase;

	/**
	 * Read payment methods for an user
	 */
	private final ReadUserSavedPaymentInfoUseCase readUserSavedPaymentInfoUseCase;

	/**
	 * Save payment info use case for saving the new info to an user.
	 */
	private final SavePaymentInfoUseCase savePaymentInfoUseCase;
	/**
	 * New payment info mapper for entry point model and core model.
	 */
	private final NewPaymentInfoDtoMapper newPaymentInfoDtoMapper;
	/**
	 * NPayment info mapper for entry point model and core model.
	 */
	private final PaymentInfoDtoMapper paymentInfoDtoMapper;

	/**
	 * Method that brings a user with the given email from the core layer to the entry point layer.
	 *
	 * @param email email of the user.
	 * @return User dto with the given email.
	 * @throws PrototypeException When the user with the given Id does not exist
	 */
	@Override public UserDto getUserByEmail(final String email) throws PrototypeException {

		return mapper.toDto(readUserUseCase.getUserByEmail(email));
	}

	/**
	 * Adds a new payment info to the given user.
	 *
	 * @param newPaymentInfo New payment info to add to the user.
	 * @param userId         User id.
	 */
	@Override public void saveNewPaymentInfo(final NewPaymentInfoDto newPaymentInfo, final String userId) throws PrototypeException {

		savePaymentInfoUseCase.savePaymentInfo(newPaymentInfoDtoMapper.toEntity(newPaymentInfo), userId);
	}

	/**
	 * Gets the saved payment methods for an user.
	 *
	 * @param userId Id of the user
	 * @return The payment methods of the user
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public List<PaymentInfoDto> getPaymentMethods(final String userId) throws PrototypeException {

		return paymentInfoDtoMapper.toDtoList(readUserSavedPaymentInfoUseCase.getPaymentInfo(userId));
	}
}
