/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.converter;

import java.util.List;

import org.mapstruct.InjectionStrategy;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

import com.payulatam.prototype.application.core.usecase.entity.Product;
import com.payulatam.prototype.application.entrypoint.controller.dto.ProductDto;

/**
 * Product mapper class from core layer to entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Mapper(componentModel = "spring",
		unmappedTargetPolicy = ReportingPolicy.IGNORE,
		injectionStrategy = InjectionStrategy.CONSTRUCTOR)
public interface ProductDtoMapper {

	/**
	 * Method which translate a product from core entity to entry point dto.
	 *
	 * @param product from core.
	 * @return {@link ProductDto}
	 */
	ProductDto toDto(final Product product);

	/**
	 * Method which translate products from core entity list to entry point dto list.
	 *
	 * @param productList from core.
	 * @return {@link ProductDto} list.
	 */
	List<ProductDto> toDtoList(final List<Product> productList);

	/**
	 * Method which translate a new product dto to core entity object.
	 *
	 * @param productDto Object which will be translate to core entity.
	 * @return {@link Product}
	 */
	Product toEntity(ProductDto productDto);
}
