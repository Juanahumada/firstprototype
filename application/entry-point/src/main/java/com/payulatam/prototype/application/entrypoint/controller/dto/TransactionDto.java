/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import java.math.BigDecimal;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Transaction Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class TransactionDto {

	/**
	 * Transaction id
	 */
	private String id;
	/**
	 * Transaction order id
	 */
	private String orderId;
	/**
	 * Transaction state
	 */
	private String state;
	/**
	 * Transaction payment network response code
	 */
	private Integer paymentNetworkResponseCode;
	/**
	 * Transaction payment network response error message
	 */
	private String paymentNetworkResponseErrorMessage;
	/**
	 * Transaction traceabilityCode
	 */
	private String traceabilityCode;
	/**
	 * Transaction authorization code
	 */
	private String authorizationCode;
	/**
	 * Transaction pending reason
	 */
	private String pendingReason;
	/**
	 * Transaction response code
	 */
	private String responseCode;
	/**
	 * Transaction error code
	 */
	private String errorCode;
	/**
	 * Transaction response message
	 */
	private String responseMessage;
	/**
	 * Transaction date
	 */
	private Date transactionDate;
	/**
	 * Transaction time
	 */
	private BigDecimal transactionTime;
	/**
	 * Transaction operation date
	 */
	private Date operationDate;
	/**
	 * Order associated to the transaction
	 */
	private OrderDto order;
}
