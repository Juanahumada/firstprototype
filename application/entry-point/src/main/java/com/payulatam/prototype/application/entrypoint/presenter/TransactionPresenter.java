/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.TransactionDto;

/**
 * Class that adapts the transaction message from core layer to entry point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public interface TransactionPresenter {

	/**
	 * Checkout the open order for the given user with the given Payment information.
	 *
	 * @param newPaymentInfoDto The payment information.
	 * @param userId            The id of the user that has the order.
	 * @param orderId           The id of the order to make a checkout.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	TransactionDto checkoutOrderWithNewPaymentMethod(NewPaymentInfoDto newPaymentInfoDto, String orderId, String userId) throws PrototypeException;

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param paymentId The payment id.
	 * @param userId    The id of the user that has the order.
	 * @param orderId   The id of the order to make a checkout.
	 * @throws PrototypeException When the user with the given Id does not exists
	 */
	TransactionDto checkoutOrderWithSavedPaymentMethod(String paymentId, String orderId, String userId) throws PrototypeException;

	/**
	 * Checks if the transaction external api is working properly.
	 *
	 * @return Empty if the api does not have problems. Otherwise returns the message of the error.
	 */
	String ping();

	/**
	 * Checkout the open order for the given user with the selected saved Payment information.
	 *
	 * @param userId        The id of the user that has the order.
	 * @param orderId       The id of the order to make a checkout.
	 * @param transactionId The id of the transaction.
	 * @param reason        The reason for making the refund.
	 * @throws PrototypeException When the order, user or transaction does not exists.
	 */
	TransactionDto refundOrder(String orderId, String userId, String transactionId, String reason) throws PrototypeException;
}
