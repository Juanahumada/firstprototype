/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   11/03/2021
 */

package com.payulatam.prototype.application.entrypoint.presenter.impl;

import java.net.MalformedURLException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.stereotype.Component;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.DeleteProductUseCase;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.SaveProductUseCase;
import com.payulatam.prototype.application.core.usecase.exception.FileException;
import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.ProductDto;
import com.payulatam.prototype.application.entrypoint.presenter.ProductPresenter;
import com.payulatam.prototype.application.entrypoint.presenter.converter.ProductDtoMapper;

/**
 * Implementation of the {@link ProductPresenter} class.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Component
@RequiredArgsConstructor
public class ProductPresenterImpl implements ProductPresenter {

	/**
	 * Product mapper for entry point model and core model.
	 */
	private final ProductDtoMapper mapper;

	/**
	 * Read products use case for product operations.
	 */
	private final ReadProductUseCase readProductUseCase;

	/**
	 * Save product use case for product operations.
	 */
	private final SaveProductUseCase saveProductUseCase;

	/**
	 * Delete product use case for product operations.
	 */
	private final DeleteProductUseCase deleteProductUseCase;

	/**
	 * {@inheritDoc}
	 */
	@Override
	public List<ProductDto> getAllProducts() {

		return mapper
				.toDtoList(readProductUseCase.getAllProducts());
	}

	/**
	 * Saves a new product
	 *
	 * @param productDto New product to be saved
	 * @throws PrototypeException when the product already exists
	 */
	@Override public void saveNewProduct(final ProductDto productDto) throws PrototypeException {

		saveProductUseCase.saveProduct(mapper.toEntity(productDto));
	}

	/**
	 * Deletes a product with the given id
	 *
	 * @param productId Id of the product
	 * @throws PrototypeException If the product does not exists.
	 */
	@Override public void deleteProduct(final String productId) throws PrototypeException {

		deleteProductUseCase.deleteProduct(productId);
	}

	/**
	 * Gets the photo of a given product
	 *
	 * @param productId The id of the product
	 * @return The photo of the product with the given id
	 * @throws PrototypeException If the product does not exists.
	 */
	@Override public Resource getProductPhoto(final String productId) throws PrototypeException {

		final String fileName = readProductUseCase.getProductWithId(productId).getPhoto();

		final Path fileRoute = Paths.get("uploads").resolve(fileName).toAbsolutePath();
		try {
			final Resource resource = new UrlResource(fileRoute.toUri());
			if (!resource.exists()) {
				throw new FileException("The image of the product does not exists.");
			}
			return resource;
		} catch (MalformedURLException e) {
			throw new PrototypeException("Error trying to get the product image.");
		}
	}
}
