/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.controller.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Product Dto for entry-point layer.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PaymentInfoDto {

	/**
	 * Payment id.
	 */
	private String id;
	/**
	 * User credit card tokenized
	 */
	private TokenizedCardDto tokenizedCard;
	/**
	 * User billing street 1
	 */
	private String billingAddressStreet1;

	/**
	 * User billing street 2
	 */
	private String billingAddressStreet2;

	/**
	 * User billing state
	 */
	private String billingAddressState;

	/**
	 * User billing country
	 */
	private String billingAddressCountry;

	/**
	 * User billing postal code
	 */
	private String billingAddressPostalCode;

	/**
	 * User billing phone
	 */
	private String billingAddressPhone;
}
