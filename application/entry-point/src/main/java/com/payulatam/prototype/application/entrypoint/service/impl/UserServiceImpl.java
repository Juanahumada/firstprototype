/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.payulatam.prototype.application.entrypoint.service.impl;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import lombok.RequiredArgsConstructor;

import com.payulatam.prototype.application.core.usecase.exception.PrototypeException;
import com.payulatam.prototype.application.entrypoint.controller.dto.NewPaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.PaymentInfoDto;
import com.payulatam.prototype.application.entrypoint.controller.dto.UserDto;
import com.payulatam.prototype.application.entrypoint.presenter.UserPresenter;
import com.payulatam.prototype.application.entrypoint.service.UserService;

/**
 * Implementation of {@link UserService} interface.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

	/**
	 * Presenter of the product uses cases.
	 */
	private final UserPresenter presenter;

	/**
	 * Search the user in the database.
	 *
	 * @param email The email of the user.
	 * @return The user if it exists.
	 * @throws UsernameNotFoundException When the user is not registered in the database.
	 */
	@Override
	@Transactional
	public UserDetails loadUserByUsername(final String email) throws UsernameNotFoundException {

		try {
			final UserDto user = presenter.getUserByEmail(email);
			if (user == null) {
				throw new UsernameNotFoundException("The user: " + email + "does not exist.");
			}
			final List<GrantedAuthority> authorities = user.getRoles().stream().map(role -> new SimpleGrantedAuthority(role.getName()))
														   .collect(
																   Collectors.toList());
			return new User(user.getEmail(), user.getPassword(), user.getEnabled(), true, true, true, authorities);
		} catch (PrototypeException e) {
			throw new UsernameNotFoundException("The user: " + email + "does not exist.");
		}
	}

	/**
	 * Adds a new payment info to the given user.
	 *
	 * @param newPaymentInfo New payment info to add to the user.
	 * @param userId         User id.
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public void saveNewPaymentInfo(final NewPaymentInfoDto newPaymentInfo, final String userId) throws PrototypeException {

		presenter.saveNewPaymentInfo(newPaymentInfo, userId);
	}

	/**
	 * Gets the saved payment methods for an user.
	 *
	 * @param userId Id of the user
	 * @return List of the user's payment info
	 * @throws PrototypeException When the user does not exists.
	 */
	@Override public List<PaymentInfoDto> getPaymentMethods(final String userId) throws PrototypeException {

		return presenter.getPaymentMethods(userId);
	}
}
