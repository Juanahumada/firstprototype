/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.playulatam.prototype.application.configuration.dataprovider;

import java.util.Properties;
import javax.sql.DataSource;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import com.zaxxer.hikari.HikariDataSource;

/**
 * This class has the function to configure the database connection with the api
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@EnableTransactionManagement
@PropertySource("classpath:application.properties")
@EnableJpaRepositories("com.payulatam.prototype.application.dataprovider.database.repository")
public class DatabaseProviderConfiguration {

	/**
	 * Reading environment variables
	 **/
	private final Environment environment;

	/**
	 * Constructor {@link DatabaseProviderConfiguration} with its attributes initialized
	 *
	 * @param environment Holds environment variables for connect to prototype database
	 */
	public DatabaseProviderConfiguration(final Environment environment) {

		this.environment = environment;
	}

	/**
	 * A factory for connections to the physical data source that this DataSource object represents
	 *
	 * @return Datasource initialized
	 */
	@Bean
	public DataSource dataSource() {

		HikariDataSource dataSource = new HikariDataSource();

		dataSource.setJdbcUrl(environment.getRequiredProperty(DatabaseProviderEnumeration.JDBC_URL.detail()));
		dataSource.setUsername(environment.getRequiredProperty(DatabaseProviderEnumeration.JDBC_USERNAME.detail()));
		dataSource.setPassword(environment.getRequiredProperty(DatabaseProviderEnumeration.JDBC_PASSWORD.detail()));
		dataSource.setMaximumPoolSize(Integer.parseInt(environment.getRequiredProperty(
				DatabaseProviderEnumeration.JDBC_MAX_POOL_SIZE.detail())));
		dataSource.setMinimumIdle(Integer.parseInt(environment.getRequiredProperty(
				DatabaseProviderEnumeration.JDBC_MIN_IDLE.detail())));
		return dataSource;
	}

	/**
	 * Creates a JPA EntityManagerFactory according to JPA's standard container bootstrap contract
	 *
	 * @param dataSource With factory for connections to data base
	 * @return {@link LocalContainerEntityManagerFactoryBean} initialized
	 */
	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory(final DataSource dataSource) {

		LocalContainerEntityManagerFactoryBean entityManagerFactoryBean = new LocalContainerEntityManagerFactoryBean();
		entityManagerFactoryBean.setDataSource(dataSource);
		entityManagerFactoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
		entityManagerFactoryBean.setPackagesToScan(DatabaseProviderEnumeration.PACKAGE_TO_SCAN.detail());

		Properties jpaProperties = new Properties();
		jpaProperties.put(DatabaseProviderEnumeration.HIBERNATE_DIALECT.detail(), environment.getRequiredProperty(
				DatabaseProviderEnumeration.HIBERNATE_DIALECT.detail()));
		jpaProperties.put(DatabaseProviderEnumeration.HIBERNATE_SHOW_SQL.detail(), environment.getRequiredProperty(
				DatabaseProviderEnumeration.HIBERNATE_SHOW_SQL.detail()));
		jpaProperties
				.put(DatabaseProviderEnumeration.HIBERNATE_FORMAT_SQL.detail(), environment.getRequiredProperty(
						DatabaseProviderEnumeration.HIBERNATE_FORMAT_SQL.detail()));
		jpaProperties.put(DatabaseProviderEnumeration.HIBERNATE_LOB.detail(), environment.getRequiredProperty(
				DatabaseProviderEnumeration.HIBERNATE_LOB.detail()));
		jpaProperties.put(DatabaseProviderEnumeration.HIBERNATE_SCHEMA.detail(), environment.getRequiredProperty(
				DatabaseProviderEnumeration.HIBERNATE_SCHEMA.detail()));
		jpaProperties.put(DatabaseProviderEnumeration.HIBERNATE_DDL_AUTO.detail(),
						  environment.getRequiredProperty(DatabaseProviderEnumeration.HIBERNATE_DDL_AUTO.detail()));
		entityManagerFactoryBean.setJpaProperties(jpaProperties);

		return entityManagerFactoryBean;
	}

	/**
	 * Central interface in Spring's transaction infrastructure
	 *
	 * @return {@link PlatformTransactionManager} initialized
	 */
	@Bean
	public PlatformTransactionManager transactionManager() {

		JpaTransactionManager transactionManager = new JpaTransactionManager();
		transactionManager.setEntityManagerFactory(entityManagerFactory(dataSource()).getObject());

		return transactionManager;
	}
}
