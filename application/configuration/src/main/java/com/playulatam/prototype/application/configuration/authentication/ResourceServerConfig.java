/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.playulatam.prototype.application.configuration.authentication;

import java.util.Arrays;
import java.util.Collections;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.Ordered;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;

/**
 * Configuration of the resource server
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@EnableResourceServer
public class ResourceServerConfig extends ResourceServerConfigurerAdapter {

	/**
	 * User role.
	 */
	private static final String USER = "USER";
	/**
	 * Admin role.
	 */
	private static final String ADMIN = "ADMIN";

	/**
	 * {@inheritDoc}
	 */
	@Override public void configure(final HttpSecurity http) throws Exception {

		http.authorizeRequests().antMatchers(HttpMethod.GET, BasicRoutes.PRODUCT.getDetail()).permitAll()
			.and().authorizeRequests().antMatchers(HttpMethod.POST, BasicRoutes.ORDER.getDetail()).hasAnyRole(USER)
			.and().authorizeRequests().antMatchers(HttpMethod.GET, BasicRoutes.ORDER.getDetail()).hasAnyRole(USER)
			.and().authorizeRequests().antMatchers(HttpMethod.GET, BasicRoutes.ORDER.getDetail() + "/all").hasAnyRole(ADMIN)
			.and().authorizeRequests().antMatchers(HttpMethod.POST, BasicRoutes.PRODUCT.getDetail()).hasAnyRole(ADMIN)
			.and().authorizeRequests().antMatchers(HttpMethod.GET, BasicRoutes.PRODUCT.getDetail() + "/photo").permitAll()
			.and().authorizeRequests().antMatchers(HttpMethod.GET, BasicRoutes.PRODUCT.getDetail() + "/photo").permitAll()
			.and().authorizeRequests().antMatchers(HttpMethod.DELETE, BasicRoutes.PRODUCT.getDetail()).hasAnyRole(ADMIN)
			.and().authorizeRequests().antMatchers(HttpMethod.DELETE, BasicRoutes.PRODUCT.getDetail()).hasAnyRole(ADMIN)
			.and().authorizeRequests().antMatchers(HttpMethod.GET, BasicRoutes.TRANSACTION.getDetail() + "/ping").permitAll()
			.and().authorizeRequests().antMatchers(HttpMethod.POST, BasicRoutes.TRANSACTION.getDetail() + "/checkoutOrderNewPaymentInfo")
			.hasAnyRole(USER)
			.and().authorizeRequests().antMatchers(HttpMethod.POST, BasicRoutes.TRANSACTION.getDetail() + "/checkoutOrderSavedPaymentInfo")
			.hasAnyRole(USER)
			.and().authorizeRequests().antMatchers(HttpMethod.POST, BasicRoutes.TRANSACTION.getDetail() + "/refund").hasAnyRole(ADMIN)
			.and().authorizeRequests().antMatchers(HttpMethod.POST, BasicRoutes.TRANSACTION.getDetail() + "/refund").hasAnyRole(USER)
			.anyRequest().authenticated()
			.and().cors().configurationSource(corsConfigurationSource());
	}

	/**
	 * Cors configuration
	 *
	 * @return The cors configuration
	 */
	@Bean
	public CorsConfigurationSource corsConfigurationSource() {

		CorsConfiguration config = new CorsConfiguration();
		config.setAllowedOrigins(Collections.singletonList("http://localhost:4200"));
		config.setAllowedMethods(Arrays.asList("GET", "POST", "PUT", "DELETE", "OPTIONS"));
		config.setAllowCredentials(true);
		config.setAllowedHeaders(Arrays.asList("Content-Type", "Authorization"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}

	/**
	 * Register cors filter
	 *
	 * @return The bean
	 */
	@Bean
	public FilterRegistrationBean<CorsFilter> corsFilter() {

		FilterRegistrationBean<CorsFilter> bean = new FilterRegistrationBean<>(new CorsFilter(corsConfigurationSource()));
		bean.setOrder(Ordered.HIGHEST_PRECEDENCE);
		return bean;
	}
}
