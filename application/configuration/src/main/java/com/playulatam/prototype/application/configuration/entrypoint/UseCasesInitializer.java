/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.playulatam.prototype.application.configuration.entrypoint;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

import com.payulatam.prototype.application.core.provider.OrderProvider;
import com.payulatam.prototype.application.core.provider.ProductProvider;
import com.payulatam.prototype.application.core.provider.TransactionProvider;
import com.payulatam.prototype.application.core.provider.UserProvider;
import com.payulatam.prototype.application.core.usecase.AddProductToOrderUseCase;
import com.payulatam.prototype.application.core.usecase.CreateOrderUseCase;
import com.payulatam.prototype.application.core.usecase.DeleteProductUseCase;
import com.payulatam.prototype.application.core.usecase.MakeTransactionNewPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.MakeTransactionSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.PingTransactionExternalApiUseCase;
import com.payulatam.prototype.application.core.usecase.ReadOrderUseCase;
import com.payulatam.prototype.application.core.usecase.ReadProductUseCase;
import com.payulatam.prototype.application.core.usecase.ReadTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserSavedPaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.ReadUserUseCase;
import com.payulatam.prototype.application.core.usecase.ReverseTransactionUseCase;
import com.payulatam.prototype.application.core.usecase.SavePaymentInfoUseCase;
import com.payulatam.prototype.application.core.usecase.SaveProductUseCase;
import com.payulatam.prototype.application.core.usecase.TokenizeCardUseCase;
import com.payulatam.prototype.application.core.usecase.impl.AddProductToOrderUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.CreateOrderUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.DeleteProductUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.MakeTransactionNewPaymentInfoUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.MakeTransactionSavedPaymentInfoUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.PingTransactionExternalApiUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.ReadOrderUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.ReadProductUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.ReadTransactionUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.ReadUserSavedPaymentInfoUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.ReadUserUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.ReverseTransactionUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.SavePaymentInfoUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.SaveProductUseCaseImpl;
import com.payulatam.prototype.application.core.usecase.impl.TokenizeCardUseCaseImpl;

/**
 * Produces beans to be managed by the Spring container
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@ComponentScan("com.payulatam.prototype.application")
public class UseCasesInitializer {

	/**
	 * Build a valid {@link AddProductToOrderUseCase} bean
	 *
	 * @param provider retrieve order provider
	 * @return {@link AddProductToOrderUseCase} bean
	 */
	@Bean
	@Primary
	public AddProductToOrderUseCase buildAddProductToOrderUseCase(final OrderProvider provider) {

		return new AddProductToOrderUseCaseImpl(provider);
	}

	/**
	 * Build a valid {@link ReadUserUseCase} bean
	 *
	 * @param provider retrieve user provider
	 * @return {@link ReadUserUseCase} bean
	 */
	@Bean
	@Primary
	public ReadUserUseCase buildReadUserUseCase(final UserProvider provider) {

		return new ReadUserUseCaseImpl(provider);
	}

	/**
	 * Build a valid {@link AddProductToOrderUseCase} bean
	 *
	 * @param provider retrieve order provider
	 * @return {@link AddProductToOrderUseCase} bean
	 */
	@Bean
	@Primary
	public CreateOrderUseCase buildCreateOrderUseCase(final OrderProvider provider) {

		return new CreateOrderUseCaseImpl(provider);
	}

	/**
	 * Build a valid {@link MakeTransactionNewPaymentInfoUseCase} bean
	 *
	 * @param provider retrieve transaction provider
	 * @return {@link MakeTransactionNewPaymentInfoUseCase} bean
	 */
	@Bean
	@Primary
	public MakeTransactionNewPaymentInfoUseCase buildMakeTransactionNewPaymentInfoUseCase(final TransactionProvider provider,
																						  final ReadUserUseCase readUserUseCase,
																						  final ReadOrderUseCase readOrderUseCase) {

		return new MakeTransactionNewPaymentInfoUseCaseImpl(provider, readUserUseCase, readOrderUseCase);
	}

	/**
	 * Build a valid {@link MakeTransactionSavedPaymentInfoUseCase} bean
	 *
	 * @param provider retrieve transaction provider
	 * @return {@link MakeTransactionSavedPaymentInfoUseCase} bean
	 */
	@Bean
	@Primary
	public MakeTransactionSavedPaymentInfoUseCase buildMakeTransactionSavedPaymentInfoUseCase(final TransactionProvider provider,
																							  final ReadUserUseCase readUserUseCase,
																							  final ReadOrderUseCase readOrderUseCase,
																							  final ReadUserSavedPaymentInfoUseCase readUserSavedPaymentInfoUseCase) {

		return new MakeTransactionSavedPaymentInfoUseCaseImpl(provider, readUserUseCase, readOrderUseCase, readUserSavedPaymentInfoUseCase);
	}

	/**
	 * Build a valid {@link ReadOrderUseCase} bean
	 *
	 * @param provider retrieve order provider
	 * @return {@link ReadOrderUseCase} bean
	 */
	@Bean
	@Primary
	public ReadOrderUseCase buildReadOrderUseCase(final OrderProvider provider, final ReadUserUseCase readUserUseCase) {

		return new ReadOrderUseCaseImpl(provider, readUserUseCase);
	}

	/**
	 * Build a valid {@link ReadProductUseCase} bean
	 *
	 * @param provider retrieve product provider
	 * @return {@link ReadProductUseCase} bean
	 */
	@Bean
	@Primary
	public ReadProductUseCase buildReadProductUseCase(final ProductProvider provider) {

		return new ReadProductUseCaseImpl(provider);
	}

	/**
	 * Build a valid {@link ReverseTransactionUseCase} bean
	 *
	 * @param provider retrieve transaction provider
	 * @return {@link ReverseTransactionUseCase} bean
	 */
	@Bean
	@Primary
	public ReverseTransactionUseCase buildReverseTransactionUseCase(final TransactionProvider provider,
																	final ReadOrderUseCase readOrderUseCase,
																	final ReadUserUseCase readUserUseCase,
																	final ReadTransactionUseCase readTransactionUseCase) {

		return new ReverseTransactionUseCaseImpl(provider, readOrderUseCase, readUserUseCase, readTransactionUseCase);
	}

	/**
	 * Build a valid {@link ReverseTransactionUseCase} bean
	 *
	 * @param provider            retrieve transaction provider
	 * @param tokenizeCardUseCase use case for tokenize the card.
	 * @return {@link ReverseTransactionUseCase} bean
	 */
	@Bean
	@Primary
	public SavePaymentInfoUseCase buildSavePaymentInfoUseCase(final UserProvider provider, final TokenizeCardUseCase tokenizeCardUseCase,
															  final ReadUserUseCase readUserUseCase) {

		return new SavePaymentInfoUseCaseImpl(provider, tokenizeCardUseCase, readUserUseCase);
	}

	/**
	 * Build a valid {@link TokenizeCardUseCase} bean
	 *
	 * @param provider retrieve transaction provider
	 * @return {@link TokenizeCardUseCase} bean
	 */
	@Bean
	@Primary
	public TokenizeCardUseCase buildTokenizeCardUseCase(final TransactionProvider provider, final ReadUserUseCase readUserUseCase) {

		return new TokenizeCardUseCaseImpl(provider, readUserUseCase);
	}

	/**
	 * Build a valid {@link SaveProductUseCase} bean
	 *
	 * @param provider retrieve product provider
	 * @return {@link SaveProductUseCase} bean
	 */
	@Bean
	@Primary
	public SaveProductUseCase buildSaveProductUseCase(final ProductProvider provider, final ReadProductUseCase readProductUseCase) {

		return new SaveProductUseCaseImpl(provider, readProductUseCase);
	}

	/**
	 * Build a valid {@link PingTransactionExternalApiUseCase} bean
	 *
	 * @param provider retrieve transaction provider
	 * @return {@link PingTransactionExternalApiUseCase} bean
	 */
	@Bean
	@Primary
	public PingTransactionExternalApiUseCase buildPingTransactionExternalApiUseCase(final TransactionProvider provider) {

		return new PingTransactionExternalApiUseCaseImpl(provider);
	}

	/**
	 * Build a valid {@link DeleteProductUseCase} bean
	 *
	 * @param provider retrieve product provider
	 * @return {@link DeleteProductUseCase} bean
	 */
	@Bean
	@Primary
	public DeleteProductUseCase buildDeleteProductUseCase(final ProductProvider provider, final ReadProductUseCase readProductUseCase) {

		return new DeleteProductUseCaseImpl(provider, readProductUseCase);
	}

	/**
	 * Build a valid {@link ReadUserSavedPaymentInfoUseCase} bean
	 *
	 * @param provider retrieve user provider
	 * @return {@link ReadUserSavedPaymentInfoUseCase} bean
	 */
	@Bean
	@Primary
	public ReadUserSavedPaymentInfoUseCase buildReadUserSavedPaymentInfoUseCase(final UserProvider provider,
																				final ReadUserUseCase readUserUseCase) {

		return new ReadUserSavedPaymentInfoUseCaseImpl(provider, readUserUseCase);
	}

	/**
	 * Build a valid {@link ReadTransactionUseCase} bean
	 *
	 * @param provider retrieve transaction provider
	 * @return {@link ReadTransactionUseCase} bean
	 */
	@Bean
	@Primary
	public ReadTransactionUseCase buildReadTransactionUseCase(final TransactionProvider provider) {

		return new ReadTransactionUseCaseImpl(provider);
	}
}
