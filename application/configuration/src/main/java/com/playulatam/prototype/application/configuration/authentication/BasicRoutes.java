/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   19/03/2021
 */

package com.playulatam.prototype.application.configuration.authentication;

import lombok.Getter;

/**
 * Order status.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Getter
public enum BasicRoutes {
	/**
	 * Products basic route.
	 */
	PRODUCT("/v1.0/product"),
	/**
	 * Orders basic route.
	 */
	ORDER("/v1.0/order"),
	/**
	 * Transactions basic route.
	 */
	TRANSACTION("/v1.0/transaction");

	/**
	 * Order string representation.
	 **/
	private final String detail;

	/**
	 * Constructor with description injected by dependency inversion
	 *
	 * @param detail exception string representation
	 */
	BasicRoutes(final String detail) {

		this.detail = detail;
	}
}
