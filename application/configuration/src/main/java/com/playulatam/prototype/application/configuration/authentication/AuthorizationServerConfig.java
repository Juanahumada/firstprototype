/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.playulatam.prototype.application.configuration.authentication;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

/**
 * Configuration of the Authorization server
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

	/**
	 * Password encoder instance
	 */
	private final BCryptPasswordEncoder passwordEncoder;

	/**
	 * Authentication manager instance
	 */
	private final AuthenticationManager authenticationManager;

	/**
	 * Authorization server config constructor
	 *
	 * @param passwordEncoder       password encoder
	 * @param authenticationManager authentication manager
	 */
	public AuthorizationServerConfig(final BCryptPasswordEncoder passwordEncoder, final AuthenticationManager authenticationManager) {

		this.passwordEncoder = passwordEncoder;
		this.authenticationManager = authenticationManager;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public void configure(final AuthorizationServerSecurityConfigurer security) {

		security.tokenKeyAccess("permitAll()")
				.checkTokenAccess("isAuthenticated()");
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public void configure(final ClientDetailsServiceConfigurer clients) throws Exception {

		clients.inMemory().withClient("prototypeapp")
			   .secret(passwordEncoder.encode("secret12345"))
			   .scopes("read", "write")
			   .authorizedGrantTypes("password", "refresh_token")
			   .accessTokenValiditySeconds(3600)
			   .refreshTokenValiditySeconds(3600);
	}

	/**
	 * {@inheritDoc}
	 */
	@Override public void configure(final AuthorizationServerEndpointsConfigurer endpoints) {

		endpoints.authenticationManager(authenticationManager)
				 .accessTokenConverter(accessTokenConverter());
	}

	/**
	 * Creates a new access token converter bean.
	 *
	 * @return The new access token converter
	 */
	@Bean
	public JwtAccessTokenConverter accessTokenConverter() {

		return new JwtAccessTokenConverter();
	}
}
