/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   03/03/2021
 */

package com.playulatam.prototype.application.configuration.dataprovider;

/**
 * Enum with specific data to configure the connection to the database.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>
 * @version 1.0.0
 * @since 1.0.0
 */
public enum DatabaseProviderEnumeration {
	JDBC_URL("spring.datasource.hikari.jdbc-url"),
	JDBC_USERNAME("spring.datasource.hikari.username"),
	JDBC_PASSWORD("spring.datasource.hikari.password"),
	JDBC_MAX_POOL_SIZE("spring.datasource.hikari.maximum-pool-size"),
	JDBC_MIN_IDLE("spring.datasource.hikari.minimum-idle"),

	HIBERNATE_DIALECT("spring.datasource.platform"),
	HIBERNATE_SHOW_SQL("spring.jpa.show-sql"),
	HIBERNATE_FORMAT_SQL("spring.jpa.properties.hibernate.format_sql"),
	HIBERNATE_SCHEMA("spring.jpa.properties.hibernate.default_schema"),
	HIBERNATE_LOB("spring.jpa.properties.hibernate.jdbc.lob.non_contextual_creation"),
	HIBERNATE_DDL_AUTO("hibernate.hbm2ddl.auto"),

	PACKAGE_TO_SCAN("com.payulatam.prototype.application.dataprovider.database");

	private final String description;

	/**
	 * Constructor with its description initialization
	 *
	 * @param description of enumeration
	 */
	DatabaseProviderEnumeration(String description) {

		this.description = description;
	}

	/**
	 * Get the enumeration description
	 *
	 * @return String description
	 */
	public String detail() {

		return description;
	}
}
