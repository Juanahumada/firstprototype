/*
 * PayU Latam - Copyright (c) 2013 - 2021
 * http://www.payu.com.co
 * Date:   09/03/2021
 */

package com.playulatam.prototype.application.configuration;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.context.annotation.Bean;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import feign.Logger;

import com.payulatam.prototype.application.dataprovider.adapter.client.PayUClient;

/**
 * Main class that initialize the prototype backend.
 *
 * @author <a href='mailto:juan.ahumada@payu.com'>Juan Daniel Ahumada Arcos</a>>
 * @version 0.0.1
 */
@EnableAuthorizationServer
@EnableFeignClients(clients = PayUClient.class)
@SpringBootApplication
public class PrototypeStarter {

	/**
	 * Main method that initialize the prototype backend.
	 *
	 * @param args Received as parameters when starting the prototype
	 */
	public static void main(String[] args) {

		SpringApplication.run(PrototypeStarter.class, args);
	}

	@Bean
	Logger.Level feignLoggerLevel() {

		return Logger.Level.FULL;
	}
}
